<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access{
	function can_access(){
		$this->CI =& get_instance();
		return $this->CI->db->select('param,can_view,can_create,can_edit,can_delete,can_export')->from('user_permission')->join('permissions','permissions.id=user_permission.permission_id')->where('user_id',$this->CI->session->userdata('id'))->get()->result_array();
	}
}

?>