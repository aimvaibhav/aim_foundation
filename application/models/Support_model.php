<?php 

class Support_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->model('email_model');
	}

	function save_ticket(){
		$id = $this->input->post('ticket_id');
		if($this->input->post('ticket_type')=='1'){
			$hardware_type = $this->input->post('hardware_type');
			$hardware_item = $this->input->post('hardware_item');
			$hardware_other_item = $this->input->post('hardware_other_item');

			$software_other_item = NULL;
			$software_item = NULL;
			$software_type = 0;
		}else{
			$hardware_type = 0;
			$hardware_item = NULL;
			$hardware_other_item = NULL;

			$software_type = $this->input->post('software_type');
			$software_item = $this->input->post('software_item');
			$software_other_item = $this->input->post('software_other_item');
		}
		$array = array(
			'ticket_type' => $this->input->post('ticket_type'),
			'hardware_type' => $hardware_type,
			'hardware_item' => $hardware_item,
			'hardware_other_item' => $hardware_other_item,
			'software_type' => $software_type,
			'software_item' => $software_item,
			'software_other_item' => $software_other_item,
			'issue' => $this->input->post('issue'),
			'description' => $this->input->post('description'),
			'priority' => $this->input->post('priority'),
		);
		if($id==''){
			$array['date'] = date('Y-m-d h:i:s');
			$array['added_by'] = $this->session->userdata('id');
			$array['added_time'] = time();
			$this->db->insert('ticket_master',$array);

			$ticket_id = $this->db->insert_id();

			$user = $this->db->select('name,email')->where('id',$this->session->userdata('id'))->get('users')->row_array();

			$username = $user['name'];

			$issue = $this->input->post('issue');

			if($this->input->post('priority')==1){
				$priority = 'Low';
			}else if($this->input->post('priority')==2){
				$priority = 'Medium';
			}else if($this->input->post('priority')==3){
				$priority = 'High';
			}

			$company_shortcode = $this->config->item('company_shorcode');

			$ticket_id = $company_shortcode.'-'.str_pad($ticket_id, 4, '0', STR_PAD_LEFT);

			//Mail to admin
			$subject = "Ticket#$ticket_id is genrated";
			$message = "
				Hello <b>Team</b>,<br /><br />

				Ticket#$ticket_id is gerated from $username.<br /><br />

				<b>Issue :</b> $issue<br /><br />
				<b>Priority :</b> $priority<br /><br />

				Thanks,<br />
				Aimcore Team
			";
			$to = get_email_tigger('support_emails');
			$this->email_model->send_email($to,$subject,$message,'');

			//Mail to user
			$user_subject = "Ticket#$ticket_id is genrated";
			$user_message = "
				Hello <b>$username</b>,<br /><br />
				Your ticket is genrated successfully and your ticket id is #$ticket_id. we will assign user to this ticket.<br /><br />
				Thanks,<br />
				<b>Aimcore Team</b>
			";
			$user_to = $user['email'];
			$this->email_model->send_email($user_to,$user_subject,$user_message,'');
		}else{
			$array['updated_by'] = $this->session->userdata('id');
			$array['updated_time'] = time();
			$this->db->where('id',$id)->update('ticket_master',$array);
		}
	}

	function get_my_tickets(){
		return $this->db->select('ticket_master.id,ticket_master.date,ticket_master.issue,ticket_master.description,ticket_master.priority,ticket_master.status,users.name as assign_person')->join('users','users.id=ticket_master.assign','left')->where('ticket_master.added_by',$this->session->userdata('id'))->get('ticket_master')->result();
	}
	function get_ticket_by_id($id){
		return $this->db->where('id',$id)->get('ticket_master')->row_array();
	}

	function get_ticket_commment_by_id($id){
		return $this->db->where('ticket_id',$id)->get('ticket_comments')->result();
	}
	function get_ticket_documents_by_id($id){
		return $this->db->where('ticket_id',$id)->get('ticket_documents')->result();
	}

	function save_comment(){
		$id = $this->input->post('comment_id');
		if($id==''){
			$data=array(
				'comment' => $this->input->post('comment'),
				'ticket_id' => $this->input->post('ticket_id'),
				'added_by' => $this->session->userdata('id'),
				'added_time' => time(),
			);
			$this->db->insert('ticket_comments',$data);
		}else{
			$data=array(
				'comment' => $this->input->post('comments'),
				'updated_by' => $this->session->userdata('id'),
				'updated_time' => time(),
			);
			$this->db->where('id',$id)->update('ticket_comments',$data);
		}
	}

	function delete_comment(){
		$this->db->where('id',$this->input->post('delete_comment_id'))->delete('ticket_comments');
	}

	function upload_ticket_document(){
		$ticket_id = $this->input->post('ticket_id');

		for ($k=0; $k < count($_FILES['files']['name']); $k++){ 
		 	$_FILES['single_file']['name'] = $_FILES['files']['name'][$k];
		    $_FILES['single_file']['type'] = $_FILES['files']['type'][$k];
		    $_FILES['single_file']['tmp_name'] = $_FILES['files']['tmp_name'][$k];
		    $_FILES['single_file']['error'] = $_FILES['files']['error'][$k];
		    $_FILES['single_file']['size'] = $_FILES['files']['size'][$k];
			$doc_part_array = explode('-', $_FILES['single_file']['name']);

			$file_name = file_upload('single_file','support/ticket_document/');
			$array = array(
				'ticket_id' => $ticket_id,
				'original_name  ' => $_FILES['single_file']['name'],
				'document_name ' => $file_name,
				'added_by' => $this->session->userdata('id'),
				'added_time' => time(),
			);
			$this->db->insert('ticket_documents',$array);
		}
	}

	function delete_document(){
		$data =$this->db->where('id',$this->input->post('delete_document_id'))->get('ticket_documents')->row_array();
		if ($data['document_name']) {
			$dest = './documents/support/ticket_document/'.$data['document_name'];
			echo $dest;
			unlink($dest);
		}
		$this->db->where('id',$this->input->post('delete_document_id'))->delete('ticket_documents');
	}

	function get_all_tickets(){
		return $this->db->select('ticket_master.id,ticket_master.date,ticket_master.issue,ticket_master.description,ticket_master.priority,ticket_master.status,assign.name as assign_person,users.name as added_person')->join('users','users.id=ticket_master.added_by')->join('users assign','assign.id=ticket_master.assign','left')->get('ticket_master')->result();
	}

	function get_active_uses(){
		return $this->db->select('id,name,email')->where('status',1)->get('users')->result();
	}

	function get_assigned_tickets(){
		return $this->db->select('ticket_master.id,ticket_master.date,ticket_master.issue,ticket_master.description,ticket_master.priority,ticket_master.status,users.name as added_person')->join('users','users.id=ticket_master.added_by')->where('ticket_master.assign',$this->session->userdata('id'))->get('ticket_master')->result();
	}

	function get_ticket_remote(){
		return $this->db->where('id',$_REQUEST['ticket_id'])->get('ticket_master')->row_array();
	}

	function get_ticket_comment_remote(){
		return $this->db->where('id',$_REQUEST['ticket_comment_id'])->get('ticket_comments')->row_array();
	}

	function assign_ticket(){
		$id = $this->input->post('assign_ticket_id');
		$array = array(
			'assign' => $this->input->post('assign'),
			'status' => 1,
		);
		$this->db->where('id',$id)->update('ticket_master',$array);
		$assign_person = $this->db->select('id,name,email')->where('id',$this->input->post('assign'))->get('users')->row_array();

		$assign_person_name = $assign_person['name'];
		$assign_person_email = $assign_person['email'];
		$company_shortcode = $this->config->item('company_shorcode');
		$ticket_id = $company_shortcode.'-'.str_pad($id, 4, '0', STR_PAD_LEFT);
		$subject = "Ticket#$ticket_id assigned to you !!";
		$message = "
			Hello <b>$assign_person_name</b><br /><br />
			Ticket#$ticket_id assigned to you so please check details and start working on this ticket asap.<br /><br />
			Thanks,<br />
			<b>Aimcore Team</b>
		";
		$this->email_model->send_email($assign_person_email,$subject,$message,'');
	}

	function start_ticket(){
		$id = $this->input->post('ticket_id');
		$array = array(
			'status' => 2,
		);
		$this->db->where('id',$id)->update('ticket_master',$array);
	}

	function resolve_ticket(){
		$id = $this->input->post('resolve_ticket_id');
		$array = array(
			'status' => 3,
		);
		$this->db->where('id',$id)->update('ticket_master',$array);

		$user = $this->db->select('users.name as user_name,users.email')->join('users','users.id=ticket_master.added_by')->where('ticket_master.id',$id)->get('ticket_master')->row_array();

		$user_email = $user['email'];
		$user_name = $user['user_name'];
		$company_shortcode = $this->config->item('company_shorcode');
		$ticket_id = $company_shorcode.'-'.str_pad($id, 4, '0', STR_PAD_LEFT);
		$subject = "Your Ticket#$ticket_id is resolved !!";
		$message = "
			Hello <b>$user_name</b><br /><br />

			Your ticket #$ticket_id is resolved please check and update status of this ticket.<br /><br />

			Thanks,<br />
			<b>Aimcore Team</b>
		";
		$this->email_model->send_email($user_email,$subject,$message,'');
	}

	function reopen_ticket(){
		$id = $this->input->post('reopen_ticket_id');
		$array = array(
			'status' => 4,
		);
		$this->db->where('id',$id)->update('ticket_master',$array);

		$user = $this->db->select('users.name as user_name,users.email')->join('users','users.id=ticket_master.assign')->where('ticket_master.id',$id)->get('ticket_master')->row_array();

		$user_email = $user['email'];
		$user_name = $user['user_name'];
		$company_shortcode = $this->config->item('company_shorcode');
		$ticket_id = $company_shorcode.'-'.str_pad($id, 4, '0', STR_PAD_LEFT);
		$subject = "Ticket#$ticket_id is reopend !!";
		$message = "
			Hello <b>$user_name</b><br /><br />

			Ticket #$ticket_id is reopend by user please check and resolved this issue asap.<br /><br />

			Thanks,<br />
			<b>Aimcore Team</b>
		";
		$this->email_model->send_email($user_email,$subject,$message,'');
	}

	function close_ticket(){
		$id = $this->input->post('close_ticket_id');
		$array = array(
			'status' => 5,
		);
		$this->db->where('id',$id)->update('ticket_master',$array);

		$user = $this->db->select('users.name as user_name,users.email')->join('users','users.id=ticket_master.assign')->where('ticket_master.id',$id)->get('ticket_master')->row_array();

		$user_email = $user['email'];
		$user_name = $user['user_name'];
		$company_shortcode = $this->config->item('company_shorcode');
		$ticket_id = $company_shorcode.'-'.str_pad($id, 4, '0', STR_PAD_LEFT);
		$subject = "Ticket#$ticket_id is closed !!";
		$message = "
			Hello <b>$user_name</b><br /><br />

			Ticket #$ticket_id is closed by user. Thanks for resolving user request.<br /><br />

			Thanks,<br />
			<b>Aimcore Team</b>
		";
		$this->email_model->send_email($user_email,$subject,$message,'');
	}
	
}
?>