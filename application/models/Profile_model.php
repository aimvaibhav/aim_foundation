<?php 

class Profile_model extends CI_Model{		
	function __construct(){
		parent::__construct();
	}

	function profile_pic_upload(){
		$location = 'users/profile_images';
		$file_name = file_upload('profile',$location);
		if($file_name!=''){
			unlink('./documents/users/profile_images/'.$this->session->userdata('profile_image'));
		}
		$array = array(
			'profile_image' => $file_name,
		);
		$this->db->where('id',$this->session->userdata('id'))->update('users',$array);
		$this->session->set_userdata($array);
	}

	function change_password(){
		$check = $this->db->select('id')->where('email',$this->session->userdata('email'))->where('password',md5($this->input->post('old_password')))->get('users')->row_array();
		if(!empty($check)){
			$array = array(
				'password' => md5($this->input->post('password')),
				'updated_by' => $this->session->userdata('id'),
				'updated_time' => time(),
			);
			$this->db->where('id',$this->session->userdata('id'))->update('users',$array);
			return true;
		}else{
			return false;
		}
	}

	function save_detail(){
		$array=array(
			'name'=>$this->input->post('name'),
			'phone'=>$this->input->post('phone'),
			'emergency_phone'=>$this->input->post('emergency_phone'),
			'blood_group'=>$this->input->post('blood_group'),
			'address_line_1'=>$this->input->post('address_line_1'),
			'city'=>$this->input->post('city'),
			'country'=>$this->input->post('country'),
			'date_of_birth'=>$this->input->post('date_of_birth'),
			'marital_status'=>$this->input->post('marital_status'),
			'address_line_2'=>$this->input->post('address_line_2'),
			'state'=>$this->input->post('state'),
			'zip_code'=>$this->input->post('zip_code'),
			'bio' => $this->input->post('bio'),
		);
		$this->db->where('id',$this->session->userdata('id'))->update('users',$array);
		$this->session->set_userdata($array);
	}
}

?>