<?php 

class Permissions_model extends CI_Model{		
	function __construct(){
		parent::__construct();
	}

	function get_active_permissions(){
		return $this->db->select('id,name,param,item_view,item_edit,item_create,item_delete,item')->where('status',1)->get('permissions')->result();
	}

	function get_remote_role_permissions(){
		return $this->db->select('permission_id,can_view,can_create,can_edit,can_delete,can_export,param')->from('role_permission')->join('permissions','permissions.id=role_permission.permission_id')->where('role_id',$_REQUEST['role_id'])->get()->result();
	}

}


?>