<?php 

class Login_model extends CI_Model{		
	function __construct(){
		parent::__construct();
	}

	function do_login($username,$password){
		$user=$this->db->select('users.id as id,username,users.name as name,email,phone,users.status as status,departments.name as department_name,profile_image,department_id,emp_code,emergency_phone,blood_group,address_line_1,city,country,address_line_2,date_of_birth,marital_status,state,zip_code,bio,joining_date,pwd_change_date,expire')->from('users')->join('departments','departments.id=users.department_id')->where('username',$username)->where('password',md5($password))->where('users.status',1)->get()->row_array();
		if(!empty($user)){
			if($user['expire']==0){
				if($user['joining_date']!=''){
					if($user['pwd_change_date']==''){
						$this->db->set('expire',1)->where('id',$user['id'])->update('users');
						return 2;
					}else if(date('Y-m-d')>=$user['pwd_change_date']){
						$this->db->set('expire',1)->where('id',$user['id'])->update('users');
						return 2;
					}
				}
				$this->session->set_userdata($user);
				return true;
			}else{
				return 2;
			}
		}else{
			return false;
		}
	}

	function check_login_status(){
		if($this->session->userdata('username')!=null){
			return true;
		}else{
			return false;
		}
	}

	function send_forgot_password_link(){
		if($_POST['user_type']==1){
			$user = $this->db->select('id,email,name')->where('email',$this->input->post('forgot_password_email'))->get('users')->row_array();
		}else{
			$user = array();
		}
		if($_POST['user_type']==2){
			$user1 = $this->db->select('id,ref_email,name')->where('username',$this->input->post('forgot_password_username'))->where('ref_email',$this->input->post('forgot_password_email'))->get('users')->row_array();
		}else{
			$user1 = array();
		}
		if(!empty($user)){
			$forgot_key = random_string('alnum',10).time().time().random_string('alnum',10);
			$array = array(
				'forgot_key' => $forgot_key,
				'updated_by' => $this->session->userdata('id'),
				'updated_time' => time() 
			);
			$this->db->where('id',$user['id'])->update('users',$array);

			$search = array('{{USER_NAME}}','{{USER_EMAIL}}','{{SUPERVISOR_NAME}}','{{FROM_DATE}}','{{THRU_DATE}}','{{REASON}}','{{COMMENT}}','{{DEPARTMENT}}','{{DECLINE_REASON}}','{{APPROVAL_LINK}}');

			$data = array($user['name'],'','','','','','','','',base_url().'login/change_password/'.$forgot_key);

			$user_email_template = get_email_template_by_title("Forgot Password Email");
			$subject = str_replace($search, $data, $user_email_template['subject']);
			$message = str_replace($search, $data, $user_email_template['message']);

			$to = $user['email'];

			$this->load->model('email_model');
			$this->email_model->send_email($to,$subject,$message);
			return true;
		}else if(!empty($user1)){
			$forgot_key = random_string('alnum',10).time().time().random_string('alnum',10);
			$array = array(
				'forgot_key' => $forgot_key,
				'updated_by' => $this->session->userdata('id'),
				'updated_time' => time() 
			);
			$this->db->where('id',$user1['id'])->update('users',$array);

			$search = array('{{USER_NAME}}','{{USER_EMAIL}}','{{SUPERVISOR_NAME}}','{{FROM_DATE}}','{{THRU_DATE}}','{{REASON}}','{{COMMENT}}','{{DEPARTMENT}}','{{DECLINE_REASON}}','{{APPROVAL_LINK}}');

			$data = array($user1['name'],'','','','','','','','',base_url().'login/change_password/'.$forgot_key);

			$user_email_template = get_email_template_by_title("Forgot Password Email");
			$subject = str_replace($search, $data, $user_email_template['subject']);
			$message = str_replace($search, $data, $user_email_template['message']);

			$to = $user1['ref_email'];

			$this->load->model('email_model');
			$this->email_model->send_email($to,$subject,$message);
			return true;
		}else{
			return flase;
		}
	}

	function check_forgot_password_key($forgot_key){
		return $this->db->select('id')->where('forgot_key',$forgot_key)->get('users')->row_array();
	}

	function reset_password(){
		$array=array(
			'password' => md5($this->input->post('password')),
			'forgot_key' => NULL,
			'updated_by' => $this->session->userdata('id'),
			'updated_time' => time(),
		);
		$this->db->where('forgot_key',$this->input->post('cp_forgot_key'))->update('users',$array);
	}

	function expire_password_update(){
		$username = $this->session->userdata('expire_username');
		$old_password = $this->input->post('old_password');
		$array = array(
			'pwd_change_date' => date('Y-m-d',strtotime('+6 Month')),
			'expire' => 0,
			'password' => md5($this->input->post('password')),
			'updated_by' => $this->session->userdata('id'),
			'updated_time' => time(),
		);
		$this->db->where('username',$username)->where('password',md5($old_password))->update('users',$array);
	}

}

?>