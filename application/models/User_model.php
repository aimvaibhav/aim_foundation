<?php 

class User_model extends CI_Model{		
	function __construct(){
		parent::__construct();
	}
	function get_users(){
		if($this->session->userdata('user_status')==''){
			$status = 1;
		}else{
			if($this->session->userdata('user_status')==2){
				$status = 0;
			}else if($this->session->userdata('user_status')==1){
				$status = 1;
			}
		}
		$this->db->select('users.id as id,username,users.name as name,email,phone,users.status as status,departments.name as department_name,roles.name as role_name,profile_image,emp_code')->join('departments','departments.id=users.department_id')->join('roles','roles.id=users.role_id')->where('users.status',$status);

		if($this->session->userdata('users_location_filter')!='' &&$this->session->userdata('users_location_filter')!='All' ){
			$this->db->where('location',$this->session->userdata('users_location_filter'));
		}
		return $this->db->get('users')->result();
	}

	function get_active_users(){
		return $this->db->select('id,name,email')->where('status',1)->get('users')->result();
	}

	function get_user_by_id($id){
		return $this->db->select("users.id as id,users.name,designation,designation_2,username,email,phone,department_id,users.status,profile_image,GROUP_CONCAT(permission_id,'_',CONCAT(can_view,can_create,can_edit,can_delete,can_export) SEPARATOR ';') as permissions,role_id,emp_code,supervisors,joining_date,departments.name as department_name,card_profile_image,users.last_name")->from('users')->join('departments','departments.id=users.department_id')->join('user_permission','user_permission.user_id=users.id')->where('users.id',$id)->get()->row_array();
	}

	function get_user_data_for_qrcode(){
		return $this->db->select("id,name,email")->where('id',$this->input->post('qr_code_user_id'))->get('users')->row_array();
	}

	function get_user_view_permission_by_id($id){
		return $this->db->select('param,can_view')->join('permissions','permissions.id=user_permission.permission_id')->where('user_id',$id)->get('user_permission')->result_array();
	}

	function get_user_view_profile_by_id($id){
		return $this->db->select('username,name,profile_image')->where('id', $id)->get('users')->row_array();
	}

	function check_username(){
		$user=$this->db->select('username')->from('users')->where('username', $_REQUEST['username'])->where('id<>',$_REQUEST['user_id'])->get()->row_array();
		if(!empty($user)){
			return false;
		}else {
			return true;
		}
	}

	function check_emp_code(){
		$user=$this->db->select('emp_code')->from('users')->where('emp_code', $_REQUEST['emp_code'])->where('id<>',$_REQUEST['user_id'])->get()->row_array();
		if(!empty($user)){
			return false;
		}else {
			return true;
		}
	}

	function check_email(){
		$user=$this->db->select('email')->from('users')->where('email', $_REQUEST['email'])->where('id<>',$_REQUEST['user_id'])->get()->row_array();
		if(!empty($user)){
			return false;
		}else {
			return true;
		}
	}

	function save_user(){
		$config = array('upload_path'=>"./documents/temp",'allowed_types'=>"*");
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$profile_image=null;
		$card_profile_image=null;

		$location = 'users/profile_images';

		if (!is_dir('./documents/'.$location)) {
			mkdir('./documents/'.$location, 0777, true);
		}

		$profile_image = file_upload('profile_image',$location);

		$card_profile_image = file_upload('card_profile_image',$location);

		$id=$this->input->post('user_id');
		if($this->input->post('profile_image_hidden')!=null){
			if($profile_image!=null){
				$path = './documents/users/profile_images/'.$this->input->post('profile_image_hidden');
				unlink($path);
			}else{
				$profile_image = $this->input->post('profile_image_hidden');
			}
		}

		if($this->input->post('card_profile_image_hidden')!=null){
			if($card_profile_image!=null){
				$path = './documents/users/profile_images/'.$this->input->post('card_profile_image_hidden');
				unlink($path);
			}else{
				$card_profile_image = $this->input->post('card_profile_image_hidden');
			}
		}

		if($this->input->post('status')!=''){
			$status = $this->input->post('status');
		}else{
			$status = 0;
		}

		if($this->input->post('joining_date')!=''){
			$joining_date = date('Y-m-d',strtotime($this->input->post('joining_date')));
		}else{
			$joining_date = NULL;
		}

		$username = $this->input->post('username');

		$name = $this->input->post('name');

		$array = array(
			'name' => $this->input->post('name'),
			'last_name' => $this->input->post('last_name'),
			'designation' => $this->input->post('designation'),
			'designation_2' => $this->input->post('designation_2'),
			'username' => $this->input->post('username'), 
			'emp_code' => $this->input->post('emp_code'),
			'email' => $this->input->post('email'), 
			'ref_email' => $this->input->post('ref_email'), 
			'phone' => $this->input->post('phone'), 
			'location' => $this->input->post('location'), 
			'department_id' => $this->input->post('department'), 
			'role_id' => $this->input->post('role'),
			'supervisors' => implode(',', $this->input->post('supervisors')),
			'joining_date' => $joining_date,
			'profile_image' => $profile_image,
			'card_profile_image' => $card_profile_image,
			'status' => $status,
		);
		if($id==null){
			$joining_year = date('y',strtotime($this->input->post('joining_date')));
			$joining_month = date('m',strtotime($this->input->post('joining_date')));

			$user_name = explode(' ', $this->input->post('name'));

			$password = "";

			$i1 = $i2 = 0;

			if(isset($user_name[0])){
				$password = strtoupper($user_name[0][0]);
				$i1 = strlen($user_name[0]);
			}

			if(isset($user_name[1])){
				$password .= strtolower($user_name[1][0]);
				$i2 = strlen($user_name[1]);
			}

			$password .= "@".$i1.$joining_year.$joining_month.$i2;

			$db_password = $password;

			$array['password'] = md5($db_password);

			$array['added_by'] = $this->session->userdata('id');
			$array['added_time'] = time();
			$this->db->insert('users',$array);

			$subject = "$name user account has been created in Aimnet";

			$message = "
				Hello <b>Team</b><br /><br />
				User account has been created in aimnet. User can access aimnet using below details:<br /><br />
				<b>Name : </b> $name<br />
				<b>Username : </b> $username<br />
				<b>Password : </b> $db_password<br /><br />
				Thanks,<br />
				<b>Aimcore Team</b>
			";

			$this->load->model('Email_model');

			$this->Email_model->send_email('amit@aimtron.com',$subject,$message);

			$user_id = $this->db->insert_id();
		}else{
			$array['updated_by'] = $this->session->userdata('id');
			$array['updated_time'] = time();
			$this->db->where('id',$id)->update('users',$array);
			$user_id=$id;
		}

		$this->load->model('permissions_model');

		$permissions=$this->permissions_model->get_active_permissions();

		foreach ($permissions as $permission) {
			if($this->input->post('can_view')!=''){
				$view_array = $this->input->post('can_view');
			}else{
				$view_array = array();
			}
			if($this->input->post('can_create')!=''){
				$create_array = $this->input->post('can_create');
			}else{
				$create_array = array();
			}
			if($this->input->post('can_edit')!=''){
				$edit_array = $this->input->post('can_edit');
			}else{
				$edit_array = array();
			}
			if($this->input->post('can_delete')!=''){
				$delete_array = $this->input->post('can_delete');
			}else{
				$delete_array = array();
			}
			if($this->input->post('can_export')!=''){
				$export_array = $this->input->post('can_export');
			}else{
				$export_array = array();
			}
			$can_view = array_key_exists($permission->id, $view_array) ?  1 : 0;
			$can_create = array_key_exists($permission->id, $create_array) ?  1 : 0;
			$can_edit = array_key_exists($permission->id, $edit_array) ?  1 : 0;
			$can_delete = array_key_exists($permission->id, $delete_array) ? 1 : 0;
			$can_export = array_key_exists($permission->id, $export_array) ? 1 : 0;

			$user_p = $this->db->select('id')->where('user_id',$id)->where('permission_id',$permission->id)->get('user_permission')->row_array();
			$array=array(
				"user_id" => $user_id,
				"permission_id" => $permission->id,
				"can_view" => $can_view,
				"can_create" => $can_create,
				"can_edit" => $can_edit,
				"can_delete" => $can_delete,
				"can_export" => $can_export,
			);
			if($id==null || empty($user_p)){
				$array["added_by"]=$this->session->userdata('id');
				$array["added_time"]=time();
				$this->db->insert('user_permission',$array);
			}else{
				$array["updated_by"]=$this->session->userdata('id');
				$array["updated_time"]=time();
				$this->db->where('user_id',$id)->where('permission_id',$permission->id)->update('user_permission',$array);
			}
		}
	}

	function reset_user_password(){
		$array = array(
			'password' => md5($this->input->post('password')),
			'updated_by' => $this->session->userdata('id'),
			'updated_time' => time(),
		);
		$this->db->where('id',$this->input->post('rp_user_id'))->update('users',$array);
	}

	function delete_user(){
		$id=$this->input->post('delete_user_id');

		$user = $this->db->select('profile_image,emp_code')->from('users')->where('id',$id)->get()->row_array();

		if($user['profile_image']!=null){
			unlink('./documents/employees/'.$user['emp_code'].'/'.$user['profile_image']);
		}

		// Delete user
		$this->db->where('id',$id)->delete('users');

		// Delete user permissions
		$this->db->where('user_id',$id)->delete('user_permission');

		// Delete user timeoff requests
		$this->db->where('user_id',$id)->delete('timeoff_requests');
	}
}
?>