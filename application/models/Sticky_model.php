<?php 
class Sticky_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function get_users(){
		return $this->db->select('id,name')->where('status',1)->get('users')->result();
	}

	function get_sticky_notes(){
		$id = $this->session->userdata('id');
		return  $this->db->select('sticky_id,sticky_heading,sticky_desc,sticky_status,shared_person,sticky_notes.added_time as added_time,sticky_notes.added_by as added_by ,GROUP_CONCAT(share.name) as sperson,added.name as ad_name,sticky_notes.updated_time as updated_time')->join('users added','added.id=sticky_notes.added_by')->join('users share','FIND_IN_SET(share.id, sticky_notes.shared_person) > 0','left')->where('sticky_status',1)->group_start()->where('sticky_notes.added_by',$id)->or_where("FIND_IN_SET(".$id.",shared_person) !=", 0)->group_end()->group_by('sticky_id')->order_by('added_time','DESC')->get('sticky_notes')->result();
	}

	function addnote(){
		$id=$this->input->post('stickyid');
		$array=array(
			'sticky_heading'=>$this->input->post('heading'),
			'sticky_desc'=>$this->input->post('desc'),
		);
		if($id==null){
			$array['added_by'] = $this->session->userdata('id');
			$array['added_time'] = time();
			$this->db->insert('sticky_notes',$array);
		}else{
			$array['updated_by'] = $this->session->userdata('id');
			$array['updated_time'] = time();
			$this->db->where('sticky_id',$id)->update('sticky_notes',$array);
		}
	}

	function getstickydetail(){
		return $this->db->select('sticky_id,sticky_heading,sticky_desc')->where('sticky_id',$_REQUEST['stickyid'])->get('sticky_notes')->row_array();
	}

	function updatenote(){
		$array=array(
			'sticky_status'=>0,
		);
		$this->db->where('sticky_id',$this->input->post('delete_note_id'))->update('sticky_notes',$array);
	}

	function sharenote(){
		$person=implode(',',$this->input->post('person'));
		$array = array('shared_person' => $person);
		$this->db->where('sticky_id',$this->input->post('sharestickyid'))->update('sticky_notes',$array);
	}
}
?>