<?php 

class Department_model extends CI_Model{		
	function __construct(){
		parent::__construct();
	}

	function get_active_departments(){
		return $this->db->select('id,name')->where('status',1)->get('departments')->result();
	}

	function get_departments(){
		return $this->db->select('departments.id as id,departments.name as name,departments.status as status,users.name as supervisor_name')->join('users','users.id=departments.supervisor')->get('departments')->result();
	}

	function save_department(){
		$status = $this->input->post('status')=='' ? 0 : 1;
		$array=array(
			'name' => $this->input->post('name'),
			'status' => $status,
			'supervisor' => $this->input->post('supervisor')
		);
		if($this->input->post('department_id')==''){
			$array['added_by'] = $this->session->userdata('id');
			$array['added_time'] = time();
			$this->db->insert('departments',$array);
		}else{
			$array['updated_by'] = $this->session->userdata('id');
			$array['updated_time'] = time();
			$this->db->where('id',$this->input->post('department_id'))->update('departments',$array);
		}
	}

	function get_department_remote(){
		return $this->db->select('id,status,name,supervisor')->where('id',$_REQUEST['department_id'])->get('departments')->row_array();
	}

	function delete_department(){
		$this->db->where('id',$this->input->post('delete_department_id'))->delete('departments');
	}
}

?>