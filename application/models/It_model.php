<?php 

class It_model extends CI_Model{	
	function __construct(){
		parent::__construct();
		$this->load->model('email_model');
	}	

	function get_active_permissions(){
		return $this->db->select('id,name,param,item_view,item_edit,item_create,item_delete,item')->where('status',1)->get('permissions')->result();
	}

	function get_all_permission_for_permission_items(){
		return $this->db->select('id,name,param,item,item_view,item_edit,item_create,item_delete')->get('permissions')->result();
	}

	function get_permission_items_remote(){
		return $this->db->select('id,item,param')->where('id',$_REQUEST['permission_remote_id'])->get('permissions')->row_array();
	}

	function get_remote_role_permissions(){
		return $this->db->select('permission_id,can_view,can_create,can_edit,can_delete,param')->from('role_permission')->join('permissions','permissions.id=role_permission.permission_id')->where('role_id',$_REQUEST['role_id'])->get()->result();
	}

	function delete_permission_json_item($id,$json_id){
		$json_array = $this->db->select('item')->where('id',$id)->get('permissions')->row_array();
		$json_array = json_decode($json_array['item']);
		$json_new = array();
		foreach ($json_array as $json) {
			if($json->id != $json_id){
				$new_array = array( 
					'id' => $json->id,
					'access' => $json->access,
					'item' => $json->item,
				);
				array_push($json_new, $new_array);
			}
		}
		$array = array(
			'item' => json_encode($json_new),
		  'updated_by' => $this->session->userdata('id'),
		  'updated_time' => time(),
		);
		$this->db->where('id',$id)->update('permissions',$array);
	}

	function save_permission_access(){
		$id = $this->input->post('permission_access_id');
		$access = $this->input->post('permission_access');
		$status = $this->input->post('permission_access_status') == 1 ? '0':'1';
		$data = array(
			$access => $status,
			'updated_by' => $this->session->userdata('id'),
			'updated_time' => time(),
		);
		$this->db->where('id',$id)->update('permissions',$data);
	}

	function save_permission_items(){
		$id = $this->input->post('permission_items_id');
		$permission_array = $this->db->select('item')->where('id',$id)->get('permissions')->row_array();
		$i = '';
		if ($permission_array['item'] == 'null' || empty($permission_array['item'])) {
			$permission_array = array();
			$i = 1;
		}else if(!empty($permission_array['item'])) {
			$permission_array = json_decode($permission_array['item']);
			$last_item = end($permission_array);
			$i = $last_item->id + 1;
		}
		$json = array(
			'id' => $i,
			'access' => $this->input->post('permission'),
			'item' => $this->input->post('permission_item'),
		);
		array_push($permission_array, $json);
		$array = array(
			'item' => json_encode($permission_array),
		  'updated_by' => $this->session->userdata('id'),
		  'updated_time' => time(),
		);
		$this->db->where('id',$id)->update('permissions',$array);
	}

}

?>