<?php 

class Hr_model extends CI_Model{	
	function __construct(){
		parent::__construct();
		$this->load->model('email_model');
	}

	function save_time_off_request(){
		$access=$this->access->can_access(); 
  		$time_off_requests_list=array_search('time_off_requests_list', array_column($access, 'param')); 
		$array=array(
			'from_date' => date('Y-m-d H:i',strtotime($this->input->post('from_date'))),
			'thru_date' => date('Y-m-d H:i',strtotime($this->input->post('thru_date'))),
			'reason' => $this->input->post('reason'),
			'comment' => $this->input->post('comment'),
		);
		$id = $this->input->post('timeoff_id');
		if($id==''){
			$array['date'] = date('Y-m-d');
			if($access[$time_off_requests_list]['can_delete']==1){
				$array['user_id'] = $this->input->post('name');
				$array['department_id'] = $this->input->post('department_id');
			}else{
				$array['user_id'] = $this->session->userdata('id');
				$array['department_id'] = $this->session->userdata('department_id');
			}
			$array['added_by'] = $this->session->userdata('id');
			$array['added_time'] = time();
			$this->db->insert('timeoff_requests',$array);

			$insert_id = $this->db->insert_id();

			if($access[$time_off_requests_list]['can_delete']!=1){
				$supervisor = $this->db->select('GROUP_CONCAT(u1.name) as supervisor_name,GROUP_CONCAT(u1.email) as supervisor_email')->where('users.id',$this->session->userdata('id'))->join('users u1','FIND_IN_SET(u1.id, users.supervisors) > 0')->get('users')->row_array();
				
				#############################
				## User confirmation email ##
				#############################
				$search = array('{{USER_NAME}}','{{USER_EMAIL}}','{{SUPERVISOR_NAME}}','{{FROM_DATE}}','{{THRU_DATE}}','{{REASON}}','{{COMMENT}}','{{DEPARTMENT}}','{{DECLINE_REASON}}','{{APPROVAL_LINK}}');
				$data = array($this->session->userdata('name'),'',$supervisor['supervisor_name'],$this->input->post('from_date'),$this->input->post('thru_date'),$this->input->post('reason'),$this->input->post('comment'),$this->session->userdata('department_name'),'',base_url()."hr/time_off_approval/supervisor/$insert_id");
				$user_email_template = get_email_template_by_title("Time off request user confirmation mail");
				$user_subject = str_replace($search, $data, $user_email_template['subject']);
				$user_message = str_replace($search, $data, $user_email_template['message']);
				$user_email = $this->session->userdata('email');
				$this->email_model->send_email($user_email,$user_subject,$user_message);
				
				if(!empty($supervisor)){
					###################################
					## Supervisor email for approval ##
					###################################
					$sup_email_template = get_email_template_by_title("Time off request supervisor approval mail");
					$sup_email = $supervisor['supervisor_email'];
					$sup_subject = str_replace($search, $data, $sup_email_template['subject']);
					$sup_message = str_replace($search, $data, $sup_email_template['message']);
					$this->email_model->send_email($sup_email,$sup_subject,$sup_message);
				}else{
					################################################################
					## If supervisor not found then email send to hr for approval ##
					################################################################

					$hr_email = get_email_tigger('hr_email');
					$email_template = get_email_template_by_title("Time off request hr approval mail after supervisor");
					$subject = str_replace($search, $data, $email_template['subject']);
					$message = str_replace($search, $data, $email_template['message']);
					$this->email_model->send_email($hr_email,$subject,$message);
				}
			}else{
				$a1 = array(
					'status' => 3
				);
				$this->db->where('id',$insert_id)->update('timeoff_requests',$a1);
			}
		}else{
			$array['updated_by'] = $this->session->userdata('id');
			$array['updated_time'] = time();
			$this->db->where('id',$id)->update('timeoff_requests',$array);
		}
	}

	function delete_timeoff_request(){
		$this->db->where('id',$this->input->post('delete_timeoff_id'))->delete('timeoff_requests');
	}

	function get_time_off_request_approved_list(){
		if($this->session->userdata('approved_list_from_date')!=''){
			$from_date = date('Y-m-d',strtotime($this->session->userdata('approved_list_from_date')));
			$to_date = date('Y-m-d',strtotime($this->session->userdata('approved_list_to_date')));
		}else{
			$from_date = date('Y-m-d',strtotime('-1 Year'));
			$to_date = date('Y-m-d');
		}
		return $this->db->select('timeoff_requests.id as id,DATE_FORMAT(date,"%m/%d/%Y") as date,DATE_FORMAT(from_date,"%b %d %Y %r") as from_date,DATE_FORMAT(thru_date,"%b %d %Y %r") as thru_date,reason,comment,users.name as user_name,departments.name as department_name,timeoff_requests.status as status')->join('users','users.id=timeoff_requests.user_id')->join('departments','departments.id=timeoff_requests.department_id')->where('timeoff_requests.status !=',0)->where('timeoff_requests.status !=',1)->where('timeoff_requests.date >=',date('Y-m-d',strtotime($from_date)))->where('timeoff_requests.date <=',date('Y-m-d',strtotime($to_date)))->get('timeoff_requests')->result();
	}

	function get_time_off_request_pending_list(){
		$access=$this->access->can_access(); 
  		$time_off_requests_list=array_search('time_off_requests_list', array_column($access, 'param')); 
		if($this->session->userdata('pending_list_from_date')!=''){
			$from_date = date('Y-m-d',strtotime($this->session->userdata('pending_list_from_date')));
			$to_date = date('Y-m-d',strtotime($this->session->userdata('pending_list_to_date')));
		}else{
			$from_date = date('Y-m-d',strtotime('-1 Year'));
			$to_date = date('Y-m-d');
		}
		$this->db->select('timeoff_requests.id as id,DATE_FORMAT(date,"%m/%d/%Y") as date,DATE_FORMAT(from_date,"%b %d %Y %r") as from_date,DATE_FORMAT(thru_date,"%b %d %Y %r") as thru_date,reason,comment,users.name as user_name,departments.name as department_name,timeoff_requests.status as status');
		$this->db->join('users','users.id=timeoff_requests.user_id');
		$this->db->join('departments','departments.id=timeoff_requests.department_id');
		if($access[$time_off_requests_list]['can_delete']==1){
			$this->db->where('timeoff_requests.status',0);
			$this->db->or_where('timeoff_requests.status',1);
		}else{
			$this->db->where('timeoff_requests.status',0);
			$u_id = $this->session->userdata('id');
			$this->db->where('find_in_set("'.$u_id.'", users.supervisors) <> 0');
		}
		$this->db->where('timeoff_requests.date >=',date('Y-m-d',strtotime($from_date)));
		$this->db->where('timeoff_requests.date <=',date('Y-m-d',strtotime($to_date)));
		$query = $this->db->get('timeoff_requests');
		$res = $query->result();
		return $res;
	}

	function get_my_time_off_requests(){
		return $this->db->select('timeoff_requests.id as id,DATE_FORMAT(date,"%m/%d/%Y") as date,DATE_FORMAT(from_date,"%b %d %Y %r") as from_date,DATE_FORMAT(thru_date,"%b %d %Y %r") as thru_date,reason,comment,users.name as user_name,departments.name as department_name,timeoff_requests.status as status')->join('users','users.id=timeoff_requests.user_id')->join('departments','departments.id=timeoff_requests.department_id')->where('user_id',$this->session->userdata('id'))->get('timeoff_requests')->result();
	}

	function cancel_timeoff_request(){
		$array=array(
			'status'=>5,
			'updated_by' => $this->session->userdata('id'),
			'updated_time' => time(),
		);
		$this->db->where('id',$this->input->post('cancel_ticket_id'))->update('timeoff_requests',$array);

		$user=$this->get_timeoff_data($this->input->post('cancel_ticket_id'));
		#############################
		## Cancellation mail to hr ##
		#############################

		$to = get_email_tigger('hr_email');
		$search = array('{{USER_NAME}}','{{USER_EMAIL}}','{{SUPERVISOR_NAME}}','{{FROM_DATE}}','{{THRU_DATE}}','{{REASON}}','{{COMMENT}}','{{DEPARTMENT}}','{{DECLINE_REASON}}','{{APPROVAL_LINK}}');
		$data = array($user["user_name"],'','',date('F j, Y, g:i A',strtotime($user['from_date'])),date('F j, Y, g:i A ',strtotime($user['thru_date'])),$user['reason'],$user['comment'],$user['department_name'],'','');
		$email_template = get_email_template_by_title("Cancel time off request: HR email");
		$subject = str_replace($search, $data, $email_template['subject']);
		$message = str_replace($search, $data, $email_template['message']);
		$this->email_model->send_email($to,$subject,$message);
	}

	function get_timeoff_request_detail(){
		return $this->db->select('timeoff_requests.id as id,DATE_FORMAT(date,"%m/%d/%Y") as date,DATE_FORMAT(from_date,"%M %d %Y %r") as from_date,DATE_FORMAT(thru_date,"%M %d %Y %r") as thru_date,reason,comment,users.name as user_name,departments.name as department_name,timeoff_requests.status as status')->join('users','users.id=timeoff_requests.user_id')->join('departments','departments.id=timeoff_requests.department_id')->where('timeoff_requests.id',$_REQUEST['timeoff_id'])->get('timeoff_requests')->row_array();
	}

	function get_timeoff_data($id){
		return $this->db->select('timeoff_requests.id as id,DATE_FORMAT(date,"%m/%d/%Y") as date,DATE_FORMAT(from_date,"%m/%d/%Y %H:%i") as from_date,DATE_FORMAT(thru_date,"%m/%d/%Y %H:%i") as thru_date,reason,comment,users.name as user_name,departments.name as department_name,timeoff_requests.status as status,users.email as email')->join('users','users.id=timeoff_requests.user_id')->join('departments','departments.id=timeoff_requests.department_id')->where('timeoff_requests.id',$id)->get('timeoff_requests')->row_array();
	}

	function update_time_off_request_approval(){
		$array=array(
			'status' => $this->input->post('status'),
			'updated_by' => $this->session->userdata('id'),
			'updated_time' => time(),
		);
		$this->db->where('id',$this->input->post('timeoff_id'))->update('timeoff_requests',$array);

		$user=$this->get_timeoff_data($this->input->post('timeoff_id'));
		$search = array('{{USER_NAME}}','{{USER_EMAIL}}','{{SUPERVISOR_NAME}}','{{FROM_DATE}}','{{THRU_DATE}}','{{REASON}}','{{COMMENT}}','{{DEPARTMENT}}','{{DECLINE_REASON}}','{{APPROVAL_LINK}}');
		$data = array($user["user_name"],'','',date('F j, Y, g:i A',strtotime($user['from_date'])),date('F j, Y, g:i A ',strtotime($user['thru_date'])),$user['reason'],$user['comment'],$user['department_name'],'',base_url()."hr/time_off_approval/hr/".$this->input->post('timeoff_id'));

		if($this->input->post('status')==1){
			######################################
			## Supervisor Approve : => HR Email ##
			######################################
			$to = get_email_tigger('hr_email');
			$email_template = get_email_template_by_title("Time off request hr approval mail after supervisor");
			$subject = str_replace($search, $data, $email_template['subject']);
			$message = str_replace($search, $data, $email_template['message']);
			$this->email_model->send_email($to,$subject,$message);
		}else if($this->input->post('status')==3){
			###############################################
			## HR Approve : => Send notification to user ##
			###############################################
			$to = $user['email'];
			$user_email_template = get_email_template_by_title("Time off request user approval mail");
			$user_subject = str_replace($search, $data, $user_email_template['subject']);
			$user_message = str_replace($search, $data, $user_email_template['message']);
			$this->email_model->send_email($to,$user_subject,$user_message);
		}
	}

	function approval_timeoff_request(){
		$array=array(
			'status' => $this->input->post('status'),
			'updated_by' => $this->session->userdata('id'),
			'updated_time' => time(),
		);
		$this->db->where('id',$this->input->post('approval_timeoff_id'))->update('timeoff_requests',$array);

		$user=$this->get_timeoff_data($this->input->post('approval_timeoff_id'));
		$search = array('{{USER_NAME}}','{{USER_EMAIL}}','{{SUPERVISOR_NAME}}','{{FROM_DATE}}','{{THRU_DATE}}','{{REASON}}','{{COMMENT}}','{{DEPARTMENT}}','{{DECLINE_REASON}}','{{APPROVAL_LINK}}');
		$data = array($user["user_name"],'','',date('F j, Y, g:i A',strtotime($user['from_date'])),date('F j, Y, g:i A ',strtotime($user['thru_date'])),$user['reason'],$user['comment'],$user['department_name'],'',base_url()."hr/time_off_approval/hr/".$this->input->post('approval_timeoff_id'));

		if($this->input->post('status')==1){
			######################################
			## Supervisor Approve : => HR Email ##
			######################################
			$to = get_email_tigger('hr_email');
			$email_template = get_email_template_by_title("Time off request hr approval mail after supervisor");
			$subject = str_replace($search, $data, $email_template['subject']);
			$message = str_replace($search, $data, $email_template['message']);
			$this->email_model->send_email($to,$subject,$message);
		}else if($this->input->post('status')==3){
			###############################################
			## HR Approve : => Send notification to user ##
			###############################################
			$to = $user['email'];
			$user_email_template = get_email_template_by_title("Time off request user approval mail");
			$user_subject = str_replace($search, $data, $user_email_template['subject']);
			$user_message = str_replace($search, $data, $user_email_template['message']);
			$this->email_model->send_email($to,$user_subject,$user_message);
		}else if($this->input->post('status')==2){
			######################################################
			## Supervisor deline : => send notification to user ##
			######################################################
			$to = $user['email'];
			$email_template = get_email_template_by_title("Time off decline by supervisor mail");
			$subject = str_replace($search, $data, $email_template['subject']);
			$message = str_replace($search, $data, $email_template['message']);
			$this->email_model->send_email($to,$subject,$message);
		}else if($this->input->post('status')==4){
			###############################################
			## HR decline : => send notification to user ##
			###############################################
			$to = $user['email'];
			$email_template = get_email_template_by_title("Time off decline by HR mail");
			$subject = str_replace($search, $data, $email_template['subject']);
			$message = str_replace($search, $data, $email_template['message']);
			$this->email_model->send_email($to,$subject,$message);
		}
	}

	function decline_time_off_request_approval(){
		$array=array(
			'status' => $this->input->post('status'),
			'decline_reason' => $this->input->post('decline_reason'),
			'updated_by' => $this->session->userdata('id'),
			'updated_time' => time(),
		);
		$this->db->where('id',$this->input->post('timeoff_id_modal'))->update('timeoff_requests',$array);

		$user=$this->get_timeoff_data($this->input->post('timeoff_id_modal'));
		$search = array('{{USER_NAME}}','{{USER_EMAIL}}','{{SUPERVISOR_NAME}}','{{FROM_DATE}}','{{THRU_DATE}}','{{REASON}}','{{COMMENT}}','{{DEPARTMENT}}','{{DECLINE_REASON}}','{{APPROVAL_LINK}}');
		$data = array($user["user_name"],'','',date('F j, Y, g:i A',strtotime($user['from_date'])),date('F j, Y, g:i A ',strtotime($user['thru_date'])),$user['reason'],$user['comment'],$user['department_name'],$this->input->post('decline_reason'),'');

		if($this->input->post('status')==2){
			######################################################
			## Supervisor deline : => send notification to user ##
			######################################################
			$to = $user['email'];
			$email_template = get_email_template_by_title("Time off decline by supervisor mail");
			$subject = str_replace($search, $data, $email_template['subject']);
			$message = str_replace($search, $data, $email_template['message']);
			$this->email_model->send_email($to,$subject,$message);
		}else if($this->input->post('status')==4){
			###############################################
			## HR decline : => send notification to user ##
			###############################################
			$to = $user['email'];
			$email_template = get_email_template_by_title("Time off decline by HR mail");
			$subject = str_replace($search, $data, $email_template['subject']);
			$message = str_replace($search, $data, $email_template['message']);
			$this->email_model->send_email($to,$subject,$message);
		}
	}

	function get_approved_time_off_requests(){
		return $this->db->select('date,from_date,thru_date,users.name as user_name,reason,comment')->join('users','users.id=timeoff_requests.user_id')->where('timeoff_requests.status',3)->get('timeoff_requests')->result();
	}


	function get_active_users(){
		return $this->db->select('id,name')->where('status',1)->get('users')->result();
	}

	function get_user_department(){
		return $this->db->select('departments.name,departments.id')->join('departments','departments.id=users.department_id')->where('users.id',$_REQUEST['user_id'])->get('users')->row_array();
	}

	function save_holiday(){
		$id = $this->input->post('holiday_id');
		$array = array(
			'name' => $this->input->post('name'),
			'date' => date('Y-m-d',strtotime($this->input->post('date'))),
			'company' => $this->input->post('company'),
			'note' => $this->input->post('note'),
		);
		if($id==''){
			$array['added_by']  = $this->session->userdata('id');
			$array['added_time'] = time();
			$this->db->insert('holiday_list',$array);
		}else{
			$array['updated_by']  = $this->session->userdata('id');
			$array['updated_time'] = time();
			$this->db->where('id',$id)->update('holiday_list',$array);
		}
	}

	function get_holiday_remote(){
		return $this->db->select('*')->where('id',$_REQUEST['holiday_id'])->get('holiday_list')->row_array();
	}

	function get_holiday_list(){
		return $this->db->select('*')->where('YEAR(date)',date('Y'))->get('holiday_list')->result();
	}

}

?>