<?php 

class Role_model extends CI_Model{		
	function __construct(){
		parent::__construct();
	}

	function get_active_roles(){
		return $this->db->select('id,name')->where('status',1)->get('roles')->result();
	}

	function get_roles(){
		return $this->db->select('id,name,status')->get('roles')->result();
	}

	function get_role_by_id($id){
		return $this->db->select("roles.id as id,roles.name as name,roles.status as status,GROUP_CONCAT(CONCAT(can_view,can_create,can_edit,can_delete,can_export)) as permissions")->from('roles')->join('role_permission','role_permission.role_id=roles.id')->where('roles.id',$id)->get()->row_array();
	}

	function save_role(){
		$id=$this->input->post('role_id');
		$array=array(
			"name"=>$this->input->post('name'),
			"status"=>$this->input->post('status')
		);
		if($id==null){
			$array["added_by"]=$this->session->userdata('id');
			$array["added_time"]=time();
			$this->db->insert('roles',$array);
			$role_id=$this->db->insert_id();
		}else{
			$array["updated_by"]=$this->session->userdata('id');
			$array["updated_time"]=time();
			$this->db->where('id',$id)->update('roles',$array);
			$role_id=$id;
		}

		$this->load->model('permissions_model');

		$permissions=$this->permissions_model->get_active_permissions();

		$users = $this->db->select('id')->from('users')->where('role_id',$id)->get()->result();

		foreach ($permissions as $permission) {
			if($this->input->post('can_view')!=''){
				$view_array = $this->input->post('can_view');
			}else{
				$view_array = array();
			}
			if($this->input->post('can_create')!=''){
				$create_array = $this->input->post('can_create');
			}else{
				$create_array = array();
			}
			if($this->input->post('can_edit')!=''){
				$edit_array = $this->input->post('can_edit');
			}else{
				$edit_array = array();
			}
			if($this->input->post('can_delete')!=''){
				$delete_array = $this->input->post('can_delete');
			}else{
				$delete_array = array();
			}
			if($this->input->post('can_export')!=''){
				$export_array = $this->input->post('can_export');
			}else{
				$export_array = array();
			}
			$can_view = array_key_exists($permission->id, $view_array) ?  1 : 0;
			$can_create = array_key_exists($permission->id, $create_array) ?  1 : 0;
			$can_edit = array_key_exists($permission->id, $edit_array) ?  1 : 0;
			$can_delete = array_key_exists($permission->id, $delete_array) ? 1 : 0;
			$can_export = array_key_exists($permission->id, $export_array) ? 1 : 0;

			$role_p = $this->db->select('id')->where('role_id',$id)->where('permission_id',$permission->id)->get('role_permission')->row_array();
			$array=array(
				"permission_id" => $permission->id,
				"can_view" => $can_view,
				"can_create" => $can_create,
				"can_edit" => $can_edit,
				"can_delete" => $can_delete,
				"can_export" => $can_export,
			);
			if($id==null || empty($role_p)){
				$array["role_id"] = $role_id;
				$array["added_by"]=$this->session->userdata('id');
				$array["added_time"]=time();
				$this->db->insert('role_permission',$array);
			}else{
				$array["role_id"] = $role_id;
				$array["updated_by"]=$this->session->userdata('id');
				$array["updated_time"]=time();
				$this->db->where('role_id',$id)->where('permission_id',$permission->id)->update('role_permission',$array);
			}
			if($this->input->post('user_check')==1){
				foreach ($users as $user) {
					$user_p = $this->db->select('id')->where('user_id',$user->id)->where('permission_id',$permission->id)->get('user_permission')->row_array();
					$u_array=array(
						"permission_id" => $permission->id,
						"can_view" => $can_view,
						"can_create" => $can_create,
						"can_edit" => $can_edit,
						"can_delete" => $can_delete,
						"can_export" => $can_export,
					);
					if(empty($user_p)){
							$u_array["user_id"]=$user->id;
							$u_array["added_by"]=$this->session->userdata('id');
							$u_array["added_time"]=time();
							$this->db->insert('user_permission',$u_array);
					}else{
						$u_array["updated_by"]=$this->session->userdata('id');
						$u_array["updated_time"]=time();
						$this->db->where('user_id',$user->id)->where('permission_id',$permission->id)->update('user_permission',$u_array);
					}
				}
			}
		}
	}

	function delete_role(){
		$id=$this->input->post('delete_role_id');
		// Role delete
		$this->db->where('id',$id)->delete('roles');

		// Role permission delete
		$this->db->where('role_id',$id)->delete('role_permission');

		$users = $this->db->select('id,profile_image')->from('users')->where('role_id',$id)->get()->result();

		// User permission delete
		foreach ($users as $user) {
			$this->db->where('user_id',$user->id)->delete('user_permission');

			// Timeoff request delete
			// $this->db->where('user_id',$user->id)->delete('timeoff_requests');

			if($user->profile_image!=null){
				unlink('./documents/users/profile_images/'.$user->profile_image);
			}
		}

		// Users delete in role
		$this->db->where('role_id',$id)->delete('users');
	}
}

?>