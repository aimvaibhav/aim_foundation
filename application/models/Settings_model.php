<?php 

class Settings_model extends CI_Model{		
	function __construct(){
		parent::__construct();
	}

	function save(){
		$data = $_POST;

		$company_logo = file_upload('company_logo','site_settings');
		if($company_logo !=''){
			unlink('./documents/site_settings/'.get_site_setting('company_logo'));
		}else{
			$company_logo = get_site_setting('company_logo');
		}
		$data['company_logo'] = $company_logo;

		$company_favicon = file_upload('company_favicon','site_settings');
		if($company_favicon !=''){
			unlink('./documents/site_settings/'.get_site_setting('company_favicon'));
		}else{
			$company_favicon = get_site_setting('company_favicon');
		}
		$data['company_favicon'] = $company_favicon;

		$privacy_policy = file_upload('privacy_policy','site_settings');
		if($privacy_policy !=''){
			unlink('./documents/site_settings/'.get_site_setting('privacy_policy'));
		}else{
			$privacy_policy = get_site_setting('privacy_policy');
		}
		$data['privacy_policy'] = $privacy_policy;

		$company_terms = file_upload('company_terms','site_settings');
		if($company_terms !=''){
			unlink('./documents/site_settings/'.get_site_setting('company_terms'));
		}else{
			$company_terms = get_site_setting('company_terms');
		}
		$data['company_terms'] = $company_terms;

		foreach ($data as $key => $value) {
			$result = $this->db->where('name',$key)->get('site_settings')->row_array();
			if(!empty($result)){
				$this->db->where('name',$key)->update('site_settings',array('value'=>$value));
			}else{
				$this->db->insert('site_settings',array($key=>$value));
			}
		}
	}


	function save_email_template(){
		if($this->input->post('status')==''){
			$status = 0;
		}else{
			$status = 1;
		}
		$array=array(
			'status' => $status,
			'title' => $this->input->post('title'),
			'subject' => $this->input->post('subject'),
			'message' => $this->input->post('message'),
		);
		if($this->input->post('et_id')==''){
			$array['added_by'] = $this->session->userdata('id');
			$array['added_time'] = time();
			$this->db->insert('email_templates',$array);
		}else{
			$array['updated_by'] = $this->session->userdata('id');
			$array['updated_time'] = time();
			$this->db->where('id',$this->input->post('et_id'))->update('email_templates',$array);
		}
		
	}

	function get_email_templates(){
		return $this->db->select('id,status,title,subject,message')->get('email_templates')->result();
	}

	function get_email_template_remote(){
		return $this->db->select('id,status,title,subject,message')->where('id',$_REQUEST['et_id'])->get('email_templates')->row_array();
	}
	
}
?>