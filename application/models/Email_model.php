<?php 

class Email_model extends CI_Model{		
	function __construct(){
		parent::__construct();
		$this->load->library('email');
	}

	function send_email($to,$subject,$message,$bcc="",$cc=""){
		$this->email->from('aimnet@aimtron.com')->to($to)->cc($cc)->bcc($bcc)->subject($subject)->message($message)->send();
	}

}

?>