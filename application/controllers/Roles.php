<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends MY_Controller {
	protected $as;
	public function __construct(){
		parent::__construct();
		$this->load->model('role_model');
		$this->as=$this->access->can_access();
	}

	public function index(){
		$roles=array_search('roles', array_column($this->as, 'param')); 
		if($this->as[$roles]['can_view']==1){
			$data['roles'] = $this->role_model->get_roles();
			$this->template->set_page_title("Roles");
			$this->template->loadContent("roles/index", $data);
		}else{
			redirect(base_url());
		}
	}

	function add_role(){
		$roles=array_search('roles', array_column($this->as, 'param')); 
    	if($this->as[$roles]['can_create']==1){
			$this->load->model('permissions_model');
			$data['permissions']=$this->permissions_model->get_active_permissions();
			$data['role']=array();
			$this->template->set_page_title("Add Role");
			$this->template->loadContent("roles/form", $data);
		}else{
			redirect(base_url());
		}
	}

	function edit_role($id=""){
		$roles=array_search('roles', array_column($this->as, 'param')); 
    	if($this->as[$roles]['can_edit']==1){
			$this->load->model('permissions_model');
			$data['permissions']=$this->permissions_model->get_active_permissions();
			$data['role']=$this->role_model->get_role_by_id($id);
			$this->template->set_page_title("Edit Role");
			$this->template->loadContent("roles/form", $data);
		}else{
			redirect(base_url());
		}
	}

	function save_role(){
		$this->role_model->save_role();
		$this->session->set_flashdata('success','Role details saved successfully !!');
		redirect(base_url('roles'));
	}

	function get_remote_role_permissions(){
		$this->load->model('permissions_model');
		$data["permissions"]=$this->permissions_model->get_remote_role_permissions();
		echo json_encode($data);
	}

	function delete_role(){
		$this->role_model->delete_role();
		$this->session->set_flashdata('success','Role deleted successfully !!');
		redirect(base_url('roles'));
	}

}
?>