<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	protected $as;
	public function __construct(){
		parent::__construct();
		$this->load->model('home_model');
		$this->as=$this->access->can_access();
	}

	public function index(){
		$this->template->set_page_title("Home");
		$this->template->loadContent("home/index", array());
	}

}
?>