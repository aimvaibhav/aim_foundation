<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends MY_Controller {
	public function __construct(){
		parent::__construct();
	}

	function index(){
		$this->template->set_page_title('Page Not Found');
		$this->template->loadContent('errors/page_not_found',array());
	}

}

?>