<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		if($this->login_model->check_login_status()==true){
			redirect(base_url('home'));
		}else{
			if(isset($_POST['submit'])){
				$username=$this->input->post('username');
				$password=$this->input->post('password');
				$check = $this->login_model->do_login($username,$password);
				if($check===true){
					$this->session->set_flashdata('welcome','Welcome back '.$this->session->userdata('name').' !!');
					$this->session->set_flashdata('success','Login successfully !!');
					redirect(base_url('home'));
				}else if($check===false){
					$this->session->set_flashdata('error','Username or password is incorrect !!');
					redirect(base_url('login'));
				}else{
					$this->session->set_userdata('expire_username',$username);
					$this->session->set_flashdata('error','Your password is expired !! Please reset your password !!');
					redirect(base_url('login/expire_reset_password'));
				}
			}else{
				$company_name = get_site_setting('company_name');
				$this->template->set_page_title($company_name);
				$this->template->set_layout('layout/login_layout');
				$this->template->loadContent('login/index',array());
			}
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login/logout_red'));
	}

	function logout_red(){
		$this->session->set_flashdata('success','Logout successfully !!');
		redirect(base_url());
	}

	function forgot(){
		if($this->login_model->check_login_status()==true){
			redirect(base_url('home'));
		}else{
			$this->template->set_page_title('AIMTRON CORPORATION');
			$this->template->set_layout('layout/login_layout');
			$this->template->loadContent('login/forgot',array());
		}
	}

	function send_forgot_password_link(){
		$check = $this->login_model->send_forgot_password_link();
		if($check === true){
			$this->session->set_flashdata('success','Forgot password link send successfully to your email !!');
		}else{
			$this->session->set_flashdata('error','We didn\'t find your email address in our system !!');
		}
		redirect(base_url(('login')));
	}

	function change_password($forgot_key=''){
		$check = $this->login_model->check_forgot_password_key($forgot_key);
		if(!empty($check)){
			$data['forgot_key'] = $forgot_key;
			$this->template->set_page_title('AIMTRON CORPORATION');
			$this->template->set_layout('layout/login_layout');
			$this->template->loadContent('login/change_password',$data);
		}else{
			$this->session->set_flashdata('error','Your link is expire please get the key and try again !!');
			redirect(base_url('login'));
		}
	}

	function reset_password(){
		$this->login_model->reset_password();
		$this->session->set_flashdata('success','Your password reset successfully !!');
		redirect(base_url('login'));
	}

	function expire_reset_password(){
		$this->template->set_page_title('AIMTRON CORPORATION');
		$this->template->set_layout('layout/login_layout');
		$this->template->loadContent('login/expire_reset_password',array());
	}

	function expire_password_update(){
		$this->login_model->expire_password_update();
		$this->session->set_flashdata('success','Your password changed successfully !!');
		redirect(base_url('login'));
	}
}
