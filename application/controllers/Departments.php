<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departments extends MY_Controller {
	protected $as;
	public function __construct(){
		parent::__construct();
		$this->load->model('department_model');
		$this->as=$this->access->can_access();
	}

	public function index(){
		$departments=array_search('departments', array_column($this->as, 'param')); 
		if($this->as[$departments]['can_view']==1){
			$this->load->model('user_model');
			$data['users'] = $this->user_model->get_active_users();
			$data['departments'] = $this->department_model->get_departments();
			$this->template->set_page_title("Departments");
			$this->template->loadContent("department/index", $data);
		}else{
			redirect(base_url());
		}
	}

	function save_department(){
		$this->department_model->save_department();
		$this->session->set_flashdata('success','Department saved successfully !!');
		redirect(base_url('departments'));
	}

	function get_department_remote(){
		$data['department']=$this->department_model->get_department_remote();
		echo json_encode($data);
	}

	function delete_department(){
		$this->department_model->delete_department();
		$this->session->set_userdata('success','Department deleted successfully !!');
		redirect(base_url('departments'));
	}

}
?>