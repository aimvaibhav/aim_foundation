<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr extends MY_Controller {
	protected $as;
	function __construct(){
		parent::__construct();
		$this->load->model('hr_model');
		$this->as=$this->access->can_access();
	}

	function time_off_request(){
		$data['users'] = $this->hr_model->get_active_users();
		$this->template->set_page_title("Time off requests");
		$this->template->loadContent("hr/time-off-requests/index", $data);
	}

	function get_user_department(){
		$data['department'] = $this->hr_model->get_user_department();
		echo json_encode($data);
	}

	function time_off_request_approved_list(){
		$time_off_requests_list=array_search('time_off_requests_list', array_column($this->as, 'param')); 
		if($this->as[$time_off_requests_list]['can_view']==1){
			$data['requests']=$this->hr_model->get_time_off_request_approved_list();
			$this->template->set_page_title("Time off request Approved List");
			$this->template->loadContent("hr/time-off-requests/approved_list", $data);
		}else{
			redirect(base_url());
		}
	}

	function time_off_request_pending_list(){
		$time_off_requests_list=array_search('time_off_requests_list', array_column($this->as, 'param')); 
		if($this->as[$time_off_requests_list]['can_view']==1){
			$data['requests']=$this->hr_model->get_time_off_request_pending_list();
			$this->template->set_page_title("Time off request Pending List");
			$this->template->loadContent("hr/time-off-requests/pending_list", $data);
		}else{
			redirect(base_url());
		}
	}

	function my_time_off_request(){
		$data['requests']=$this->hr_model->get_my_time_off_requests();
		$this->template->set_page_title("My Time off requests");
		$this->template->loadContent("hr/time-off-requests/my", $data);
	}

	function save_time_off_request(){
		$this->hr_model->save_time_off_request();
		$this->session->set_flashdata('success','Time off request saved successfully !!');
		redirect(base_url('hr/time_off_request'));
	}

	function delete_timeoff_request(){
		$this->hr_model->delete_timeoff_request();
		$this->session->set_flashdata('success','Timeoff request deleted successfully !!');
		redirect(base_url('hr/time_off_request_list'));
	}

	function cancel_timeoff_request(){
		$this->hr_model->cancel_timeoff_request();
		$this->session->set_flashdata('success','Your timeoff request canceled successfully !!');
		redirect(base_url('hr/my_time_off_request'));
	}

	function get_timeoff_request_detail(){
		$data['request']=$this->hr_model->get_timeoff_request_detail();
		echo json_encode($data);
	}

	function time_off_approval($person,$timeoff_id){
		$data['time_off']=$this->hr_model->get_timeoff_data($timeoff_id);
		$data['person']=$person;
		if($data["time_off"]["status"]==0 && $person == 'supervisor'){
			$this->template->set_page_title("Timeoff Request Approval For".$data['time_off']["user_name"]);
			$this->template->loadContent("hr/time-off-requests/approval", $data);
		}else if(($data["time_off"]["status"]==0 || $data["time_off"]["status"]==1) && $person=='hr'){
			$this->template->set_page_title("Timeoff Request Approval For".$data['time_off']["user_name"]);
			$this->template->loadContent("hr/time-off-requests/approval", $data);
		}else{
			redirect(base_url());
		}
	}

	function update_time_off_request_approval(){
		$this->hr_model->update_time_off_request_approval();
		redirect(base_url());
	}

	function decline_time_off_request_approval(){
		$this->hr_model->decline_time_off_request_approval();
		redirect(base_url());
	}

	function approval_timeoff_request(){
		$this->hr_model->approval_timeoff_request();
		$this->session->set_flashdata('success','Timeoff request status changed successfully !!');
		redirect(base_url('hr/time_off_request_pending_list'));
	}

	function time_off_calendar(){
		$time_off_calendar=array_search('time_off_calendar', array_column($this->as, 'param')); 
		if($this->as[$time_off_calendar]['can_view']==1){
			$data['requests']=$this->hr_model->get_approved_time_off_requests();
			$data['holidays']=$this->hr_model->get_holiday_list();
			$this->template->set_page_title("Time off calendar");
			$this->template->loadContent("hr/time-off-requests/calendar", $data);
		}else{
			redirect(base_url());
		}
	}

	function holiday_list(){
		$data['holidays']=$this->hr_model->get_holiday_list();
		$this->template->set_page_title("Holiday List");
		$this->template->loadContent("hr/holiday_list", $data);
	}

	function save_holiday(){
		$this->hr_model->save_holiday();
		$this->session->set_flashdata('success','Holiday saved successfully !!');
		redirect(base_url('hr/holiday_list'));
	}

	function get_holiday_remote(){
		$data['holiday'] = $this->hr_model->get_holiday_remote();
		echo json_encode($data);
	}
}
?>