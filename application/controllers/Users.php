<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {
	protected $as;
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->as=$this->access->can_access();
	}

	public function index(){
		$users=array_search('users', array_column($this->as, 'param')); 
		if($this->as[$users]['can_view']==1){
			$this->template->set_page_title("Users");
			$data['users'] = $this->user_model->get_users();
			$this->template->loadContent("users/index", $data);
		}else{
			redirect(base_url());
		}
	}

	function add_user(){
		$users=array_search('users', array_column($this->as, 'param')); 
    	if($this->as[$users]['can_create']==1){
			$this->load->model('role_model');
			$data['roles']=$this->role_model->get_active_roles();
			$this->load->model('department_model');
			$data['departments']=$this->department_model->get_active_departments();
			$this->load->model('permissions_model');
			$data['permissions']=$this->permissions_model->get_active_permissions();
			$data['user']=array();
			$data['users'] = $this->user_model->get_users();
			$this->template->set_page_title("Add User");
			$this->template->loadContent("users/form", $data);
		}else{
			redirect(base_url());
		}
	}

	function edit_user($id=''){
		$users=array_search('users', array_column($this->as, 'param')); 
    	if($this->as[$users]['can_edit']==1){
			$this->load->model('role_model');
			$data['roles']=$this->role_model->get_active_roles();
			$this->load->model('department_model');
			$data['departments']=$this->department_model->get_active_departments();
			$this->load->model('permissions_model');
			$data['permissions']=$this->permissions_model->get_active_permissions();
			$data['user']=$this->user_model->get_user_by_id($id);
			$data['users'] = $this->user_model->get_users();
			$this->template->set_page_title("Edit User");
			$this->template->loadContent("users/form", $data);
		}else{
			redirect(base_url());
		}
	}

	function save_user(){
		$this->user_model->save_user();
		$this->session->set_flashdata('success','User saved successfully !!');
		redirect(base_url('users'));
	}

	function check_username(){
		$check = $this->user_model->check_username();
		if($check==true){
			echo "true";
		}elseif($check==false){
			echo "false";
		}
	}

	function check_email(){
		$check = $this->user_model->check_email();
		if($check==true){
			echo "true";
		}elseif($check==false){
			echo "false";
		}
	}

	function check_emp_code(){
		$check = $this->user_model->check_emp_code();
		if($check==true){
			echo "true";
		}elseif($check==false){
			echo "false";
		}
	}

	function delete_user(){
		$this->user_model->delete_user();
		$this->session->set_flashdata('success','User deleted successfully !!');
		redirect(base_url('users'));
	}


	function reset_user_password(){
		$this->user_model->reset_user_password();
		$this->session->set_flashdata('success','User password reset successfully !!');
		redirect(base_url('users'));
	}

	function set_user_status_filter(){
		$this->session->set_userdata('user_status',$_REQUEST['status']);
	}

	function set_user_location_filter(){
		$this->session->set_userdata('users_location_filter',$_REQUEST['location']);
	}

	function get_user_remote_data(){
		$data['user'] = $this->user_model->get_user_by_id($_REQUEST['user_id']);
		echo json_encode($data);
	}

}
?>