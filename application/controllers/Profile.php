<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function index(){
		$this->template->set_page_title($this->session->userdata('name'));
		$this->template->loadContent("profile/index", array());
	}

	function profile_pic_upload(){
		$this->profile_model->profile_pic_upload();
		$this->session->set_flashdata('success','Profile pic uploaded successfully.');
		redirect(base_url('profile'));
	}

	function change_password(){
		$check=$this->profile_model->change_password();
		if($check==true){
			$this->session->set_flashdata('success','Password changed successfully !!');
		}else{
			$this->session->set_flashdata('error','Something went wrong... maybe, your old password is wrong !!');
		}
		redirect(base_url('profile'));
	}
	
	function save_detail(){
		$this->profile_model->save_detail();
		$this->session->set_flashdata('success','Profile detail saved successfully.');
		redirect(base_url('profile'));
	}

}
?>