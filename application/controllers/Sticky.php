<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sticky extends MY_Controller {
	protected $as;
	public function __construct(){
		parent::__construct();
		$this->load->model('sticky_model');
		$this->as=$this->access->can_access();
	}

	public function index(){
		$data['sticky_notes'] = $this->sticky_model->get_sticky_notes();
		$data['users'] = $this->sticky_model->get_users();	
		$this->template->set_page_title("Sticky Notes");
		$this->template->loadContent("sticky/index",$data);
	}

	function getstickydetail(){
		$data['notes'] = $this->sticky_model->getstickydetail();
		echo json_encode($data);

	}

	function addnote(){
		$this->sticky_model->addnote();
		$this->session->set_flashdata('success','Sticky Note Added Successfully');
		redirect(base_url().'sticky');
	}

	function delete_note(){
	  	$this->sticky_model->updatenote();
	  	$this->session->set_flashdata('success','Sticky Note Deleted Successfully');
	  	redirect(base_url().'sticky');
	}

	function sharenote(){
	    $this->sticky_model->sharenote();
	    $this->session->set_flashdata('success','Sticky Note shared Successfully');
	    redirect(base_url().'sticky');
  }

}

?>