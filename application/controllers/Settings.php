<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {
	protected $as;
	function __construct(){
		parent::__construct();
		$this->load->model('settings_model');
		$this->as=$this->access->can_access();
	}

	public function index(){
		$settings=array_search('settings', array_column($this->as, 'param')); 
		if($this->as[$settings]['can_view']==1){
			$this->template->set_page_title("Basic Details");
			$this->template->loadContent("settings/index", array());
		}else{
			redirect(base_url());
		}
	}

	function email_templates(){
		$settings=array_search('settings', array_column($this->as, 'param')); 
		if($this->as[$settings]['can_view']==1){
			$data['templates'] = $this->settings_model->get_email_templates();
			$this->template->set_page_title("Email Templates");
			$this->template->loadContent("settings/email_templates", $data);
		}else{
			redirect(base_url());
		}
	}

	function save(){
		$this->settings_model->save();
		$this->session->set_flashdata('success','Setting detail saved successfully !!');
		redirect(base_url('settings'));
	}


	function save_email_template(){
		$this->settings_model->save_email_template();
		$this->session->set_flashdata('success','Email template saved successfully !!');
		redirect(base_url('settings/email_templates'));
	}

	function get_email_template_remote(){
		$data['template'] = $this->settings_model->get_email_template_remote();
		echo json_encode($data);
	}
}

?>