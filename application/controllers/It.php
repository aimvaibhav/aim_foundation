<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class It extends MY_Controller {
	protected $as;
	function __construct(){
		parent::__construct();
		$this->load->model('it_model');
		$this->as=$this->access->can_access();
	}

	function permission_items(){
		$data['permission_items'] = $this->it_model->get_all_permission_for_permission_items();
		$this->template->set_page_title("IT Permission Items");
		$this->template->loadContent("it/permission_items",$data);
		
	}

	function save_permission_items(){
		$this->it_model->save_permission_items();
		$this->session->set_flashdata('success','Permission items added successfully !!');
		redirect(base_url('it/permission_items'));
	}

	function save_permission_access(){
		$this->it_model->save_permission_access();
		$this->session->set_flashdata('success','Permission Update successfully !!');
		redirect(base_url('it/permission_items'));
	}

	function get_permission_items_remote(){
		$data['permission_item'] = $this->it_model->get_permission_items_remote();
		$html = '';
		$json_data = json_decode($data['permission_item']['item']);
		if (!empty($json_data)) {
			foreach ($json_data as $json) {
				$html.= '
					<tr>
						<td>'.$json->access.'</td>
						<td>'.$json->item.'</td>
						<td><a href="delete_permission_json_item/'.$data['permission_item']['id'].'/'.$json->id.'"><i class="la la-trash"></i></a></td>
					</tr>
				';
			}
		}else{
			$html = "<div class='text-center text-danger'>Table is empty</div>";
		}
		$data['html'] = $html;
		echo json_encode($data);
	}

	function delete_permission_json_item($id,$json_id){
		$this->it_model->delete_permission_json_item($id,$json_id);
		$this->session->set_flashdata('success','Item Delete successfully !!');
		redirect(base_url('it/permission_items'));
	}

}

?>