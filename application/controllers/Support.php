<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends MY_Controller {
	protected $as;
	function __construct(){
		parent::__construct();
		$this->load->model('support_model');
		$this->as=$this->access->can_access();
  }

  function add_ticket(){
    $this->load->model('user_model');
    $data['users']=$this->user_model->get_active_users();
  	$this->template->set_page_title("Add Ticket");
		$this->template->loadContent("support/add_ticket", $data);
  }

  function my_tickets(){
  	$data['tickets'] = $this->support_model->get_my_tickets();
    $this->load->model('user_model');
    $data['users']=$this->user_model->get_active_users();
  	$this->template->set_page_title("My Tickets");
		$this->template->loadContent("support/my_tickets", $data);
  }

  function ticket_detail($id){
    $data['mytickets'] = $this->support_model->get_ticket_by_id($id);
    $data['ticket_comments'] = $this->support_model->get_ticket_commment_by_id($id);
    $data['ticket_documents'] = $this->support_model->get_ticket_documents_by_id($id);
    $this->template->set_page_title("Ticket Detail");
    $this->template->loadContent("support/ticket_detail", $data);
  }

  function save_comment(){
    $id = $this->input->post('ticket_id');
    $this->support_model->save_comment();
    $this->session->set_flashdata('success','Comment submitted successfully !!');
    redirect(base_url('support/ticket_detail/'.$id.''));
  }
  function edit_comment(){
    $id = $this->input->post('edit_ticket_id');
    $this->support_model->save_comment();
    $this->session->set_flashdata('success','Comment submitted successfully !!');
    redirect(base_url('support/ticket_detail/'.$id.''));
  }

  function delete_comment(){
    $id = $this->input->post('myticket_id');
    $this->support_model->delete_comment();
    $this->session->set_flashdata('success','Document deleted successfully !!');
    redirect(base_url('support/ticket_detail/'.$id.''));
  }

  function upload_ticket_document(){
    $this->support_model->upload_ticket_document();
    return true;
  }

  function delete_document(){
    $id = $this->input->post('ticket_document_id');
    $this->support_model->delete_document();
    $this->session->set_flashdata('success','Comment deleted successfully !!');
    redirect(base_url('support/ticket_detail/'.$id.''));
  }

  function all_tickets(){
  	$settings=array_search('settings', array_column($this->as, 'param')); 
		if($this->as[$settings]['can_view']==1){
	  	$data['tickets'] = $this->support_model->get_all_tickets();
	  	$data['users'] = $this->support_model->get_active_uses();
	  	$this->template->set_page_title("All Tickets");
			$this->template->loadContent("support/all_tickets", $data);
		}else{
			redirect(base_url());
		}
  }

  function assign_tickets(){
  	$settings=array_search('settings', array_column($this->as, 'param')); 
		if($this->as[$settings]['can_view']==1){
	  	$data['tickets'] = $this->support_model->get_assigned_tickets();
	  	$this->template->set_page_title("Assign Tickets");
			$this->template->loadContent("support/assign_tickets", $data);
		}else{
			redirect(base_url());
		}
  }

  function save_ticket(){
  	$this->support_model->save_ticket();
  	$this->session->set_flashdata('success','Your ticket submitted successfully !!');
  	redirect($_SERVER['HTTP_REFERER']);
  }

  function get_ticket_remote(){
  	$data['ticket'] = $this->support_model->get_ticket_remote();
  	echo json_encode($data);
  }

  function get_ticket_comment_remote(){
    $data['ticket_comment'] = $this->support_model->get_ticket_comment_remote();
    echo json_encode($data);
  }

  function assign_ticket(){
  	$this->support_model->assign_ticket();
  	$this->session->set_flashdata('success','Person assign to ticket !!');
  	redirect(base_url('support/all_tickets'));
  }

  function start_ticket(){
  	$this->support_model->start_ticket();
  	$this->session->set_flashdata('success','Working start on ticket !!');
  	redirect(base_url('support/assign_tickets'));
  }

  function resolve_ticket(){
  	$this->support_model->resolve_ticket();
  	$this->session->set_flashdata('success','Ticket resolve successfully !!');
  	redirect(base_url('support/assign_tickets'));
  }

  function reopen_ticket(){
  	$this->support_model->reopen_ticket();
  	$this->session->set_flashdata('success','Ticket reopened successfully !!');
  	redirect(base_url('support/my_tickets'));
  }

  function close_ticket(){
  	$this->support_model->close_ticket();
  	$this->session->set_flashdata('success','Ticket closed successfully !!');
  	redirect(base_url('support/my_tickets'));
  }

}
?>