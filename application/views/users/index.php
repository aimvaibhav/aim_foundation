<style type="text/css">
	#qrcode > canvas{
		width: 100%;
	}
</style>
<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Users</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					IT 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Users 
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<?php 
		      $access=$this->access->can_access(); 
		      $users_access=array_search('users', array_column($access, 'param')); 
		      if($access[$users_access]['can_create']==1){
		    ?>
			<a href="<?php echo base_url('users/add_user') ?>" class="btn btn-brand btn-icon-sm">
				<i class="flaticon2-plus"></i> Add New
			</a>
			<?php } ?>
		</div>
	</div>
</div>
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div class="text-center">
				<button type="button" class="btn btn-primary <?php if($this->session->userdata('user_status')=='' || $this->session->userdata('user_status')==1){ echo 'btn-lg'; } ?>" onclick="user_status_filter('1');">Active</button>
				<button type="button" class="btn btn-danger <?php if($this->session->userdata('user_status')==2){ echo 'btn-lg'; } ?>" onclick="user_status_filter('2');">Inactive</button>
			</div>
			<div class="row margin-top-10">
		        <div class="col-md-12 text-center">
		          <?php $check = $this->session->userdata('users_location_filter'); ?>
		          <button type="button" class="btn btn-success <?php if($check=='All' || $check==''){ echo 'btn-lg'; } ?>" onclick="setlocationfilter('All');">
		            All
		          </button>
		          <button type="button" class="btn btn-warning <?php if($check=='Corporation'){ echo 'btn-lg'; } ?>" onclick="setlocationfilter('Corporation');">
		            Coporation
		          </button>
		          <button type="button" class="btn btn-primary <?php if($check=='India'){ echo 'btn-lg'; } ?>" onclick="setlocationfilter('India');">
		            India
		          </button>
		        </div>
      		</div>
			<table class="table table-bordered table-striped" id="ajax_data">
				<thead>
					<th>Employee#</th>
					<th>User</th>
					<th>Username</th>
					<th>Department</th>
					<th>Role</th>
					<th>Phone</th>
					<th>Status</th>
					<th>Actions</th>
				</thead>
				<tbody>
					<?php foreach ($users as $user) { ?>
					<tr>
						<td><?php echo $user->emp_code; ?></td>
						<td>
							<?php if($user->profile_image==''){ ?>
								<div class="kt-user-card-v2">
									<div class="kt-user-card-v2__pic">
										<img src="<?php echo base_url(); ?>documents/users/profile_images/default.jpg" >
									</div>
									<div class="kt-user-card-v2__details">
										<span class="kt-user-card-v2__name"><?php echo $user->name; ?></span>
										<a class="kt-user-card-v2__email kt-link" href="mailto:<?php echo $user->email; ?>">
											<?php echo $user->email; ?>
										</a>
									</div>
								<div>
							<?php }else{ ?>
								<div class="kt-user-card-v2">
									<div class="kt-user-card-v2__pic">
										<img src="<?php echo base_url(); ?>documents/users/profile_images/<?php echo $user->profile_image; ?>" >
									</div>
									<div class="kt-user-card-v2__details">
										<span class="kt-user-card-v2__name"><?php echo $user->name; ?></span>
										<a class="kt-user-card-v2__email kt-link" href="mailto:<?php echo $user->email; ?>">
											<?php echo $user->email; ?>
										</a>
									</div>
								</div>
							<?php } ?>
						</td>
						<td>
							<?php echo $user->username; ?>
						</td>
						<td>
							<?php echo $user->department_name; ?>
						</td>
						<td>
							<?php echo $user->role_name; ?>
						</td>
						<td>
							<?php echo $user->phone; ?>
						</td>
						<td>
							<?php
								if($user->status==1){
									echo '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Active</span>';
								}else{
									echo '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Inactive</span>';
								}
							?>
						</td>
						<td>
							<?php if($access[$users_access]['can_edit']==1){ ?>
								<a href="<?php echo base_url(); ?>users/edit_user/<?php echo $user->id; ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">
									<i class="la la-edit"></i>
								</a>
								<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md reset-action" data-user-name="<?php echo $user->name; ?>" title="Reset Password" data-id="<?php echo $user->id; ?>">
									<i class="la la-unlock-alt"></i>
								</a>
							<?php } ?>
							<?php if($access[$users_access]['can_delete']==1){ ?>
								<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md delete-action" title="Delete" data-id="<?php echo $user->id; ?>">
									<i class="la la-trash"></i>
								</a>
							<?php } ?>
							<?php if($access[$users_access]['can_view']==1){ ?>
								<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md card-action" title="Print ID Card" data-id="<?php echo $user->id; ?>">
									<i class="la la-print"></i>
								</a>
							<?php } ?>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_user_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Delete User
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="delete_user_form" method="post" action="<?php echo base_url('users/delete_user'); ?>">
					<input type="hidden" name="delete_user_id" id="delete_user_id">
					<div class="alert alert-warning" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">This action is affect in many other places and all data will be deleted after this action.</div>
        	</div>
          <h4 class="text-center">Are you sure you still want to delete this user ?</h4>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#delete_user_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="id_card_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					User ID Card
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div id="print_area">
					<div class="row">
						<div class="col-md-6" style="width: 100%; text-align: center;">
							<img src="<?php echo base_url('assets/img/id_card_frontend.jpg'); ?>" id="frontend_img" width="100%">
							<span id="id_card_frontend_img"></span>
							<span id="id_card_frontend_name"></span>
							<span id="id_card_frontend_last_name"></span>
							<span id="id_card_frontend_designation"></span>
							<span id="id_card_frontend_designation_2"></span>
							<span id="id_card_frontend_barcode"></span>
						</div>
						<div class="col-md-6" style="width: 100%; text-align: center;">
							<img src="<?php echo base_url('assets/img/id_card_backend.jpg'); ?>" id="backend_img" width="100%">
							<span id="id_card_backend_name"></span>
							<span id="id_card_backend_last_name"></span>
							<span id="id_card_backend_designation"></span>
							<span id="id_card_backend_designation_2"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="print_card();">Print</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="rp_user_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Reset password for <span id="rp_user_name"></span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="reset_user_password_form" method="post" action="<?php echo base_url('users/reset_user_password'); ?>">
					<input type="hidden" name="rp_user_id" id="rp_user_id">
					<div class="alert alert-warning" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">Are you sure you want to reset password for this user.</div>
        	</div>
          <div class="form-group">
						<label>Password</label>
						<input type="password" name="password" id="rp_password" class="form-control">
					</div>
					<div class="form-group">
						<label>Confirm Password</label>
						<input type="password" name="confirm_password" id="rp_confirm_password" class="form-control">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#reset_user_password_form').submit();">Reset</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		<?php if($access[$users_access]['can_export']==1){ ?>
		    var export_button = `<'col-sm-6 text-right'B>`;
		<?php }else{ ?>
		    var export_button = '';
		<?php } ?>
		$('#ajax_data').DataTable({
			dom: `<'row'<'col-sm-6 text-left'f>`+export_button+`>
				<'row'<'col-sm-12'tr>>
				<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			pageLength: 100,
			buttons:["print","copyHtml5","excelHtml5","csvHtml5"],
		    fixedHeader: {
		      header: true,
		      footer: true
		    },
		    responsive: true
		});
		$("#reset_user_password_form").validate ({
		    rules: {
		      password:{
		        required : true,
		      },
		      confirm_password:{
		        required : true,
		        equalTo:"#rp_password",
		      },
		    }, 
		    highlight: function(element) {
		      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		    },
		    success: function(element) {
		      element.closest('.form-group').removeClass('has-error').addClass('has-success');
		      $(element).closest('.error').remove();
		    }
			});
		$('#ajax_data').on('click','.delete-action',function(){
			$('#delete_user_id').val($(this).attr("data-id"));
      		$('#delete_user_model').modal('show');
		});
		$('#ajax_data').on('click','.card-action',function(){
			var id = $(this).attr("data-id");
			$.ajax({
			    type:'POST',   
			    url: "<?php echo base_url('users/get_user_remote_data'); ?>",
			    data: {"user_id":id},
			    success: function(data){
			    	var json = jQuery.parseJSON(data);
			        $('#id_card_frontend_name').html(json.user.name);
			        $('#id_card_frontend_last_name').html(json.user.last_name);
			        $('#id_card_frontend_designation').html(json.user.designation);
			        $('#id_card_frontend_designation_2').html(json.user.designation_2);
			        if(json.user.card_profile_image==null){
			        	$('#id_card_frontend_img').html('<img style="border-radius: 50%;" src="<?php echo base_url(); ?>documents/users/profile_images/default.jpg" >');
			    	}else{
			    		var card_img_link = '<?php echo base_url('documents/users/profile_images/'); ?>'+json.user.card_profile_image;
						$('#id_card_frontend_img').html('<img style="border-radius: 50%;" src="'+card_img_link+'">');
					}
					var link = '<?php echo base_url('assets/vendors/barcode/barcode_show.php?code='); ?>'+json.user.emp_code;
					$('#id_card_frontend_barcode').html('<img src="'+link+'">');
					$('#id_card_backend_name').html(json.user.name);
					$('#id_card_backend_last_name').html(json.user.last_name);
			        $('#id_card_backend_designation').html(json.user.designation);
			        $('#id_card_backend_designation_2').html(json.user.designation_2);
			    }
			  });
      		$('#id_card_model').modal('show');
		});
		$('#ajax_data').on('click','.reset-action',function(){
			$('#rp_user_name').html("<b>\""+$(this).attr("data-user-name")+"\"</b>");
			$('#rp_user_id').val($(this).attr("data-id"));
      		$('#rp_user_model').modal('show');
		});
	});
	function user_status_filter(status){
		$.ajax({
	    type:'POST',   
	    url: "<?php echo base_url('users/set_user_status_filter'); ?>",
	    data: {"status":status},
	    success: function(data){
	    	window.location.reload();
	    }
	  });
	}
	function setlocationfilter(location){
		$.ajax({
		    type:'POST',   
		    url: "<?php echo base_url('users/set_user_location_filter'); ?>",
		    data: {"location":location},
		    success: function(data){
		    	window.location.reload();
		    }
		});
	}
	function print_card(){
		var prtContent = document.getElementById("print_area");
		var WinPrint = window.open('', '', 'left=0,top=0,width=1000,height=500,toolbar=0,scrollbars=0,status=0');
		WinPrint.document.write('<html><head><title></title>');
	    WinPrint.document.write('<link rel="stylesheet" href="<?php echo base_url('assets/css/demo1/style.bundle.css'); ?>" type="text/css" />');
	    WinPrint.document.write('<link rel="stylesheet" href="<?php echo base_url('assets/css/demo1/print.css'); ?>" type="text/css" />');
	    WinPrint.document.write('</head><body >');
	    WinPrint.document.write(prtContent.innerHTML);
      	WinPrint.document.write('</body></html>');
		WinPrint.document.close();
		WinPrint.focus();
		setTimeout(function() {
			WinPrint.print();
			WinPrint.close();
		}, 1000);
	}
</script>