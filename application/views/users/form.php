<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title"><?php echo empty($user) ? "Add" : "Edit"; ?> User</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="<?php echo base_url('users'); ?>" class="kt-subheader__breadcrumbs-link">
					Users 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="#" class="kt-subheader__breadcrumbs-link">
					<?php echo empty($user) ? "Add" : "Edit"; ?> User 
				</a>
			</div>
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<form method="post" action="<?php echo base_url('users/save_user') ?>" id="user_form" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-6">
						<input type="hidden" name="user_id" id="user_id" value="<?php if(isset($user['id'])){ echo $user['id']; } ?>">
						<div class="form-group">
							<label class="kt-checkbox">
								<input type="checkbox" name="status" id="user_status" value="1" <?php if(empty($user)){ echo 'checked'; }else{  if($user['status'] == 1){ echo 'checked'; } } ?>> Active
								<span></span>
							</label>
						</div>
						<div class="form-group">
							<label>Joining Date</label>
							<input type="text" name="joining_date" id="joining_date" class="form-control" value="<?php if(isset($user['joining_date'])){ if($user['joining_date']!=''){ echo date('m/d/Y',strtotime($user['joining_date'])); } } ?>">
						</div>
						<div class="form-group">
							<label>First Name</label>
							<input type="text" name="name" id="user_name" class="form-control" value="<?php if(isset($user['name'])){ echo $user['name']; } ?>">
						</div>
						<div class="form-group">
							<label>Last Name</label>
							<input type="text" name="last_name" id="user_last_name" class="form-control" value="<?php if(isset($user['last_name'])){ echo $user['last_name']; } ?>">
						</div>
						<div class="form-group">
							<label>Designation</label>
							<input type="text" name="designation" id="user_designation" class="form-control" value="<?php if(isset($user['designation'])){ echo $user['designation']; } ?>">
						</div>
						<div class="form-group">
							<label>Designation 2</label>
							<input type="text" name="designation_2" id="user_designation_2" class="form-control" value="<?php if(isset($user['designation_2'])){ echo $user['designation_2']; } ?>">
						</div>
						<div class="form-group">
							<label>Employee Code</label>
							<input type="text" name="emp_code" id="user_emp_code" class="form-control" value="<?php if(isset($user['emp_code'])){ echo $user['emp_code']; } ?>">
						</div>
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" id="user_username" class="form-control" value="<?php if(isset($user['username'])){ echo $user['username']; } ?>">
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="text" name="email" id="user_email" class="form-control" value="<?php if(isset($user['email'])){ echo $user['email']; } ?>">
						</div>
						<div class="form-group">
							<label>Ref. Email</label>
							<input type="text" name="ref_email" id="ref_email" class="form-control" value="<?php if(isset($user['ref_email'])){ echo $user['ref_email']; } ?>">
						</div>
						<div class="form-group">
							<label>Phone</label>
							<input type="text" name="phone" id="user_phone" class="form-control" value="<?php if(isset($user['phone'])){ echo $user['phone']; } ?>">
						</div>
						<div class="form-group">
							<label>Location</label>
							<select name="location" id="user_location" class="form-control">
								<option value="">Select Location</option>
								<option value="Corporation" <?php if(isset($user['location'])){ if($user['location']=='Corporation'){ echo 'selected'; } } ?> >Aimtron Corporation</option>
								<option value="India" <?php if(isset($user['location'])){ if($user['location']=='India'){ echo 'selected'; } } ?> >Aimtron India</option>
							</select>
						</div>
						<div class="form-group">
							<label>Supervisors</label>
							<select name="supervisors[]" id="supervisors" class="form-control" multiple>
								<?php foreach ($users as $u) { ?>
									<option value="<?php echo $u->id; ?>" <?php if(isset($user['supervisors'])){ $sup_array=explode(',', $user['supervisors']); if(in_array($u->id, $sup_array)){ echo 'selected'; } } ?>><?php echo $u->name; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Department</label>
							<select name="department" id="user_department" class="form-control">
								<option value="">Select Department</option>
								<?php foreach ($departments as $department) { ?>
									<option value="<?php echo $department->id; ?>" <?php if(isset($user["department_id"])){ if($user["department_id"]==$department->id){ echo 'selected'; } } ?>><?php echo $department->name; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<?php if(isset($user["profile_image"])){ if($user['profile_image']!=''){ ?>
								<img width="50" height="50" src="<?php echo base_url('documents/users/profile_images/').$user['profile_image']; ?>" class="img-thumbnail float-right">
							<?php } } ?>
							<label>Profile Image</label>
							<input type="hidden" name="profile_image_hidden" id="profile_image_hidden" value="<?php if(isset($user["profile_image"])){ echo $user['profile_image']; } ?>">
							<div>
								<input type="file" name="profile_image" id="user_profile_image">
							</div>
						</div>
						<div class="form-group">
							<?php if(isset($user["card_profile_image"])){ if($user['card_profile_image']!=''){ ?>
								<img width="50" height="50" src="<?php echo base_url('documents/users/profile_images/').$user['card_profile_image']; ?>" class="img-thumbnail float-right">
							<?php } } ?>
							<label>Card Profile Image</label>
							<input type="hidden" name="card_profile_image_hidden" id="card_profile_image_hidden" value="<?php if(isset($user["profile_image"])){ echo $user['card_profile_image']; } ?>">
							<div>
								<input type="file" name="card_profile_image" id="user__card_profile_image">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Role</label>
							<select class="form-control" name="role" id="user_role" onChange="get_permission(this.value);">
								<option value="">Select Role</option>
								<?php foreach ($roles as $role) { ?>
									<option value="<?php echo $role->id; ?>" <?php if(isset($user["role_id"])){ if($role->id==$user["role_id"]){ echo 'selected'; } } ?>><?php echo $role->name; ?></option>
								<?php } ?>
							</select>
						</div>
						<table class="table table-bordered">
							<thead class="table-active">
								<th>Permissions</th>
								<th class="text-center">
									View
									<label class="kt-checkbox permission-checkbox">
										<input type="checkbox" name="can_view" id="can_view" value="1" onchange="check_checkbox('can_view');">
										<span></span>
									</label>
								</th>
								<th class="text-center">
									Create
									<label class="kt-checkbox permission-checkbox">
										<input type="checkbox" name="can_create" id="can_create" value="1" onchange="check_checkbox('can_create');">
										<span></span>
									</label>
								</th>
								<th class="text-center">
									Edit
									<label class="kt-checkbox permission-checkbox">
										<input type="checkbox" name="can_edit" id="can_edit" value="1" onchange="check_checkbox('can_edit');">
										<span></span>
									</label>
								</th>
								<th class="text-center">
									Delete
									<label class="kt-checkbox permission-checkbox">
										<input type="checkbox" name="can_delete" id="can_delete" value="1" onchange="check_checkbox('can_delete');">
										<span></span>
									</label>
								</th>
								<th class="text-center">
									Export
									<label class="kt-checkbox permission-checkbox">
										<input type="checkbox" name="can_export" id="can_export" value="1" onchange="check_checkbox('can_export');">
										<span></span>
									</label>
								</th>
							</thead>
							<tbody>
								<?php
									if(isset($user['permissions'])){ 
										$user_permissions = array();
		                				$temp_array = explode(";", $user['permissions']); 
						                foreach($temp_array as $t) {
										    $temp = explode("_", $t);
										    $user_permissions[$temp[0]] = $temp[1];
										}
		            				} else{ 
		                				$user_permissions = array(); 
		              				}
								 foreach ($permissions as $key => $permission) {
								 	if(isset($user_permissions[$permission->id])){ 
						              	$temp = str_split ($user_permissions[$permission->id]);
							        } else{ 
						              	$temp = array(); 
							        } 
				        ?>
								<tr>
									<td><?php echo $permission->name; ?></td>
									<td class="text-center">
										<label class="kt-checkbox" data-toggle="kt-tooltip" data-html="true"
											<?php 
												if (!empty(json_decode($permission->item,true))) {
													?>title="<?php
														foreach (json_decode($permission->item,true) as $json_data) {
															echo "<ul class='list-unstyled'>";
																if($json_data['access'] == 'View'){
																	echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
																}
															echo "</ul>";
														}
													?>"<?php
												}
											?>
										>
											<input type="checkbox" name="can_view[<?php echo $permission->id; ?>]" <?php if ($permission->item_view != 1 ) { echo "disabled";	} ?> value="1" <?php if(isset($temp[0])){ if($temp[0]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_view">
											<span></span>
										</label>
									</td>
									<td class="text-center">
										<label class="kt-checkbox" data-toggle="kt-tooltip" data-html="true"
											<?php 
												if (!empty(json_decode($permission->item,true))) {
													?>title="<?php
														foreach (json_decode($permission->item,true) as $json_data) {
															echo "<ul class='list-unstyled'>";
																if($json_data['access'] == 'Create'){
																	echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
																}
															echo "</ul>";
														}
													?>"<?php
												}
											?>
										>
											<input type="checkbox" name="can_create[<?php echo $permission->id; ?>]" <?php if ($permission->item_create != 1 ) { echo "disabled";	} ?> value="1" <?php if(isset($temp[1])){ if($temp[1]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_create">
											<span></span>
										</label>
									</td>
									<td class="text-center">
										<label class="kt-checkbox" data-toggle="kt-tooltip" data-html="true"
											<?php 
												if (!empty(json_decode($permission->item,true))) {
													?>title="<?php
														foreach (json_decode($permission->item,true) as $json_data) {
															echo "<ul class='list-unstyled'>";
																if($json_data['access'] == 'Edit'){
																	echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
																}
															echo "</ul>";
														}
													?>"<?php
												}
											?>
										>
											<input type="checkbox" name="can_edit[<?php echo $permission->id; ?>]" <?php if ($permission->item_edit != 1 ) { echo "disabled";	} ?> value="1" <?php if(isset($temp[2])){ if($temp[2]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_edit">
											<span></span>
										</label>
									</td>
									<td class="text-center">
										<label class="kt-checkbox" data-toggle="kt-tooltip" data-html="true"
											<?php 
												if (!empty(json_decode($permission->item,true))) {
													?>title="<?php
														foreach (json_decode($permission->item,true) as $json_data) {
															echo "<ul class='list-unstyled'>";
																if($json_data['access'] == 'Delete'){
																	echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
																}
															echo "</ul>";
														}
													?>"<?php
												}
											?>
										>
											<input type="checkbox" name="can_delete[<?php echo $permission->id; ?>]" <?php if ($permission->item_delete != 1 ) { echo "disabled";	} ?> value="1" <?php if(isset($temp[3])){ if($temp[3]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_delete">
											<span></span>
										</label>
									</td>
									<td class="text-center">
										<label class="kt-checkbox">
											<input type="checkbox" name="can_export[<?php echo $permission->id; ?>]" value="1" <?php if(isset($temp[4])){ if($temp[4]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_export">
											<span></span>
										</label>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Save User</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#supervisors').select2({
	  placeholder: "Select Supervisors"
	});
 	$("#user_form").validate ({
    rules: {
      name:{
        required : true,
      },
      designation:{
      	required:true,
      },
      username:{
      	required:true,
      	remote:{
          url:"<?php echo base_url('users/check_username'); ?>",
          type:"POST",
          data:{
            username: function(){ return $("#user_username").val(); },
            user_id: function(){ return $("#user_id").val(); },
          }
        },
      },
      email:{
      	required:true,
    	 	email:true,
        remote:{
          url:"<?php echo base_url('users/check_email'); ?>",
          type:"POST",
          data:{
            email: function(){ return $("#user_email").val(); },
            user_id: function(){ return $("#user_id").val(); },
          }
        },
      },
      emp_code:{
      	required:true,
      	remote:{
          url:"<?php echo base_url('users/check_emp_code'); ?>",
          type:"POST",
          data:{
            emp_code: function(){ return $("#user_emp_code").val(); },
            user_id: function(){ return $("#user_id").val(); },
          }
        },
      },
      department:{
      	required:true
      },
      role:{
      	required:true,
      }
    }, 
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      element.closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.error').remove();
    }
	});
	var arrows = {
	    leftArrow: '<i class="la la-angle-left"></i>',
	    rightArrow: '<i class="la la-angle-right"></i>'
	};

	$('#joining_date').datepicker({
	    todayHighlight: true,
	    orientation: "bottom left",
	    autoclose: true,
	    templates: arrows
	});
});
function get_permission(id){
  $.ajax({
    type:'POST',   
    url: "<?php echo base_url('roles/get_remote_role_permissions'); ?>",
    data: {"role_id":id},
    success: function(data){
       var json = jQuery.parseJSON(data);
       for(var i in json.permissions){
          pr=json.permissions[i];
          if(pr.can_view==1){
              $("#"+pr.param+"_can_view").prop('checked',true);
          }else{
              $("#"+pr.param+"_can_view").prop('checked',false);
          }
          if(pr.can_create==1){
              $("#"+pr.param+"_can_create").prop('checked',true);
          }else{
              $("#"+pr.param+"_can_create").prop('checked',false);
          }
          if(pr.can_edit==1){
              $("#"+pr.param+"_can_edit").prop('checked',true);
          }else{
              $("#"+pr.param+"_can_edit").prop('checked',false);
          }
          if(pr.can_delete==1){
              $("#"+pr.param+"_can_delete").prop('checked',true);
          }else{
              $("#"+pr.param+"_can_delete").prop('checked',false);
          }
          if(pr.can_export==1){
              $("#"+pr.param+"_can_export").prop('checked',true);
          }else{
              $("#"+pr.param+"_can_export").prop('checked',false);
          }
       }
    }
  }); 
}
function check_checkbox(action){
  $("form#user_form :input[id*='_"+action+"']").each(function(){
    if($('#'+action+'').prop('checked')==true){
    	$(this).prop('checked',true);
    }else{
      $(this).prop('checked',false);
    }
  });
}
</script>