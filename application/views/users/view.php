<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php echo $user_view_profile['name'] ?>'s Profile</title>
		<meta name="description" content="Updates and statistics">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<link href="<?php echo base_url(); ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/vendors/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/custom.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('assets/vendors/custom/datatables/datatables.bundle.css'); ?>"  rel="stylesheet" type="text/css">
		<?php if(get_site_setting('company_favicon')==''){ ?>
			<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/media/logos/favicon.png" />
		<?php }else{ ?>
			<link rel="shortcut icon" href="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_favicon'); ?>" />
		<?php } ?>
		<script src="<?php echo base_url(); ?>assets/vendors/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js" type="text/javascript"></script>
		<script src="//cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js" type="text/javascript"></script>
	</head>
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<?php $access= $user_views; ?>
		<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="<?php echo base_url(); ?>">
					<?php if(get_site_setting('company_logo')==''){ ?>
						<img alt="Logo" class="logo_width" src="<?php echo base_url(); ?>assets/media/logos/aimtron.png" />
					<?php }else{ ?>
						<img alt="Logo" class="logo_width" src="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_logo');; ?>">
					<?php } ?>
					<?php if(get_site_setting('company_favicon')==''){ ?>
						<img src="<?php echo base_url(); ?>assets/media/logos/favicon.png" width="30%" />
					<?php }else{ ?>
						<img src="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_favicon'); ?>" width="30%" />
					<?php } ?>
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
			</div>
		</div>
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
				<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
					<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
						<div class="kt-aside__brand-logo">
							<a href="<?php echo base_url(); ?>">
								<?php if(get_site_setting('company_logo')==''){ ?>
									<img alt="Logo" class="margin-top-10" src="<?php echo base_url(); ?>assets/media/logos/aimtron.png" width="100%" />
								<?php }else{ ?>
									<img alt="Logo" class="margin-top-10" src="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_logo');; ?>" width="100%">
								<?php } ?>
							</a>
							<?php if(get_site_setting('company_favicon')==''){ ?>
								<img src="<?php echo base_url(); ?>assets/media/logos/favicon.png" width="30%" />
							<?php }else{ ?>
								<img src="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_favicon'); ?>" width="30%" />
							<?php } ?>
						</div>
						<div class="kt-aside__brand-tools">
							<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
								<span>
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
											<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
											<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
										</g>
									</svg>
								</span>
								<span>
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
											<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
											<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
										</g>
									</svg>
								</span>
							</button>
						</div>
					</div>
					<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
						<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
							<ul class="kt-menu__nav ">
								<li class="kt-menu__item" aria-haspopup="true">
									<a href="<?php echo base_url('home'); ?>" class="kt-menu__link " target="_blank">
										<span class="kt-menu__link-icon" >
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon id="Bound" points="0 0 24 0 24 24 0 24" />
													<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" id="Shape" fill="#000000" fill-rule="nonzero" />
													<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" id="Path" fill="#000000" opacity="0.3" />
												</g>
											</svg>
										</span>
										<span class="kt-menu__link-text">Dashboard</span>
									</a>
								</li>
								<?php 
						            $leads=array_search('leads', array_column($access, 'param')); 
						            $my_leads=array_search('my_leads', array_column($access, 'param')); 
						            $shared_leads=array_search('shared_leads', array_column($access, 'param')); 
						            $customers=array_search('customers', array_column($access, 'param')); 
						            $my_customers=array_search('my_customers', array_column($access, 'param')); 
						            $shared_customers=array_search('shared_customers', array_column($access, 'param')); 
						            $prospects=array_search('prospects', array_column($access, 'param')); 
						            if($access[$leads]['can_view']==1 || $access[$customers]['can_view']==1 || $access[$my_leads]['can_view']==1 || $access[$shared_leads]['can_view']==1 || $access[$shared_customers]['can_view']==1 || $access[$my_customers]['can_view']==1 || $access[$prospects]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <circle fill="#000000" opacity="0.3" cx="20.5" cy="12.5" r="1.5"/>
											        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 6.500000) rotate(-15.000000) translate(-12.000000, -6.500000) " x="3" y="3" width="18" height="7" rx="1"/>
											        <path d="M22,9.33681558 C21.5453723,9.12084552 21.0367986,9 20.5,9 C18.5670034,9 17,10.5670034 17,12.5 C17,14.4329966 18.5670034,16 20.5,16 C21.0367986,16 21.5453723,15.8791545 22,15.6631844 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,6 C2,4.8954305 2.8954305,4 4,4 L20,4 C21.1045695,4 22,4.8954305 22,6 L22,9.33681558 Z" fill="#000000"/>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">CRM</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">CRM</span></span></li>
													<?php if($access[$prospects]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('prospect'); ?>" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Prospects</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$leads]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('leads'); ?>" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Leads</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$my_leads]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('leads/my'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">My Leads</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$shared_leads]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('leads/shared'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Shared Leads</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$customers]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('customers'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Customers</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$my_customers]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('customers/my'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">My Customers</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$shared_customers]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('customers/shared'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Shared Customers</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
			            			$projects=array_search('projects', array_column($access, 'param'));
			            			if($access[$projects]['can_view']==1){
			          					?>
										<li class="kt-menu__item" aria-haspopup="true">
											<a href="<?php echo base_url(); ?>projects" class="kt-menu__link " target="_blank">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <path d="M15.9956071,6 L9,6 C7.34314575,6 6,7.34314575 6,9 L6,15.9956071 C4.70185442,15.9316381 4,15.1706419 4,13.8181818 L4,6.18181818 C4,4.76751186 4.76751186,4 6.18181818,4 L13.8181818,4 C15.1706419,4 15.9316381,4.70185442 15.9956071,6 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
											        <path d="M10.1818182,8 L17.8181818,8 C19.2324881,8 20,8.76751186 20,10.1818182 L20,17.8181818 C20,19.2324881 19.2324881,20 17.8181818,20 L10.1818182,20 C8.76751186,20 8,19.2324881 8,17.8181818 L8,10.1818182 C8,8.76751186 8.76751186,8 10.1818182,8 Z" fill="#000000"/>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Projects</span>
											</a>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $eng_pkg_master=array_search('eng_pkg_master', array_column($access, 'param')); 
						            $quote_bag_list=array_search('quote_bag_list', array_column($access, 'param'));
						            $quote_pipeline_record=array_search('quote_pipeline_record', array_column($access, 'param'));
						            $quotation=array_search('quotation', array_column($access, 'param'));
						            $we_won_quotes=array_search('we_won_quotes', array_column($access, 'param'));
						            $quote_tasks=array_search('quote_tasks', array_column($access, 'param'));
			            			$winning_ratio_graph=array_search('winning_ratio_graph', array_column($access, 'param'));
			            			if($access[$eng_pkg_master]['can_view']==1 || $access[$quote_bag_list]['can_view']==1 || $access[$quote_pipeline_record]['can_view']==1 || $access[$quotation]['can_view']==1 || $access[$we_won_quotes]['can_view']==1 || $access[$quote_tasks]['can_view']==1 || $access[$winning_ratio_graph]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"></rect>
											        <path d="M11.7573593,15.2426407 L8.75735931,15.2426407 C8.20507456,15.2426407 7.75735931,15.6903559 7.75735931,16.2426407 C7.75735931,16.7949254 8.20507456,17.2426407 8.75735931,17.2426407 L11.7573593,17.2426407 L11.7573593,18.2426407 C11.7573593,19.3472102 10.8619288,20.2426407 9.75735931,20.2426407 L5.75735931,20.2426407 C4.65278981,20.2426407 3.75735931,19.3472102 3.75735931,18.2426407 L3.75735931,14.2426407 C3.75735931,13.1380712 4.65278981,12.2426407 5.75735931,12.2426407 L9.75735931,12.2426407 C10.8619288,12.2426407 11.7573593,13.1380712 11.7573593,14.2426407 L11.7573593,15.2426407 Z" fill="#000000" opacity="0.3" transform="translate(7.757359, 16.242641) rotate(-45.000000) translate(-7.757359, -16.242641) "></path>
											        <path d="M12.2426407,8.75735931 L15.2426407,8.75735931 C15.7949254,8.75735931 16.2426407,8.30964406 16.2426407,7.75735931 C16.2426407,7.20507456 15.7949254,6.75735931 15.2426407,6.75735931 L12.2426407,6.75735931 L12.2426407,5.75735931 C12.2426407,4.65278981 13.1380712,3.75735931 14.2426407,3.75735931 L18.2426407,3.75735931 C19.3472102,3.75735931 20.2426407,4.65278981 20.2426407,5.75735931 L20.2426407,9.75735931 C20.2426407,10.8619288 19.3472102,11.7573593 18.2426407,11.7573593 L14.2426407,11.7573593 C13.1380712,11.7573593 12.2426407,10.8619288 12.2426407,9.75735931 L12.2426407,8.75735931 Z" fill="#000000" transform="translate(16.242641, 7.757359) rotate(-45.000000) translate(-16.242641, -7.757359) "></path>
											        <path d="M5.89339828,3.42893219 C6.44568303,3.42893219 6.89339828,3.87664744 6.89339828,4.42893219 L6.89339828,6.42893219 C6.89339828,6.98121694 6.44568303,7.42893219 5.89339828,7.42893219 C5.34111353,7.42893219 4.89339828,6.98121694 4.89339828,6.42893219 L4.89339828,4.42893219 C4.89339828,3.87664744 5.34111353,3.42893219 5.89339828,3.42893219 Z M11.4289322,5.13603897 C11.8194565,5.52656326 11.8194565,6.15972824 11.4289322,6.55025253 L10.0147186,7.96446609 C9.62419433,8.35499039 8.99102936,8.35499039 8.60050506,7.96446609 C8.20998077,7.5739418 8.20998077,6.94077682 8.60050506,6.55025253 L10.0147186,5.13603897 C10.4052429,4.74551468 11.0384079,4.74551468 11.4289322,5.13603897 Z M0.600505063,5.13603897 C0.991029355,4.74551468 1.62419433,4.74551468 2.01471863,5.13603897 L3.42893219,6.55025253 C3.81945648,6.94077682 3.81945648,7.5739418 3.42893219,7.96446609 C3.0384079,8.35499039 2.40524292,8.35499039 2.01471863,7.96446609 L0.600505063,6.55025253 C0.209980772,6.15972824 0.209980772,5.52656326 0.600505063,5.13603897 Z" fill="#000000" opacity="0.3" transform="translate(6.014719, 5.843146) rotate(-45.000000) translate(-6.014719, -5.843146) "></path>
											        <path d="M17.9142136,15.4497475 C18.4664983,15.4497475 18.9142136,15.8974627 18.9142136,16.4497475 L18.9142136,18.4497475 C18.9142136,19.0020322 18.4664983,19.4497475 17.9142136,19.4497475 C17.3619288,19.4497475 16.9142136,19.0020322 16.9142136,18.4497475 L16.9142136,16.4497475 C16.9142136,15.8974627 17.3619288,15.4497475 17.9142136,15.4497475 Z M23.4497475,17.1568542 C23.8402718,17.5473785 23.8402718,18.1805435 23.4497475,18.5710678 L22.0355339,19.9852814 C21.6450096,20.3758057 21.0118446,20.3758057 20.6213203,19.9852814 C20.2307961,19.5947571 20.2307961,18.9615921 20.6213203,18.5710678 L22.0355339,17.1568542 C22.4260582,16.76633 23.0592232,16.76633 23.4497475,17.1568542 Z M12.6213203,17.1568542 C13.0118446,16.76633 13.6450096,16.76633 14.0355339,17.1568542 L15.4497475,18.5710678 C15.8402718,18.9615921 15.8402718,19.5947571 15.4497475,19.9852814 C15.0592232,20.3758057 14.4260582,20.3758057 14.0355339,19.9852814 L12.6213203,18.5710678 C12.2307961,18.1805435 12.2307961,17.5473785 12.6213203,17.1568542 Z" fill="#000000" opacity="0.3" transform="translate(18.035534, 17.863961) scale(1, -1) rotate(45.000000) translate(-18.035534, -17.863961) "></path>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">RFQ</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">RFQ</span></span></li>
													<?php if($access[$eng_pkg_master]['can_view']==1 || $access[$quote_bag_list]['can_view']==1 || $access[$quote_pipeline_record]['can_view']==1 || $access[$quotation]['can_view']==1 || $access[$we_won_quotes]['can_view']==1 || $access[$winning_ratio_graph]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Quotation</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$quotation]['can_view']==1){ ?>
																<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
																	<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Quote</span>
																		<i class="kt-menu__ver-arrow la la-angle-right"></i>
																	</a>
																	<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
																		<ul class="kt-menu__subnav">
																			<?php if($access[$quotation]['can_view']==1){ ?>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/new_quote'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">New Quotes</span>
																				</a>
																			</li>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/working_quotes'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">Working Quotes</span>
																				</a>
																			</li>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/review_quote'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">Review Quotes</span>
																				</a>
																			</li>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/submited_quotes'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">Submited Quotes</span>
																				</a>
																			</li>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/won_loss_close_quotes'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">Won/Loss/Close Quotes</span>
																				</a>
																			</li>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">All Quotes</span>
																				</a>
																			</li>
																			<?php } ?>
																		</ul>
																	</div>
																</li>
																<?php } ?>
																<?php if($access[$eng_pkg_master]['can_view']==1 || $access[$quote_bag_list]['can_view']==1){ ?>
																<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
																	<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Setup</span>
																		<i class="kt-menu__ver-arrow la la-angle-right"></i>
																	</a>
																	<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
																		<ul class="kt-menu__subnav">
																			<?php if($access[$eng_pkg_master]['can_view']==1){ ?>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/eng_pkg_master'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">ENG PKG Master</span>
																				</a>
																			</li>
																			<?php } ?>
																			<?php if($access[$quote_bag_list]['can_view']==1){ ?>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/quote_bag_list'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">Quote BAG List</span>
																				</a>
																			</li>
																			<?php } ?>
																		</ul>
																	</div>
																</li>
																<?php } ?>
																<?php if($access[$quote_pipeline_record]['can_view']==1 || $access[$quotation]['can_view']==1 || $access[$winning_ratio_graph]['can_view']==1){ ?>
																<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
																	<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Reports</span>
																		<i class="kt-menu__ver-arrow la la-angle-right"></i>
																	</a>
																	<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
																		<ul class="kt-menu__subnav">
																			<?php if($access[$quote_pipeline_record]['can_view']==1){ ?>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/pipeline_record'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">Pipeline Record</span>
																				</a>
																			</li>
																			<?php } ?>
																			
																			<?php if($access[$quotation]['can_view']==1){ ?>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/quote_report'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">Quote Report</span>
																				</a>
																			</li>
																			<?php } ?>
																			<?php if($access[$winning_ratio_graph]['can_view']==1){ ?>
																			<li class="kt-menu__item " aria-haspopup="true">
																				<a href="<?php echo base_url('quotes/winning_ratio_graph'); ?>" class="kt-menu__link " target="_blank">
																					<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																					<span class="kt-menu__link-text">Winning Ratio Graph</span>
																				</a>
																			</li>
																			<?php } ?>
																		</ul>
																	</div>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$quote_tasks]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Quote Task</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('quotes/new_quote_task'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">New Quote Task</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('quotes/all_quotes_task'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">All Quote Task</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('quotes/my_quote_task'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">My Quote Task</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $part_lookup=array_search('part_lookup', array_column($access, 'param')); 
						            $where_used=array_search('where_used', array_column($access, 'param'));
						            $incoming_inspection=array_search('incoming_inspection', array_column($access, 'param'));
						            $engineering_backlog=array_search('engineering_backlog', array_column($access, 'param'));
						            $inward=array_search('inward', array_column($access, 'param'));
						            $job_pick_list=array_search('job_pick_list', array_column($access, 'param'));
						            $open_purchase_orders=array_search('open_purchase_orders', array_column($access, 'param'));
						            $purchase_cfm=array_search('purchase_cfm', array_column($access, 'param'));
						            if($access[$part_lookup]['can_view']==1 || $access[$where_used]['can_view']==1 || $access[$incoming_inspection]['can_view']==1 || $access[$engineering_backlog]['can_view']==1 || $access[$inward]['can_view']==1 || $access[$job_pick_list]['can_view']==1 || $access[$open_purchase_orders]['can_view']==1 || $access[$purchase_cfm]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"></rect>
											        <path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000"></path>
											        <rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1"></rect>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Materials</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Materials</span></span></li>
													<?php if($access[$part_lookup]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('materials/part_lookup'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Part Lookup</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('materials/part_multi_search'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Part Multi Search</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('documents/general/NEW-STOCKROOM-MAP.pdf'); ?>" target="_blank" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Stockroom Chart</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$where_used]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('materials/where_used'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Where Used</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$incoming_inspection]['can_view']==1 || $access[$engineering_backlog]['can_view']==1 || $access[$inward]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Incoming Inspection</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$incoming_inspection]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('materials/incoming_inspections'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">View All</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('materials/manage_standards'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Manage Standards</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$engineering_backlog]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('materials/engineering_backlogs'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Engineering Backlogs</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$inward]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('materials/inward_entry'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Inward Entry</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('materials/inward_list'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Inward List</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('materials/iqc_list'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">IQC Material List</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$job_pick_list]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('materials/job_pick_list'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Job Pick List</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$open_purchase_orders]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('materials/open_orders'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Open Orders</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$purchase_cfm]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/cfm'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">CFM</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $approved_vendor=array_search('approved_vendor', array_column($access, 'param')); 
						            $mrp_time_phased=array_search('mrp_time_phased', array_column($access, 'param')); 
						            $mrp_job_phased=array_search('mrp_job_phased', array_column($access, 'param')); 
						            $supplier_dashboard=array_search('supplier_dashboard', array_column($access, 'param')); 
						            $purchase_by_supplier=array_search('purchase_by_supplier', array_column($access, 'param')); 
						            $purchase_history=array_search('purchase_history', array_column($access, 'param')); 
						            $purchase_jobs_with_status=array_search('purchase_jobs_with_status', array_column($access, 'param')); 
						            $compact_mrp=array_search('compact_mrp', array_column($access, 'param')); 
						            $purchase_part_requisition=array_search('purchase_part_requisition', array_column($access, 'param')); 
						            if($access[$approved_vendor]['can_view']==1 || $access[$mrp_time_phased]['can_view']==1 || $access[$mrp_job_phased]['can_view']==1 || $access[$supplier_dashboard]['can_view']==1 || $access[$purchase_by_supplier]['can_view']==1 || $access[$purchase_history]['can_view']==1 || $access[$purchase_jobs_with_status]['can_view']==1 || $access[$compact_mrp]['can_view']==1 || $access[$purchase_part_requisition]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"/>
											        <path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"/>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Purchasing</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Purchasing</span></span></li>
													<?php if($access[$approved_vendor]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/approved_vendors'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Approved Vendors</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$mrp_time_phased]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/mrp_time_phased'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">MRP (Time Phased)</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$mrp_job_phased]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/mrp_job_specific'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">MRP (Job Specific)</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$supplier_dashboard]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/supplier_dashboard'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Supplier Dashboard</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$supplier_dashboard]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/supplier_scorecard'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Supplier Scorecard</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$purchase_by_supplier]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/purchase_by_supplier'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Purchase By Supplier</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$purchase_history]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/purchase_history'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Purchase History</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$purchase_jobs_with_status]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Jobs With Status</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('purchase/completed_jobs'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Completed Jobs</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('purchase/working_jobs'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Working Jobs</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('purchase/all_jobs'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">All Jobs</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$compact_mrp]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/compact_mrp') ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Compact MRP</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/received_vs_dmr') ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Received vs DMR</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$purchase_part_requisition]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('parts/purchase_part_requisition') ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Purchase Part Requisition</span>
														</a>
													</li>
													<?php } ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/purchase_on_time_delivery') ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Purchase On Time Delivery By Month</span>
														</a>
													</li>
													<?php if($access[$compact_mrp]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('purchase/adjustment_qty') ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Adjustment Qty</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $receiving_certificates=array_search('receiving_certificates', array_column($access, 'param'));
						            $receiving_forecast=array_search('receiving_forecast', array_column($access, 'param'));
						            $kitting_dashboard=array_search('kitting_dashboard', array_column($access, 'param'));
						            $store_part_requisition=array_search('store_part_requisition', array_column($access, 'param'));
						            $part_requisition_list=array_search('part_requisition_list', array_column($access, 'param'));
						            if($access[$receiving_certificates]['can_view']==1 || $access[$receiving_forecast]['can_view']==1 || $access[$kitting_dashboard]['can_view']==1 || $access[$store_part_requisition]['can_view']==1 || $access[$part_requisition_list]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
											        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
											        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Receiving</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Receiving</span></span></li>
													<?php if($access[$receiving_certificates]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('receiving/certificates'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Certificates</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$receiving_forecast]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('receiving/receiving_forcast'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Receiving Forecast</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$kitting_dashboard]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('receiving/kitting_dashboard'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Kitting Dashboard</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$store_part_requisition]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('parts/store_part_requisition'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Store Part Requisition</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$part_requisition_list]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('parts/part_requisition_list'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Part Requisition List</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $assembly=array_search('assembly', array_column($access, 'param')); 
						            $assembly_customers=array_search('assembly_customers', array_column($access, 'param'));
						            $eco=array_search('eco', array_column($access, 'param'));
						            $ecr=array_search('ecr', array_column($access, 'param'));
						            $ecr_reasons=array_search('ecr_reasons', array_column($access, 'param'));
						            $ecr_impact_areas=array_search('ecr_impact_areas', array_column($access, 'param'));
						            $deviation=array_search('deviation', array_column($access, 'param'));
						            $eng_checklists=array_search('eng_checklists', array_column($access, 'param'));
						            $assembly_quote_list=array_search('assembly_quote_list', array_column($access, 'param'));
						            $quote_dfm=array_search('quote_dfm', array_column($access, 'param'));
						            if($access[$assembly]['can_view']==1 || $access[$assembly_customers]['can_view']==1 || $access[$eco]['can_view']==1 || $access[$ecr]['can_view']==1 || $access[$ecr_reasons]['can_view']==1 || $access[$ecr_impact_areas]['can_view']==1 || $access[$deviation]['can_view']==1 || $access[$quote_dfm]['can_view']==1 || $access[$eng_checklists]['can_view']==1 || $access[$assembly_quote_list]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"></rect>
											        <rect fill="#000000" opacity="0.3" x="2" y="3" width="20" height="18" rx="2"></rect>
											        <path d="M9.9486833,13.3162278 C9.81256925,13.7245699 9.43043041,14 9,14 L5,14 C4.44771525,14 4,13.5522847 4,13 C4,12.4477153 4.44771525,12 5,12 L8.27924078,12 L10.0513167,6.68377223 C10.367686,5.73466443 11.7274983,5.78688777 11.9701425,6.75746437 L13.8145063,14.1349195 L14.6055728,12.5527864 C14.7749648,12.2140024 15.1212279,12 15.5,12 L19,12 C19.5522847,12 20,12.4477153 20,13 C20,13.5522847 19.5522847,14 19,14 L16.118034,14 L14.3944272,17.4472136 C13.9792313,18.2776054 12.7550291,18.143222 12.5298575,17.2425356 L10.8627389,10.5740611 L9.9486833,13.3162278 Z" fill="#000000" fill-rule="nonzero"></path>
											        <circle fill="#000000" opacity="0.3" cx="19" cy="6" r="1"></circle>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Engineering</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Engineering</span></span></li>
													<?php if($access[$assembly]['can_view']==1 || $access[$assembly_customers]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Assembly</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$assembly]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('assemblies/addAssembly'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Add Assembly</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('assemblies'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">All Assembly</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('assemblies/selectassembly'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Assembly View</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('assemblies/upload_files'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Upload Files</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('assemblies/manufactured_part_list'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Manufactured Part List</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$assembly_customers]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('assemblies/all_customer'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">All Customer</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$eco]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('eco'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">ECO</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$ecr]['can_view']==1 || $access[$ecr]['can_create']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">ECR</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$ecr]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('ecr'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">All ECR</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$ecr]['can_create']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('ecr/new_ecr'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">New ECR</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$ecr]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('ecr/pending'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Pending ECR</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$ecr]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('ecr/reviewed'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Reviewed ECR</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$ecr_reasons]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('ecr/reasons'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">ECR Reasons</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$ecr_impact_areas]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('ecr/impact_areas'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">ECR Impect Areas</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$deviation]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('deviation'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Deviation</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$eng_checklists]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Eng Check List</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('eng_checklists/entry'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Checklist Entry</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('eng_checklists'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Engineering Checklist</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('eng_checklists/manufacturing_checklist'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Manufacturing Checklist</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('eng_checklists/my'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">My Checklist</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$assembly_quote_list]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('quotes/assembly_quotes'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Assembly Quote List</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$quote_dfm]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">DFM</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('dfm/new_dfm'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">New DFM</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('dfm'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">All DFM</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $job_list=array_search('job_list', array_column($access, 'param')); 
						            $closed_job_list=array_search('closed_job_list', array_column($access, 'param')); 
						            $dev_job_list=array_search('dev_job_list', array_column($access, 'param')); 
						            $kanban_orders=array_search('kanban_orders', array_column($access, 'param'));
						            $job_ticket=array_search('job_ticket', array_column($access, 'param'));
						            $move_ticket=array_search('move_ticket', array_column($access, 'param'));
						            $smt_lines=array_search('smt_lines', array_column($access, 'param'));
						            $active_smt_master=array_search('active_smt_master', array_column($access, 'param'));
						            $fg_part_label=array_search('fg_part_label', array_column($access, 'param'));
						            $wip_by_department=array_search('wip_by_department', array_column($access, 'param'));
						            $active_production_tasks=array_search('active_production_tasks', array_column($access, 'param'));
						            $second_assembly_schedule=array_search('second_assembly_schedule', array_column($access, 'param'));
						            $machine_downtime=array_search('machine_downtime', array_column($access, 'param'));
						            $machine_downtime_chart=array_search('machine_downtime_chart', array_column($access, 'param'));
						            $process_stop=array_search('process_stop', array_column($access, 'param'));
						            $part_requisition=array_search('part_requisition', array_column($access, 'param'));
						            $production_part_requisition=array_search('production_part_requisition', array_column($access, 'param'));
						            $finished_good_inventory=array_search('finished_good_inventory', array_column($access, 'param'));
						            $priority_job_list=array_search('priority_job_list', array_column($access, 'param'));
						            $kit_status=array_search('kit_status', array_column($access, 'param'));
						            $production_efficiency=array_search('production_efficiency', array_column($access, 'param'));
						            $schedule_export=array_search('schedule_export', array_column($access, 'param'));
						            $production_schedule=array_search('production_schedule', array_column($access, 'param'));
			            			if($access[$job_list]['can_view']==1  || $access[$kanban_orders]['can_view']==1 || $access[$job_ticket]['can_view']==1 || $access[$move_ticket]['can_view']==1 || $access[$smt_lines]['can_view']==1 || $access[$active_smt_master]['can_view']==1 || $access[$fg_part_label]['can_view']==1 || $access[$wip_by_department]['can_view']==1 || $access[$active_production_tasks]['can_view']==1 || $access[$second_assembly_schedule]['can_view']==1 || $access[$machine_downtime]['can_view']==1 || $access[$machine_downtime_chart]['can_view']==1 || $access[$process_stop]['can_view']==1 || $access[$part_requisition]['can_view']==1 || $access[$production_part_requisition]['can_view']==1 || $access[$finished_good_inventory]['can_view']==1 || $access[$priority_job_list]['can_view']==1 || $access[$closed_job_list]['can_view']==1 || $access[$production_efficiency]['can_view']==1 || $access[$schedule_export]['can_view']==1 || $access[$production_schedule]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"></rect>
											        <path d="M5,5 L5,15 C5,15.5948613 5.25970314,16.1290656 5.6719139,16.4954176 C5.71978107,16.5379595 5.76682388,16.5788906 5.81365532,16.6178662 C5.82524933,16.6294602 15,7.45470952 15,7.45470952 C15,6.9962515 15,6.17801499 15,5 L5,5 Z M5,3 L15,3 C16.1045695,3 17,3.8954305 17,5 L17,15 C17,17.209139 15.209139,19 13,19 L7,19 C4.790861,19 3,17.209139 3,15 L3,5 C3,3.8954305 3.8954305,3 5,3 Z" fill="#000000" fill-rule="nonzero" transform="translate(10.000000, 11.000000) rotate(-315.000000) translate(-10.000000, -11.000000) "></path>
											        <path d="M20,22 C21.6568542,22 23,20.6568542 23,19 C23,17.8954305 22,16.2287638 20,14 C18,16.2287638 17,17.8954305 17,19 C17,20.6568542 18.3431458,22 20,22 Z" fill="#000000" opacity="0.3"></path>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Production</span>
												<i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
														<span class="kt-menu__link">
															<span class="kt-menu__link-text">Production</span>
														</span>
													</li>
													<?php if($access[$job_list]['can_view']==1 || $access[$closed_job_list]['can_view']==1 || $access[$kanban_orders]['can_view']==1 || $access[$wip_by_department]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Job Lists</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$job_list]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/job_list'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Master Job List</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$kanban_orders]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/kanban_orders'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Kanban Orders</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$wip_by_department]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/wip_by_department'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">WIP By Department</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$closed_job_list]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/closed_job_list'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Closed Job List</span>
																		</a>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$smt_lines]['can_view']==1 || $access[$active_smt_master]['can_view']==1 || $access[$second_assembly_schedule]['can_view']==1 || $access[$dev_job_list]['can_view']==1 || $access[$priority_job_list]['can_view']==1 || $access[$kit_status]['can_view']==1 || $access[$schedule_export]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Job Schedule</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$smt_lines]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/smt_lines'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">SMT Lines</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$active_smt_master]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/active_smt_master'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Active SMT Master</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$dev_job_list]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/dev_job_list'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Scheduling Job List</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$priority_job_list]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/priority_job_list'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Priority Job List</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$kit_status]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/kit_status'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Kit Status</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$production_efficiency]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/efficiency'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Efficiency</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$schedule_export]['can_view']==1 || $access[$production_schedule]['can_view']==1 ){ ?>
																	<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
																		<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Production Schedule</span>
																			<i class="kt-menu__ver-arrow la la-angle-right"></i>
																		</a>
																		<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
																			<ul class="kt-menu__subnav">
																				<?php if($access[$schedule_export]['can_view']==1){ ?>
																				<li class="kt-menu__item " aria-haspopup="true">
																					<a href="<?php echo base_url('production/schedule_export'); ?>" class="kt-menu__link " target="_blank">
																						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																							<span></span>
																						</i>
																						<span class="kt-menu__link-text">
																							Schedule Export
																						</span>
																					</a>
																				</li>
																				<li class="kt-menu__item " aria-haspopup="true">
																					<a href="<?php echo base_url('production/schedule_import'); ?>" class="kt-menu__link " target="_blank">
																						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																							<span></span>
																						</i>
																						<span class="kt-menu__link-text">
																							Schedule Import
																						</span>
																					</a>
																				</li>
																				<?php } ?>
																				<?php if($access[$production_schedule]['can_view']==1){ ?>
																				<li class="kt-menu__item " aria-haspopup="true">
																					<a href="<?php echo base_url('production/smt_schedule'); ?>" class="kt-menu__link " target="_blank">
																						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																						<span class="kt-menu__link-text">SMT Schedule</span>
																					</a>
																				</li>
																				<li class="kt-menu__item " aria-haspopup="true">
																					<a href="<?php echo base_url('production/backend_schedule'); ?>" class="kt-menu__link " target="_blank">
																						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																						<span class="kt-menu__link-text">Backend Schedule</span>
																					</a>
																				</li>
																				<li class="kt-menu__item " aria-haspopup="true">
																					<a href="<?php echo base_url('production/test_schedule'); ?>" class="kt-menu__link " target="_blank">
																						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																						<span class="kt-menu__link-text">Test Schedule</span>
																					</a>
																				</li>
																				<li class="kt-menu__item " aria-haspopup="true">
																					<a href="<?php echo base_url('production/inspection_schedule'); ?>" class="kt-menu__link " target="_blank">
																						<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																						<span class="kt-menu__link-text">Inspection Schedule</span>
																					</a>
																				</li>
																				<?php } ?>
																			</ul>
																		</div>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$machine_downtime]['can_view']==1 || $access[$machine_downtime_chart]['can_view']==1 || $access[$process_stop]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Process Related</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$machine_downtime]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/machine_downtime'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Machine Downtime</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$machine_downtime_chart]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/machine_downtime_chart'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Machine Downtime Chart</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$process_stop]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/process_stop'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Process Stop</span>
																		</a>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$job_ticket]['can_view']==1 || $access[$move_ticket]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Job Label</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$job_ticket]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/job_ticket'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Job Ticket</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$job_ticket]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/part_ticket'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Part Ticket</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$move_ticket]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/move_ticket'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Move Ticket</span>
																		</a>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$part_requisition]['can_view']==1 || $access[$production_part_requisition]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Part Request</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$part_requisition]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('parts/part_requisition'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Part Requisition</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$production_part_requisition]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('parts/production_part_requisition'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Part Requisition List</span>
																		</a>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$finished_good_inventory]['can_view']==1 || $access[$fg_part_label]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Finish Goods</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$fg_part_label]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/fg_part_label'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">FG Part Label</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$finished_good_inventory]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('production/finished_goods_inventory'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Finished Goods Inventory</span>
																		</a>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$active_production_tasks]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('production/active_production_task'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Active Production Task</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
			            			$shipment_details=array_search('shipment_details', array_column($access, 'param')); 
			            			if($access[$shipment_details]['can_view']==1){ 
			         					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												        <rect x="0" y="0" width="24" height="24"></rect>
											        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
											        <rect fill="#000000" opacity="0.3" x="11" y="2" width="2" height="14" rx="1"></rect>
											        <path d="M12.0362375,3.37797611 L7.70710678,7.70710678 C7.31658249,8.09763107 6.68341751,8.09763107 6.29289322,7.70710678 C5.90236893,7.31658249 5.90236893,6.68341751 6.29289322,6.29289322 L11.2928932,1.29289322 C11.6689749,0.916811528 12.2736364,0.900910387 12.6689647,1.25670585 L17.6689647,5.75670585 C18.0794748,6.12616487 18.1127532,6.75845471 17.7432941,7.16896473 C17.3738351,7.57947475 16.7415453,7.61275317 16.3310353,7.24329415 L12.0362375,3.37797611 Z" fill="#000000" fill-rule="nonzero"></path>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Shipping</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Shipping</span></span></li>
													<?php if($access[$shipment_details]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('shiping/shipment_details'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Shippment Details</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $job_list_rma=array_search('job_list_rma', array_column($access, 'param'));
						            $defects=array_search('defects', array_column($access, 'param'));
						            $defect_type=array_search('defect_type', array_column($access, 'param'));
						            $test_yield=array_search('test_yield', array_column($access, 'param'));
						            $smt_yield=array_search('smt_yield', array_column($access, 'param'));
						            $final_qa_yield=array_search('final_qa_yield', array_column($access, 'param'));
						            $production_test=array_search('production_test', array_column($access, 'param'));
						            $customer_returns_test=array_search('customer_returns_test', array_column($access, 'param'));
						            $customer_returns_qa=array_search('customer_returns_qa', array_column($access, 'param'));
						            $quality_categories=array_search('quality_categories', array_column($access, 'param'));
						            $quality_alerts=array_search('quality_alerts', array_column($access, 'param'));
						            $cars=array_search('cars', array_column($access, 'param'));
						            $aim_fairs=array_search('aim_fairs', array_column($access, 'param'));
						            $rma_and_ship_chart=array_search('rma_and_ship_chart', array_column($access, 'param'));
						            $customer_returns=array_search('customer_returns', array_column($access, 'param'));
						            $rma_dashboard=array_search('rma_dashboard', array_column($access, 'param'));
						            $scrap_report=array_search('scrap_report', array_column($access, 'param'));
						            if($access[$job_list_rma]['can_view']==1 || $access[$defects]['can_view']==1 || $access[$defect_type]['can_view']==1 || $access[$test_yield]['can_view']==1 || $access[$smt_yield]['can_view']==1 || $access[$final_qa_yield]['can_view']==1 || $access[$production_test]['can_create']==1 || $access[$production_test]['can_view']==1 || $access[$customer_returns_test]['can_create']==1 || $access[$customer_returns_test]['can_view']==1 || $access[$customer_returns_qa]['can_create']==1 || $access[$customer_returns_qa]['can_view']==1 || $access[$quality_categories]['can_view']==1 || $access[$quality_alerts]['can_create']==1 || $access[$quality_alerts]['can_view']==1 || $access[$cars]['can_create']==1 || $access[$cars]['can_view']==1 || $access[$aim_fairs]['can_create']==1 || $access[$aim_fairs]['can_view']==1 || $access[$rma_and_ship_chart]['can_view']==1 || $access[$customer_returns]['can_view']==1 || $access[$rma_dashboard]['can_view']==1 || $access[$scrap_report]['can_view']==1 || $access[$defects]['can_create']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <polygon points="0 0 24 0 24 24 0 24"/>
											        <polygon fill="#000000" opacity="0.3" points="10.0888887 24 14.5333331 18 12.3111109 18 12.3111109 14 7.86666648 20 10.0888887 20"/>
											        <path d="M5.74714567,14.0425758 C4.09410362,12.9740356 3,11.1147886 3,9 C3,5.6862915 5.6862915,3 9,3 C11.7957591,3 14.1449096,4.91215918 14.8109738,7.5 L17.25,7.5 C19.3210678,7.5 21,9.17893219 21,11.25 C21,13.3210678 19.3210678,15 17.25,15 L8.25,15 C7.28817895,15 6.41093178,14.6378962 5.74714567,14.0425758 Z" fill="#000000"/>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Quality</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Quality</span></span></li>
													<?php if($access[$defects]['can_view']==1 || $access[$defect_type]['can_view']==1 || $access[$defects]['can_create']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Defects</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$defect_type]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('defects/types'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Defect Types</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$defects]['can_create']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('defects/entry'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Defect Entry</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$defects]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('defects'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">All Defects</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('defects/dashboard'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Defect Dashboard</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('defects/chart_by_job'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Chart By JOB#</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('defects/chart_by_aim_num'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Chart By AIM#</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('defects/chart_by_date_range'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Chart By Date</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$job_list_rma]['can_view']==1 || $access[$rma_and_ship_chart]['can_view']==1 || $access[$customer_returns]['can_view']==1 || $access[$rma_dashboard]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">RMA</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$job_list_rma]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/job_list_rma'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Job List RMA</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$rma_and_ship_chart]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/rma_and_ship_chart'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">RMA &amp; Ship Chart</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$customer_returns]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('quality/customer_returns'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Customer Returns</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$rma_dashboard]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('quality/rma_dashboard'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">RMA Dashboard</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$quality_alerts]['can_create']==1 || $access[$test_yield]['can_create']==1 || $access[$smt_yield]['can_create']==1 || $access[$final_qa_yield]['can_create']==1 || $access[$production_test]['can_create']==1 || $access[$customer_returns_test]['can_create']==1 || $access[$customer_returns_qa]['can_create']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Quality Entry</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$quality_alerts]['can_create']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/quality_alert_entry'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Quality Alerts Entry</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$test_yield]['can_create']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/test_yield_entry'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Test Yield Entry</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$smt_yield]['can_create']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/smt_yield_entry'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">SMT Yield Entry</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$final_qa_yield]['can_create']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/final_qa_yield_entry'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Final QA Yield Entry</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$production_test]['can_create']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/production_test_entry'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Production Test Entry</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$customer_returns_test]['can_create']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/customer_return_test_entry'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Customer Return Test Entry</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$customer_returns_qa]['can_create']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/customer_return_qa_entry'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Customer Return QA Entry</span>
																		</a>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$test_yield]['can_view']==1 || $access[$smt_yield]['can_view']==1 || $access[$final_qa_yield]['can_view']==1 || $access[$production_test]['can_view']==1 || $access[$customer_returns_test]['can_view']==1 || $access[$customer_returns_qa]['can_view']==1 || $access[$quality_alerts]['can_view']==1 || $access[$scrap_report]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Quality Data</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$test_yield]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/test_yield'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">All Test Yield</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$smt_yield]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/smt_yield'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">All SMT Yield</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$final_qa_yield]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/final_qa_yield'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">All Final QA Yield</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$production_test]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/production_test_list'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Production Test List</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$customer_returns_test]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/customer_return_test_list'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Customer Return Test List</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$customer_returns_qa]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/customer_return_qa_list'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Customer Return QA List</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$quality_alerts]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/all_quality_alerts'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Quality Alerts List</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$scrap_report]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/scrap_report'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Scrap Report</span>
																		</a>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$test_yield]['can_view']==1 || $access[$smt_yield]['can_view']==1 || $access[$final_qa_yield]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Quality Data By Month</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$test_yield]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/first_pass_test_yield'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">Test FPY By Month (1 Year)</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$smt_yield]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/smt_fpy_by_month'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">SMT FPY By Month (1 Year)</span>
																		</a>
																	</li>
																<?php } ?>
																<?php if($access[$final_qa_yield]['can_view']==1){ ?>
																	<li class="kt-menu__item " aria-haspopup="true">
																		<a href="<?php echo base_url('quality/fqa_fpy_by_month'); ?>" class="kt-menu__link " target="_blank">
																			<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																			<span class="kt-menu__link-text">FQA FPY By Month (1 Year)</span>
																		</a>
																	</li>
																<?php } ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('quality/on_time_delivery_by_month'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">On Time Delivery By Month (1 Year)</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$quality_categories]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('quality/categories'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Categories</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$cars]['can_create']==1 || $access[$cars]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Corrective Action</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$cars]['can_create']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('cars/new_car'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">New Car</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$cars]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('cars'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">All Cars</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('cars/open'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Open Cars</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$aim_fairs]['can_create']==1 || $access[$aim_fairs]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Aim Fair</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$aim_fairs]['can_create']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('fairs/new_fair'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">New Fair</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$aim_fairs]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('fairs/review'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Fair For Review</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('fairs/completed'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Completed Fair</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $stencils=array_search('stencils', array_column($access, 'param')); 
						            $wave_soldering_pallets=array_search('wave_soldering_pallets', array_column($access, 'param')); 
						            $equipment_information=array_search('equipment_information', array_column($access, 'param')); 
						            $multi_part_inventory=array_search('multi_part_inventory', array_column($access, 'param')); 
						            if($access[$stencils]['can_view']==1 || $access[$equipment_information]['can_view']==1 || $access[$multi_part_inventory]['can_view']==1 || $access[$wave_soldering_pallets]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <rect fill="#000000" opacity="0.3" x="7" y="4" width="3" height="13" rx="1.5"/>
											        <rect fill="#000000" opacity="0.3" x="12" y="9" width="3" height="8" rx="1.5"/>
											        <path d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z" fill="#000000" fill-rule="nonzero"/>
											        <rect fill="#000000" opacity="0.3" x="17" y="11" width="3" height="6" rx="1.5"/>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Miscellaneous</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Miscellaneous</span></span></li>
													<?php if($access[$stencils]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('miscellaneous/stencils'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Stencil</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$wave_soldering_pallets]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('miscellaneous/wave_soldering_pallets'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Wave Soldering Pallets</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$equipment_information]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('miscellaneous/equipment_information'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Equipment Information</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$multi_part_inventory]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('miscellaneous/multi_part_inventory'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Multi Part Inventory</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $costed_bom=array_search('costed_bom', array_column($access, 'param')); 
						            $sales_backlog=array_search('sales_backlog', array_column($access, 'param'));
						            $sales_backlog_customer=array_search('sales_backlog_customer', array_column($access, 'param'));
						            $sales_report=array_search('sales_report', array_column($access, 'param'));
			            			if($access[$costed_bom]['can_view']==1 || $access[$sales_backlog]['can_view']==1 || $access[$sales_backlog_customer]['can_view']==1 || $access[$sales_report]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <rect fill="#000000" opacity="0.3" x="2" y="5" width="20" height="14" rx="2"/>
											        <rect fill="#000000" x="2" y="8" width="20" height="3"/>
											        <rect fill="#000000" opacity="0.3" x="16" y="14" width="4" height="2" rx="1"/>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Sales</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Sales</span></span></li>
													<?php if($access[$costed_bom]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('sales/costed_bom'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Costed BOM</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$sales_backlog]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('sales/sales_backlog'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Sales Backlog</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$sales_backlog_customer]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('sales/sales_backlog_customer'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Sales Backlog Customer</span>
														</a>
													</li>
													<?php } ?>	
													<?php if($access[$sales_report]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('sales/sales_report'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Sales Report</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						           	$time_off_requests_list=array_search('time_off_requests_list', array_column($access, 'param'));
						           	$time_off_calendar=array_search('time_off_calendar', array_column($access, 'param'));
						           	$employee_master=array_search('employee_master', array_column($access, 'param'));
						           	$employee_logs=array_search('employee_logs', array_column($access, 'param'));
						           	$document_master=array_search('document_master', array_column($access, 'param'));
						           	$training_access=array_search('training', array_column($access, 'param'));
						           	$courses_access=array_search('courses', array_column($access, 'param'));
						           	$on_boarding=array_search('on_boarding', array_column($access, 'param'));
						           	$man_power_requisition=array_search('man_power_requisition', array_column($access, 'param'));
						           	$man_power_requisition_list=array_search('man_power_requisition_list', array_column($access, 'param'));
						           	$hr_team=array_search('hr_team', array_column($access, 'param'));
						           	$job_responsibilities=array_search('job_responsibilities', array_column($access, 'param'));
			          			?>
									<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
										<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
											<span class="kt-menu__link-icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <polygon points="0 0 24 0 24 24 0 24"/>
									        <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
									        <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
											    </g>
												</svg>
											</span>
											<span class="kt-menu__link-text">HR</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="kt-menu__submenu ">
											<span class="kt-menu__arrow"></span>
											<ul class="kt-menu__subnav">
												<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">HR</span></span></li>
												<?php if($access[$hr_team]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/team'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Team</span>
													</a>
												</li>
												<?php } ?>
												<?php 
												 	$settings=array_search('settings', array_column($access, 'param'));
												 	if($access[$settings]['can_view']==1){
												?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/appraisal_form'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Appraisal Form</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/appraisal_list'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Appraisal List</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/weekly_report'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Weekly Report</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/weekly_report_records'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Weekly Report Records</span>
													</a>
												</li>
												<?php } ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/holiday_list'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Holiday List</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/time_off_request'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Time off request</span>
													</a>
												</li>
												<?php if($access[$on_boarding]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/on_boarding'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">On Boarding</span>
													</a>
												</li>
												<?php } ?>
												<?php if($access[$man_power_requisition]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/man_power_requisition'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Man Power Requisition</span>
													</a>
												</li>
												<?php } ?>
												<?php if($access[$man_power_requisition_list]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/man_power_requisition_list'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">MPR List</span>
													</a>
												</li>
												<?php } ?>
												<?php if($access[$job_responsibilities]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/job_responsibilities'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Job Responsibilities</span>
													</a>
												</li>
												<?php } ?>
												<?php if($access[$time_off_requests_list]['can_view']==1){ ?>
												<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
													<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Time off request list</span>
														<i class="kt-menu__ver-arrow la la-angle-right"></i>
													</a>
													<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">
															<li class="kt-menu__item " aria-haspopup="true">
																<a href="<?php echo base_url('hr/time_off_request_approved_list'); ?>" class="kt-menu__link " target="_blank">
																	<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																	<span class="kt-menu__link-text">Approved</span>
																</a>
															</li>
															<li class="kt-menu__item " aria-haspopup="true">
																<a href="<?php echo base_url('hr/time_off_request_pending_list'); ?>" class="kt-menu__link " target="_blank">
																	<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																	<span class="kt-menu__link-text">Pending</span>
																</a>
															</li>
														</ul>
													</div>
												</li>
												<?php } ?>
												<?php if($access[$time_off_calendar]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/time_off_calendar'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Time off calendar</span>
													</a>
												</li>
												<?php } ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/my_time_off_request'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">My Time off requests</span>
													</a>
												</li>
												<?php if($access[$employee_master]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('employees'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Employee Master</span>
													</a>
												</li>
												<?php } ?>
												<?php if($access[$employee_logs]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('employees/logs'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Employee Logs</span>
													</a>
												</li>
												<?php } ?>
												<?php if($access[$document_master]['can_view']==1){ ?>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('hr/documents'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Documents Master</span>
													</a>
												</li>
												<?php } ?>
												<?php if($access[$training_access]['can_view']==1 || $access[$courses_access]['can_view']==1){ ?>
												<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
													<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Training</span>
														<i class="kt-menu__ver-arrow la la-angle-right"></i>
													</a>
													<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">
															<?php if($access[$training_access]['can_view']==1){ ?>
															<li class="kt-menu__item " aria-haspopup="true">
																<a href="<?php echo base_url('training'); ?>" class="kt-menu__link " target="_blank">
																	<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																	<span class="kt-menu__link-text">Training</span>
																</a>
															</li>
															<?php } ?>
															<?php if($access[$courses_access]['can_view']==1){ ?>
															<li class="kt-menu__item " aria-haspopup="true">
																<a href="<?php echo base_url('training/courses'); ?>" class="kt-menu__link " target="_blank">
																	<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																	<span class="kt-menu__link-text">Courses</span>
																</a>
															</li>
															<?php } ?>
														</ul>
													</div>
												</li>
												<?php } ?>
											</ul>
										</div>
									</li>
								<?php 
						            $shipped_not_invoiced=array_search('shipped_not_invoiced', array_column($access, 'param')); 
						            $shipped_amount=array_search('shipped_amount', array_column($access, 'param'));
						            $payroll=array_search('payroll', array_column($access, 'param'));
						            $aged_receivables=array_search('aged_receivables', array_column($access, 'param'));
						            $aged_receivables_rec_doc=array_search('aged_receivables_rec_doc', array_column($access, 'param'));
						            $aged_payables=array_search('aged_payables', array_column($access, 'param'));
						            $aged_payables_rec_doc=array_search('aged_payables_rec_doc', array_column($access, 'param'));
						            $customers_nda=array_search('customers_nda', array_column($access, 'param'));
						            $employees_nda=array_search('employees_nda', array_column($access, 'param'));
						            $monthly_ship=array_search('monthly_ship', array_column($access, 'param'));
						            if($access[$shipped_not_invoiced]['can_view']==1 || $access[$shipped_amount]['can_view']==1 || $access[$payroll]['can_view']==1 || $access[$aged_receivables]['can_view']==1 || $access[$aged_receivables_rec_doc]['can_view']==1 || $access[$aged_payables]['can_view']==1 || $access[$aged_payables_rec_doc]['can_view']==1 || $access[$customers_nda]['can_view']==1 || $access[$employees_nda]['can_view']==1 || $access[$monthly_ship]['can_view']==1){ 
			          					?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <path d="M13.5,6 C13.33743,8.28571429 12.7799545,9.78571429 11.8275735,10.5 C11.8275735,10.5 12.5,4 10.5734853,2 C10.5734853,2 10.5734853,5.92857143 8.78777106,9.14285714 C7.95071887,10.6495511 7.00205677,12.1418252 7.00205677,14.1428571 C7.00205677,17 10.4697177,18 12.0049375,18 C13.8025422,18 17,17 17,13.5 C17,12.0608202 15.8333333,9.56082016 13.5,6 Z" fill="#000000"/>
											        <path d="M9.72075922,20 L14.2792408,20 C14.7096712,20 15.09181,20.2754301 15.2279241,20.6837722 L16,23 L8,23 L8.77207592,20.6837722 C8.90818997,20.2754301 9.29032881,20 9.72075922,20 Z" fill="#000000" opacity="0.3"/>
												    </g>
													</svg>
												</span>
												<span class="kt-menu__link-text">Finance</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Finance</span></span></li>
													<?php if($access[$monthly_ship]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('receiving/monthly_ship'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Monthly Ship</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$shipped_not_invoiced]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('finance/shipped_not_invoices'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Shipped Not Invoiced</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$shipped_amount]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('finance/shipped_amount'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Shipped Amount</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$payroll]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('finance/payroll'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Payroll</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$aged_payables]['can_view']==1 || $access[$aged_payables_rec_doc]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Aged Payables</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu ">
															<span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$aged_payables]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('finance/aged_payables'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Aged Payables</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$aged_payables_rec_doc]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('finance/ap_receipt_document'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Receipt Documents</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('finance/ap_pending_receipt_document'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Pending Receipt Documents</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('finance/ap_complete_receipt_document'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Commplete Receipt Documents</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$aged_receivables]['can_view']==1 || $access[$aged_receivables_rec_doc]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Aged Receivables</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$aged_receivables]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('finance/aged_receivables'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Aged Receivables</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$aged_receivables_rec_doc]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('finance/ar_receipt_document'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Receipt Documents</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('finance/ar_pending_receipt_document'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Pending Receipt Documents</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('finance/ar_complete_receipt_document'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Commplete Receipt Documents</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
													<?php if($access[$customers_nda]['can_view']==1 || $access[$employees_nda]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">NDA</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<?php if($access[$customers_nda]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('nda/customer_nda'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Customer NDA</span>
																	</a>
																</li>
																<?php } ?>
																<?php if($access[$employees_nda]['can_view']==1){ ?>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('nda/employee_nda'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Employee NDA</span>
																	</a>
																</li>
																<?php } ?>
															</ul>
														</div>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
						            $roles=array_search('roles', array_column($access, 'param')); 
						            $departments=array_search('departments', array_column($access, 'param')); 
						            $users=array_search('users', array_column($access, 'param')); 
						            $settings=array_search('settings', array_column($access, 'param')); 
						            $user_wise_access=array_search('user_wise_access', array_column($access, 'param')); 
						            $access_wise_user=array_search('access_wise_user', array_column($access, 'param')); 
						            $it_requirements=array_search('it_requirements', array_column($access, 'param')); 
			            			if($access[$roles]['can_view']==1 || $access[$departments]['can_view']==1 || $access[$users]['can_view']==1 || $access[$settings]['can_view']==1 || $access[$user_wise_access]['can_view']==1 || $access[$access_wise_user]['can_view']==1 || $access[$it_requirements]['can_view']==1){ 
			            				?>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											    	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3"/>
											        <path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"/>
											    	</g>
													</svg>
												</span>
												<span class="kt-menu__link-text">IT</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">IT</span></span></li>
													<?php if($access[$departments]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('departments'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Departments</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$roles]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('roles'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Roles</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$users]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('users'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Users</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$user_wise_access]['can_view']==1 || $access[$access_wise_user]['can_view']==1){ ?>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="<?php echo base_url('users/user_access'); ?>" class="kt-menu__link " target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">User Access</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$it_requirements]['can_view']==1){ ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">IT Requirements</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('it/requirements'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">IT Requirements</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('it/pending_requirements'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Pending Requirements</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="<?php echo base_url('it/complete_requirements'); ?>" class="kt-menu__link " target="_blank">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																		<span class="kt-menu__link-text">Complete Requirements</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
								 	$settings=array_search('settings', array_column($access, 'param'));
								 	if($access[$settings]['can_view']==1){
									?>
									<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
										<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
											<span class="kt-menu__link-icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										        <rect x="0" y="0" width="24" height="24"/>
										        <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>
											    </g>
												</svg>
											</span>
											<span class="kt-menu__link-text">Settings</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="kt-menu__submenu ">
											<span class="kt-menu__arrow"></span>
											<ul class="kt-menu__subnav">
												<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Settings</span></span></li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('settings'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Basic Detail</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('settings/email_templates'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Email Templates</span>
													</a>
												</li>
												<li class="kt-menu__item " aria-haspopup="true">
													<a href="<?php echo base_url('settings/email_trigger'); ?>" class="kt-menu__link " target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Email Trigger</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
								<?php } ?>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect x="0" y="0" width="24" height="24"/>
									        <path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="#000000"/>
									        <path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="#000000" opacity="0.3"/>
										    </g>
											</svg>
										</span>
										<span class="kt-menu__link-text">Support</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu ">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Support</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true">
												<a href="<?php echo base_url('support/add_ticket'); ?>" class="kt-menu__link " target="_blank">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">Add Ticket</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true">
												<a href="<?php echo base_url('support/my_tickets'); ?>" class="kt-menu__link " target="_blank">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">My Tickets</span>
												</a>
											</li>
											<?php if($access[$settings]['can_view']==1){ ?>
											<li class="kt-menu__item " aria-haspopup="true">
												<a href="<?php echo base_url('support/all_tickets'); ?>" class="kt-menu__link " target="_blank">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">All Tickets</span>
												</a>
											</li>
											<li class="kt-menu__item " aria-haspopup="true">
												<a href="<?php echo base_url('support/assign_tickets'); ?>" class="kt-menu__link " target="_blank">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">Assign Tickets</span>
												</a>
											</li>
											<?php } ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
						<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
						<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
							<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
								<ul class="kt-menu__nav ">
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="http://www.aimtron.com/" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AC.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Corporation" />
										</a>
									</li>
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="http://www.aimtron.in" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AE.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Electronics Pvt. Ltd." />
										</a>
									</li>
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="http://www.aimtronsystems.com" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AS.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Systems" />
										</a>
									</li>	
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="https://www.aimdzn.com" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AD.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Design Studio" />
										</a>
									</li>	
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="https://www.aimtronfoundation.org/" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AF.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Foundation" />
										</a>
									</li>
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="https://www.american-pinball.com/" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AP.png'); ?>" width="50" data-toggle="kt-tooltip" title="American Pinball" />
										</a>
									</li>	
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="https://www.youtube.com/channel/UC7Y297T8LOnQRpK3ZIkqZNA" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/youtube.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Media" />
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="kt-header__topbar">
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper" >
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand"  data-toggle="kt-tooltip" title="Conference Bridge">
										<a href="javascript:;" onclick="$('#conference_modal').modal('show');">
											<img src="<?php echo base_url('assets/img/companies/code4.png'); ?>" width="25">
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item dropdown">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px" aria-expanded="true">
									<span class="kt-header__topbar-icon" data-toggle="kt-tooltip" title="IPC Document">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24" />
												<rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5" />
												<rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5" />
												<rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5" />
												<rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5" />
											</g>
										</svg> 
									</span>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
									<form>
										<div class="kt-head kt-head--skin-dark" style="background-image: url(assets/media/misc/bg-1.jpg)">
											<h3 class="kt-head__title">
												Documents
												<span class="kt-space-15"></span>
												<span class="btn btn-success btn-sm btn-bold btn-font-md">2 Documents</span>
											</h3>
										</div>
										<div class="kt-grid-nav kt-grid-nav--skin-light">
											<div class="kt-grid-nav__row">
												<a href="<?php echo base_url('documents/general/IPC-A-600G.pdf'); ?>" target="_blank" class="kt-grid-nav__item">
													<span class="kt-grid-nav__icon">
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--lg">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<rect x="0" y="0" width="24" height="24" />
																<path d="M14.8571499,13 C14.9499122,12.7223297 15,12.4263059 15,12.1190476 L15,6.88095238 C15,5.28984632 13.6568542,4 12,4 L11.7272727,4 C10.2210416,4 9,5.17258756 9,6.61904762 L10.0909091,6.61904762 C10.0909091,5.75117158 10.823534,5.04761905 11.7272727,5.04761905 L12,5.04761905 C13.0543618,5.04761905 13.9090909,5.86843034 13.9090909,6.88095238 L13.9090909,12.1190476 C13.9090909,12.4383379 13.8240964,12.7385644 13.6746497,13 L10.3253503,13 C10.1759036,12.7385644 10.0909091,12.4383379 10.0909091,12.1190476 L10.0909091,9.5 C10.0909091,9.06606198 10.4572216,8.71428571 10.9090909,8.71428571 C11.3609602,8.71428571 11.7272727,9.06606198 11.7272727,9.5 L11.7272727,11.3333333 L12.8181818,11.3333333 L12.8181818,9.5 C12.8181818,8.48747796 11.9634527,7.66666667 10.9090909,7.66666667 C9.85472911,7.66666667 9,8.48747796 9,9.5 L9,12.1190476 C9,12.4263059 9.0500878,12.7223297 9.14285008,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L14.8571499,13 Z" fill="#000000" opacity="0.3" />
																<path d="M9,10.3333333 L9,12.1190476 C9,13.7101537 10.3431458,15 12,15 C13.6568542,15 15,13.7101537 15,12.1190476 L15,10.3333333 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9,10.3333333 Z M10.0909091,11.1212121 L12,12.5 L13.9090909,11.1212121 L13.9090909,12.1190476 C13.9090909,13.1315697 13.0543618,13.952381 12,13.952381 C10.9456382,13.952381 10.0909091,13.1315697 10.0909091,12.1190476 L10.0909091,11.1212121 Z" fill="#000000" />
															</g>
														</svg> 
													</span>
													<span class="kt-grid-nav__desc">IPC-A-600G</span>
												</a>
												<a href="<?php echo base_url('documents/general/IPC-A-610F-English(L).pdf'); ?>" target="_blank" class="kt-grid-nav__item">
													<span class="kt-grid-nav__icon">
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--lg">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																<rect x="0" y="0" width="24" height="24" />
																<path d="M14.8571499,13 C14.9499122,12.7223297 15,12.4263059 15,12.1190476 L15,6.88095238 C15,5.28984632 13.6568542,4 12,4 L11.7272727,4 C10.2210416,4 9,5.17258756 9,6.61904762 L10.0909091,6.61904762 C10.0909091,5.75117158 10.823534,5.04761905 11.7272727,5.04761905 L12,5.04761905 C13.0543618,5.04761905 13.9090909,5.86843034 13.9090909,6.88095238 L13.9090909,12.1190476 C13.9090909,12.4383379 13.8240964,12.7385644 13.6746497,13 L10.3253503,13 C10.1759036,12.7385644 10.0909091,12.4383379 10.0909091,12.1190476 L10.0909091,9.5 C10.0909091,9.06606198 10.4572216,8.71428571 10.9090909,8.71428571 C11.3609602,8.71428571 11.7272727,9.06606198 11.7272727,9.5 L11.7272727,11.3333333 L12.8181818,11.3333333 L12.8181818,9.5 C12.8181818,8.48747796 11.9634527,7.66666667 10.9090909,7.66666667 C9.85472911,7.66666667 9,8.48747796 9,9.5 L9,12.1190476 C9,12.4263059 9.0500878,12.7223297 9.14285008,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L14.8571499,13 Z" fill="#000000" opacity="0.3" />
																<path d="M9,10.3333333 L9,12.1190476 C9,13.7101537 10.3431458,15 12,15 C13.6568542,15 15,13.7101537 15,12.1190476 L15,10.3333333 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9,10.3333333 Z M10.0909091,11.1212121 L12,12.5 L13.9090909,11.1212121 L13.9090909,12.1190476 C13.9090909,13.1315697 13.0543618,13.952381 12,13.952381 C10.9456382,13.952381 10.0909091,13.1315697 10.0909091,12.1190476 L10.0909091,11.1212121 Z" fill="#000000" />
															</g>
														</svg> 
													</span>
													<span class="kt-grid-nav__desc">IPC-A-610F-English(L)</span>
												</a>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper" >
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="Extenstion Directory">
										<a href="<?php echo base_url('home/extension_directory'); ?>" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/code.png'); ?>" width="25">
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper" >
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="Organizational Chart">
										<a href="<?php echo base_url('documents/general/organization_chart.pdf'); ?>" target="_blank">
											<span class="svg-icon svg-icon-primary svg-icon-2x">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												        <rect x="0" y="0" width="24" height="24"/>
												        <path d="M16.5428932,17.4571068 L11,11.9142136 L11,4 C11,3.44771525 11.4477153,3 12,3 C12.5522847,3 13,3.44771525 13,4 L13,11.0857864 L17.9571068,16.0428932 L20.1464466,13.8535534 C20.3417088,13.6582912 20.6582912,13.6582912 20.8535534,13.8535534 C20.9473216,13.9473216 21,14.0744985 21,14.2071068 L21,19.5 C21,19.7761424 20.7761424,20 20.5,20 L15.2071068,20 C14.9309644,20 14.7071068,19.7761424 14.7071068,19.5 C14.7071068,19.3673918 14.7597852,19.2402148 14.8535534,19.1464466 L16.5428932,17.4571068 Z" fill="#000000" fill-rule="nonzero"/>
												        <path d="M7.24478854,17.1447885 L9.2464466,19.1464466 C9.34021479,19.2402148 9.39289321,19.3673918 9.39289321,19.5 C9.39289321,19.7761424 9.16903559,20 8.89289321,20 L3.52893218,20 C3.25278981,20 3.02893218,19.7761424 3.02893218,19.5 L3.02893218,14.136039 C3.02893218,14.0034307 3.0816106,13.8762538 3.17537879,13.7824856 C3.37064094,13.5872234 3.68722343,13.5872234 3.88248557,13.7824856 L5.82567301,15.725673 L8.85405776,13.1631936 L10.1459422,14.6899662 L7.24478854,17.1447885 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
												    </g>
												</svg>
											</span>
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper" >
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="Sticky Notes">
										<a href="<?php echo base_url('sticky'); ?>">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect x="0" y="0" width="24" height="24"/>
								        	<path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000"/>
									        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5"/>
										    </g>
											</svg>
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper">
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="Event Manager">
										<a href="<?php echo base_url('events'); ?>">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										      <rect x="0" y="0" width="24" height="24"/>
									        <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="1"/>
									        <path d="M5,10 L7,10 C7.55228475,10 8,10.4477153 8,11 L8,13 C8,13.5522847 7.55228475,14 7,14 L5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 L14,7 C14,7.55228475 13.5522847,8 13,8 L11,8 C10.4477153,8 10,7.55228475 10,7 L10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,13 C14,13.5522847 13.5522847,14 13,14 L11,14 C10.4477153,14 10,13.5522847 10,13 L10,11 C10,10.4477153 10.4477153,10 11,10 Z M17,4 L19,4 C19.5522847,4 20,4.44771525 20,5 L20,7 C20,7.55228475 19.5522847,8 19,8 L17,8 C16.4477153,8 16,7.55228475 16,7 L16,5 C16,4.44771525 16.4477153,4 17,4 Z M17,10 L19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 L17,14 C16.4477153,14 16,13.5522847 16,13 L16,11 C16,10.4477153 16.4477153,10 17,10 Z M5,16 L7,16 C7.55228475,16 8,16.4477153 8,17 L8,19 C8,19.5522847 7.55228475,20 7,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,17 C4,16.4477153 4.44771525,16 5,16 Z M11,16 L13,16 C13.5522847,16 14,16.4477153 14,17 L14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 L10,17 C10,16.4477153 10.4477153,16 11,16 Z M17,16 L19,16 C19.5522847,16 20,16.4477153 20,17 L20,19 C20,19.5522847 19.5522847,20 19,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,17 C16,16.4477153 16.4477153,16 17,16 Z" fill="#000000"/>
										    </g>
											</svg>
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper" >
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="Quality Policy">
										<a href="<?php echo base_url('documents/quality/QM-0012-03-Quality-Policy.pdf'); ?>" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/quality.png'); ?>" width="30">
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper">
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="Visual Management Chart">
										<a href="<?php echo base_url('home/visual_management_chart'); ?>" target="_blank">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect x="0" y="0" width="24" height="24"/>
									        <polygon fill="#000000" opacity="0.3" points="6 7 6 15 18 15 18 7"/>
									        <path d="M11,19 L11,16 C11,15.4477153 11.4477153,15 12,15 C12.5522847,15 13,15.4477153 13,16 L13,19 L14.5,19 C14.7761424,19 15,19.2238576 15,19.5 C15,19.7761424 14.7761424,20 14.5,20 L9.5,20 C9.22385763,20 9,19.7761424 9,19.5 C9,19.2238576 9.22385763,19 9.5,19 L11,19 Z" fill="#000000" opacity="0.3"/>
									        <path d="M6,7 L6,15 L18,15 L18,7 L6,7 Z M6,5 L18,5 C19.1045695,5 20,5.8954305 20,7 L20,15 C20,16.1045695 19.1045695,17 18,17 L6,17 C4.8954305,17 4,16.1045695 4,15 L4,7 C4,5.8954305 4.8954305,5 6,5 Z" fill="#000000" fill-rule="nonzero"/>
										    </g>
											</svg>
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper" >
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="Health & Safety">
										<a href="<?php echo base_url('home/health_and_safe'); ?>" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/health.gif'); ?>" width="25">
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper" >
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="IT Support">
										<a href="<?php echo base_url('support/add_ticket'); ?>" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/support.png'); ?>" width="25">
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item kt-header__topbar-item--quick-panel" data-toggle="kt-tooltip" title="Change log">
				                <span class="kt-header__topbar-icon" id="kt_quick_panel_toggler_btn">
				                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
				                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				                      <rect x="0" y="0" width="24" height="24" />
				                      <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
				                      <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
				                    </g>
				                  </svg> 
				                </span>
				            </div>
							<div class="kt-header__topbar-item">
								<div class="kt-header__topbar-wrapper" >
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="Quick Email">
										<a href="javascript:;" target="_blank" data-toggle="modal" data-target="#email_modal">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect x="0" y="0" width="24" height="24"/>
									        <path d="M4,16 L5,16 C5.55228475,16 6,16.4477153 6,17 C6,17.5522847 5.55228475,18 5,18 L4,18 C3.44771525,18 3,17.5522847 3,17 C3,16.4477153 3.44771525,16 4,16 Z M1,11 L5,11 C5.55228475,11 6,11.4477153 6,12 C6,12.5522847 5.55228475,13 5,13 L1,13 C0.44771525,13 6.76353751e-17,12.5522847 0,12 C-6.76353751e-17,11.4477153 0.44771525,11 1,11 Z M3,6 L5,6 C5.55228475,6 6,6.44771525 6,7 C6,7.55228475 5.55228475,8 5,8 L3,8 C2.44771525,8 2,7.55228475 2,7 C2,6.44771525 2.44771525,6 3,6 Z" fill="#000000" opacity="0.3"/>
									        <path d="M10,6 L22,6 C23.1045695,6 24,6.8954305 24,8 L24,16 C24,17.1045695 23.1045695,18 22,18 L10,18 C8.8954305,18 8,17.1045695 8,16 L8,8 C8,6.8954305 8.8954305,6 10,6 Z M21.0849395,8.0718316 L16,10.7185839 L10.9150605,8.0718316 C10.6132433,7.91473331 10.2368262,8.02389331 10.0743092,8.31564728 C9.91179228,8.60740125 10.0247174,8.9712679 10.3265346,9.12836619 L15.705737,11.9282847 C15.8894428,12.0239051 16.1105572,12.0239051 16.294263,11.9282847 L21.6734654,9.12836619 C21.9752826,8.9712679 22.0882077,8.60740125 21.9256908,8.31564728 C21.7631738,8.02389331 21.3867567,7.91473331 21.0849395,8.0718316 Z" fill="#000000"/>
										    </g>
											</svg>
										</a>
									</span>
								</div>
							</div>
							<div class="kt-header__topbar-item dropdown">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px" aria-expanded="true">
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" data-toggle="kt-tooltip" title="User Notification">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24" />
												<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
												<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
											</g>
										</svg> 
										<span class="kt-pulse__ring"></span>
									</span>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
									<div class="kt-head kt-head--skin-dark kt-notification-item-padding-x" style="background-image: url(<?php echo base_url('assets/media/misc/bg-1.jpg'); ?>)">
										<h3 class="kt-head__title">
											User Notifications
											&nbsp;
											<?php if(count_notification() != 0){ ?>
												<span class="btn btn-success btn-sm btn-bold btn-font-md"><?php echo count_notification(); ?> new</span>
											<?php } ?>
										</h3>
									</div>
									<?php $notifications = get_latest_notification(); ?>
									<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="200" data-mobile-height="200">
										<?php if(!empty($notifications)){ foreach ($notifications as $key => $value) { ?>
											<a href="<?php echo base_url().$value['link'].'?read='.$value['id']; ?>" class="kt-notification__item">
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title">
														<?php echo $value['text']; ?>
													</div>
													<div class="kt-notification__item-time">
														<?php echo date('d M, Y h:i a',$value['added_time']); ?>
													</div>
												</div>
											</a>
										<?php } if(count_notification() > 5){ ?>
											<a href="#" class="btn btn-clean btn-bold btn-upper btn-block margin-top-10">See all notifications</a>
										<?php } }else{ ?>
											<div class="kt-grid kt-grid--ver" style="min-height: 200px;">
												<div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
					                <div class="kt-grid__item kt-grid__item--middle kt-align-center">
				                    All caught up!
				                    <br>No new notifications.
					                </div>
					            	</div>
					            </div>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="kt-header__topbar-item kt-header__topbar-item--user">
								<?php 
									$words = explode(" ", $user_view_profile['name']);
									$initials = null;
									foreach ($words as $w) {
									 	$initials .= $w[0];
									}
								?>
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
									<div class="kt-header__topbar-user" data-toggle="kt-tooltip" title="<?php echo $user_view_profile['name']; ?>">
										<span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
										<span class="kt-header__topbar-username kt-hidden-mobile"><?php echo $user_view_profile['name']; ?></span>
										<?php if($user_view_profile['profile_image']!=''){ ?>
											<img class="kt-hidden" alt="<?php echo $user_view_profile['name']; ?>" src="<?php echo base_url(); ?>documents/users/profile_images/<?php echo $user_view_profile['profile_image']; ?>" />
										<?php }else{ ?>
											<img class="kt-hidden" alt="<?php echo $user_view_profile['name']; ?>" src="<?php echo base_url(); ?>documents/users/profile_images/default.jpg" />
										<?php } ?>
										<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?php echo strtoupper($initials); ?></span>
									</div>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
									<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(<?php echo base_url(); ?>assets/media/misc/bg-1.jpg)">
										<div class="kt-user-card__avatar">
											<?php if($user_view_profile['profile_image']!=''){ ?>
												<img alt="<?php echo $user_view_profile['name']; ?>" src="<?php echo base_url(); ?>documents/users/profile_images/<?php echo $user_view_profile['profile_image']; ?>" />
											<?php }else{ ?>
												<img alt="<?php echo $user_view_profile['name']; ?>" src="<?php echo base_url(); ?>documents/users/profile_images/default.jpg" />
											<?php } ?>
											<span class="kt-hidden kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?php echo strtoupper($initials); ?></span>
										</div>
										<div class="kt-user-card__name">
											<?php echo $user_view_profile['name']; ?>
										</div>
										<div class="kt-user-card__badge">
											<!-- <span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span> -->
										</div>
									</div>
									<div class="kt-notification">
										<a href="<?php echo base_url('profile'); ?>" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-calendar-3 kt-font-success"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Profile
												</div>
												<div class="kt-notification__item-time">
													Account settings and more
												</div>
											</div>
										</a>
										<div class="kt-notification__custom kt-space-between">
											<a href="<?php echo base_url('login/logout') ?>" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="kt_quick_panel" class="kt-quick-panel">
						<div class="container">
							<div class="row padding-10">
								<div class="col-md-12">
									<h2 class="pull-left">Change Logs</h2>
									<?php $change_log_access=array_search('change_log', array_column($access, 'param'));
				    					if($access[$change_log_access]['can_create']==1 ){ ?>
				    						<button onclick="add_change_log();" type="button" class="btn btn-sm pull-right btn-clean btn-icon btn-icon-md" data-toggle="kt-tooltip" title="Add" >
												<i class="flaticon2-plus"></i>
											</button>
									<?php } ?>
								</div>
							</div>
						</div>
						<hr class="mt-0 mb-0">
						<div class="kt-quick-panel__content pt-0">
							<div class="kt-notification-v2">
								<?php $change_log_data = get_change_log();
									if(!empty($change_log_data)){
								 	foreach ($change_log_data as $change_log){ ?>
										<a href="javascript:;" class="kt-notification-v2__item">
											<div class="kt-user-card-v2 change_log_detail" data-id="<?php echo $change_log->id; ?>">
												<div class="kt-user-card-v2__pic" data-toggle="kt-tooltip" title="<?php echo $change_log->name; ?>">
													<?php if($change_log->profile_image!=''){ ?>
														<img alt="<?php echo $change_log->name; ?>" src="<?php echo base_url(); ?>documents/users/profile_images/<?php echo $change_log->profile_image; ?>">
													<?php }else{ ?>
														<img alt="<?php echo $change_log->name; ?>" src="<?php echo base_url('documents/users/profile_images/default.jpg'); ?>">
													<?php } ?>
												</div>
											</div>
											<div class="kt-notification-v2__itek-wrapper change_log_detail" data-id="<?php echo $change_log->id; ?>">
												<div class="kt-notification-v2__item-title">
													<?php	echo $change_log->title; ?>
												</div>
												<div class="kt-notification-v2__item-desc">
													<?php	echo $change_log->sub_title; ?>
												</div>
											</div>
											<span class="btn btn-sm btn-light font-weight-bolder py-1 my-lg-0 my-2 text-dark-50">
												<?php if($access[$change_log_access]['can_edit']==1 ){ ?>
											 		<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md change_log_remote" data-toggle="kt-tooltip" title="Edit" data-id="<?php echo $change_log->id; ?>">
														<i class="la la-edit"></i>
													</button>
												<?php } ?>
											</span>
										</a>
										<hr class="mt-0 mb-0">
								<?php } }else{ ?>
									<div class="alert alert-warning" role="alert">
							          <div class="alert-icon"><i class="flaticon-warning"></i></div>
							          <div class="alert-text">There is no changelog.</div>
							      	</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<script src="<?php echo base_url('assets/js/demo1/scripts.bundle.js'); ?>" type="text/javascript"></script>
						<?php $this->load->view("home/index"); ?>
					</div>
					<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
						<div class="kt-container  kt-container--fluid ">
							<div class="kt-footer__copyright">
								<?php echo date('Y'); ?>&nbsp;&copy;&nbsp;<a href="http://aimtron.com" target="_blank" class="kt-link"><?php echo get_site_setting('copyright_text'); ?></a>
							</div>
							<div class="kt-footer__menu">
								<a href="<?php echo base_url().'documents/site_settings/'.get_site_setting('privacy_policy'); ?>" target="_blank" class="kt-footer__menu-link kt-link">Privacy</a>
								<a href="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_terms'); ?>" target="_blank" class="kt-footer__menu-link kt-link">Terms</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="change_log_modal" tabindex="-1" role="dialog" aria-labelledby="changeLogModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header kt-head kt-head--skin-dark kt-notification-item-padding-x py-4 mx-0" style="background-image: url(<?php echo base_url('assets/media/bg/bg-1.jpg'); ?>)">
						<h4 class="modal-title text-white" id="changeLogModalLabel">
							<span id="change_log_modal_title"></span> - Change Log
						</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<form class="kt-form" id="change_log_form" method="post" action="<?php echo base_url('change_log/save_change_log'); ?>" enctype="multipart/form-data">
							<input type="hidden" name="change_log_id" id="change_log_id">
							<div class="form-group">
								<label class="control-lable">Title</label>
								<input type="text" name="change_log_title" id="change_log_title" class="form-control">
							</div>
							<div class="form-group">
								<label class="control-lable">Sub Title</label>
								<input type="text" name="change_log_sub_title" id="change_log_sub_title" class="form-control">
							</div>
							<div class="form-group">
								<label class="control-lable">Description</label>
								<textarea name="change_log_description" id="change_log_description" class="form-control summernote"></textarea>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" onclick="$('#change_log_form').submit();">Submit</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="change_log_detail_modal" tabindex="-1" role="dialog" aria-labelledby="change_log_DetailsModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header kt-head kt-head--skin-dark kt-notification-item-padding-x py-4 mx-0" style="background-image: url(<?php echo base_url('assets/media/bg/300.jpg'); ?>)">
						<h2 class="modal-title text-white" id="change_log_DetailsModalLabel">
							<span id="change_log_detail_modal_title"></span>
						</h2>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<!-- <h4 id="change_log_detail_title"></h4> -->
						<span id="change_log_detail_sub_title"></span>
						<hr/>
						<div id="change_log_detail_description"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="conference_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Aimtron Conference Bridge
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<h4 class="kt-font-danger"><i class="la la-warning"></i> DO NOT SHARE OUR PIN NUMBER TO ANYONE</h4>
						<div class="row">
							<div class="col-md-6">
								<h6>Host Instruction (This will be you)</h6>
								<ol class="list-padding">
									<li>Call your dial-in number: (425) 585-6052</li>
									<li>Enter the access code: 757-377-543</li>
									<li>Enter the PIN: 5327</li>
								</ol>
							</div>
							<div class="col-md-6">
								<h6>Participant instruction (This is for end user)</h6>
								<ol class="list-padding">
									<li>Call your dial-in number: (425) 585-6052</li>
									<li>Enter the access code: 757-377-543</li>
								</ol>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-md-12">
								<h6>Connect across the globe.Provide participants with a local in-country dial-in number from any of the countries listed below. (i.e) -</h6>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<ul class="list-padding">
									<li>Australia: +61 2 6194 9948</li>
									<li>Germany: +49 221 98203413</li>
									<li>India: +91 172 519 9283</li>
								</ul>
							</div>
							<div class="col-md-4">
								<ul class="list-padding">
									<li>Mexico: +52 33 4774 8479</li>
									<li>Singapore: +65 3138 9251</li>
									<li>Taiwan: +886 985 646 960</li>
								</ul>
							</div>
							<div class="col-md-4">
								<ul class="list-padding">
									<li>Japan: +81 3-5050-5118</li>
									<li>United Kingdom: +44 330 088 1935</li>
									<li>United States: +1 425-535-9853</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a class="pull-right" href="<?php echo base_url('documents/conferences/instruction.pdf'); ?>" target="_blank">View more countries.....</a>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="email_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Send Email
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<form class="kt-form" id="email_form" method="post" action="<?php echo base_url('home/send_email'); ?>" enctype="multipart/form-data">
							<div class="form-group">
								<label class="control-lable">To</label>
								<input type="text" name="email_to" id="email_to">
							</div>
							<div class="form-group">
								<label class="control-lable">CC</label>
								<input type="text" name="email_cc" id="email_cc">
							</div>
							<div class="form-group">
								<label class="control-lable">BCC</label>
								<input type="text" name="email_bcc" id="email_bcc">
							</div>
							<div class="form-group">
								<label class="control-lable">Subject</label>
								<input type="text" name="email_subject" id="email_subject" class="form-control">
							</div>
							<div class="form-group">
								<label class="control-lable">Message</label>
								<textarea name="email_message" id="email_message" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label class="control-lable">Attachment</label><br />
								<input type="file" name="email_attachment" id="email_attachment">
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" onclick="$('#email_form').submit();">Send</button>
					</div>
				</div>
			</div>
		</div>
		
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		
		<script src="<?php echo base_url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js'); ?>" type="text/javascript"></script>
		<?php echo $cssincludes; ?>
		<script type="text/javascript">
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
			$.fn.dataTable.moment('MM/DD/YYYY');
			$(document).ready(function(){
				var input_to = document.getElementById('email_to'),tagify = new Tagify(input_to);
				var input_cc = document.getElementById('email_cc'),tagify = new Tagify(input_cc);
				var input_bcc = document.getElementById('email_bcc'),tagify = new Tagify(input_bcc);
				$('#email_form').validate({
					rules: {
						email_to: {
							required: true
						},
						email_subject:{
							required:true
						},
						email_message:{
							required:true
						}
					}
				});
				$('#change_log_form').validate({
					rules: {
						change_log_title: {
							required: true
						},
						change_log_sub_title:{
							required:true
						},
						change_log_description:{
							required:true
						}
					}
				});
				$('.summernote').summernote({ height: 300 });
				$('.change_log_remote').click(function(){
					$.ajax({
			      type:'POST',   
			      url: "<?php echo base_url('change_log/change_log_remote'); ?>",
			      data: {"change_log_id":$(this).attr("data-id")},
			      success: function(data){
			        var json = jQuery.parseJSON(data);
			        $('#change_log_id').val(json.change_log.id);
			        $('#change_log_title').val(json.change_log.title);
			        $('#change_log_sub_title').val(json.change_log.sub_title);
							$('#change_log_description').summernote('code', json.change_log.description);
			        $('#change_log_modal_title').html('Edit <b>"'+json.change_log.title+"\"</b>")
			        $('#change_log_modal').modal('show');
			    	}
			  	});
				});
				$('.change_log_detail').click(function(){
					$.ajax({
			      type:'POST',   
			      url: "<?php echo base_url('change_log/change_log_remote'); ?>",
			      data: {"change_log_id":$(this).attr("data-id")},
			      success: function(data){
			        var json = jQuery.parseJSON(data);
			        // $('#detail_title').html(json.change_log.title);
			        $('#change_log_detail_sub_title').html(json.change_log.sub_title);
			        $('#change_log_detail_description').html(json.change_log.description);
			        $('#change_log_detail_modal_title').html('<b>'+json.change_log.title+'</b>')
			        $('#change_log_detail_modal').modal('show');
			    	}
			  	});
				});
				var cur_link="<?php echo base_url().uri_string(); ?>";
      			$("a").each(function(){
			        if($(this).attr('href')==cur_link){
			          $(this).parents("li").addClass("kt-menu__item--active");
			          $(this).closest('li').closest('li.kt-menu__item--submenu').addClass('kt-menu__item--open kt-menu__item--here');
			          $(this).closest('li').parents('li.kt-menu__item--submenu').addClass('kt-menu__item--open kt-menu__item--here');
			          $(this).closest('li').addClass('kt-menu__item--active');
			        }
			    });
	     		toastr.options = {
				  	"closeButton": false,
				  	"debug": false,
				  	"newestOnTop": false,
				  	"progressBar": true,
				  	"positionClass": "toast-top-right",
				  	"preventDuplicates": false,
				 	"onclick": null,
				  	"showDuration": "100",
				  	"hideDuration": "1000",
				  	"timeOut": "5000",
				  	"extendedTimeOut": "1000",
				  	"showEasing": "swing",
				  	"hideEasing": "linear",
				  	"showMethod": "slideDown",
				  	"hideMethod": "slideUp"
				};
				<?php if($this->session->flashdata('error')!=''){ ?>
					toastr.error("<?php echo $this->session->flashdata('error'); ?>");
				<?php } ?>
				<?php if($this->session->flashdata('success')!=''){ ?>
					toastr.success("<?php echo $this->session->flashdata('success'); ?>");
				<?php } ?>
				<?php if($this->session->flashdata('welcome')!=''){ ?>
					toastr.info("<?php echo $this->session->flashdata('welcome'); ?>");
				<?php } ?>
			});
			function add_change_log(){
				$('#change_log_id').val("");
				$('#change_log_title').val("");
				$('#change_log_sub_title').val("");
				$('#change_log_description').summernote('code', "");
			  $('#change_log_modal_title').html("Add ");
			  $('#change_log_modal').modal('show');
			}
		</script>
	</body>
</html>