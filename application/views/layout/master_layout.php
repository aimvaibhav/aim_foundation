<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php if(isset($page_title)) : ?><?php echo $page_title ?> <?php endif; ?></title>
		<meta name="description" content="Updates and statistics">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<link href="<?php echo base_url(); ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/vendors/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/custom.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('assets/vendors/custom/datatables/datatables.bundle.css'); ?>"  rel="stylesheet" type="text/css">
		<?php if(get_site_setting('company_favicon')==''){ ?>
			<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/media/logos/favicon.png" />
		<?php }else{ ?>
			<link rel="shortcut icon" href="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_favicon'); ?>" />
		<?php } ?>
		<script src="<?php echo base_url(); ?>assets/vendors/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js" type="text/javascript"></script>
		<script src="//cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js" type="text/javascript"></script>
	</head>
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<?php $access=$this->access->can_access(); ?>
		<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
		<div id="kt_header_mobile" class="kt-header-mobile kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="<?php echo base_url(); ?>">
					<?php if(get_site_setting('company_logo')==''){ ?>
						<img alt="Logo" class="logo_width" src="<?php echo base_url(); ?>assets/media/logos/aimtron.png" />
					<?php }else{ ?>
						<img alt="Logo" class="logo_width" src="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_logo'); ?>">
					<?php } ?>
					<?php if(get_site_setting('company_favicon')==''){ ?>
						<img src="<?php echo base_url(); ?>assets/media/logos/favicon.png" width="30%" />
					<?php }else{ ?>
						<img src="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_favicon'); ?>" width="30%" />
					<?php } ?>
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
			</div>
		</div>
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
				<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
					<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
						<div class="kt-aside__brand-logo">
							<a href="<?php echo base_url(); ?>">
								<?php if(get_site_setting('company_logo')==''){ ?>
									<img alt="Logo" src="<?php echo base_url(); ?>assets/media/logos/aimtron.png" width="100%" />
								<?php }else{ ?>
									<img alt="Logo" src="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_logo'); ?>" width="100%">
								<?php } ?>
							</a>
						</div>
						<div class="kt-aside__brand-tools">
							<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
								<span>
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
											<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
											<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
										</g>
									</svg>
								</span>
								<span>
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
											<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
											<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
										</g>
									</svg>
								</span>
							</button>
						</div>
					</div>
					<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
						<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
							<ul class="kt-menu__nav">
								<li class="kt-menu__item" aria-haspopup="true">
									<a href="<?php echo base_url('home'); ?>" class="kt-menu__link" target="_blank">
										<span class="kt-menu__link-icon" >
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon id="Bound" points="0 0 24 0 24 24 0 24" />
													<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" id="Shape" fill="#000000" fill-rule="nonzero" />
													<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" id="Path" fill="#000000" opacity="0.3" />
												</g>
											</svg>
										</span>
										<span class="kt-menu__link-text">Dashboard</span>
									</a>
								</li>
								<?php 
						           	$time_off_requests_list=array_search('time_off_requests_list', array_column($access, 'param'));
						           	$time_off_calendar=array_search('time_off_calendar', array_column($access, 'param'));
			          			?>
									<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
										<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
											<span class="kt-menu__link-icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <polygon points="0 0 24 0 24 24 0 24"/>
									        <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
									        <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
											    </g>
												</svg>
											</span>
											<span class="kt-menu__link-text">HR</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="kt-menu__submenu">
											<span class="kt-menu__arrow"></span>
											<ul class="kt-menu__subnav">
												<li class="kt-menu__item kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">HR</span></span></li>
												<li class="kt-menu__item" aria-haspopup="true">
													<a href="<?php echo base_url('hr/holiday_list'); ?>" class="kt-menu__link" target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Holiday List</span>
													</a>
												</li>
												<li class="kt-menu__item" aria-haspopup="true">
													<a href="<?php echo base_url('hr/time_off_request'); ?>" class="kt-menu__link" target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Time off request</span>
													</a>
												</li>
												<?php if($access[$time_off_requests_list]['can_view']==1){ ?>
												<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
													<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Time off request list</span>
														<i class="kt-menu__ver-arrow la la-angle-right"></i>
													</a>
													<div class="kt-menu__submenu"><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">
															<li class="kt-menu__item" aria-haspopup="true">
																<a href="<?php echo base_url('hr/time_off_request_approved_list'); ?>" class="kt-menu__link" target="_blank">
																	<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																	<span class="kt-menu__link-text">Approved</span>
																</a>
															</li>
															<li class="kt-menu__item" aria-haspopup="true">
																<a href="<?php echo base_url('hr/time_off_request_pending_list'); ?>" class="kt-menu__link" target="_blank">
																	<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
																	<span class="kt-menu__link-text">Pending</span>
																</a>
															</li>
														</ul>
													</div>
												</li>
												<?php } ?>
												<?php if($access[$time_off_calendar]['can_view']==1){ ?>
												<li class="kt-menu__item" aria-haspopup="true">
													<a href="<?php echo base_url('hr/time_off_calendar'); ?>" class="kt-menu__link" target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Time off calendar</span>
													</a>
												</li>
												<?php } ?>
												<li class="kt-menu__item" aria-haspopup="true">
													<a href="<?php echo base_url('hr/my_time_off_request'); ?>" class="kt-menu__link" target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">My Time off requests</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
								<?php 
						            $roles=array_search('roles', array_column($access, 'param'));
						            $departments=array_search('departments', array_column($access, 'param'));
						            $users=array_search('users', array_column($access, 'param'));
			            			if($access[$roles]['can_view']==1 || $access[$departments]['can_view']==1 || $access[$users]['can_view']==1){ 
			            				?>
										<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<span class="kt-menu__link-icon">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											    	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <rect x="0" y="0" width="24" height="24"/>
											        <path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3"/>
											        <path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"/>
											    	</g>
													</svg>
												</span>
												<span class="kt-menu__link-text">IT</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">IT</span></span></li>
													<?php if($access[$departments]['can_view']==1){ ?>
													<li class="kt-menu__item" aria-haspopup="true">
														<a href="<?php echo base_url('departments'); ?>" class="kt-menu__link" target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Departments</span>
														</a>
													</li>
													<?php } ?>
													<li class="kt-menu__item" aria-haspopup="true">
														<a href="<?php echo base_url('it/permission_items'); ?>" class="kt-menu__link" target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Permission Items</span>
														</a>
													</li>
													<?php if($access[$roles]['can_view']==1){ ?>
													<li class="kt-menu__item" aria-haspopup="true">
														<a href="<?php echo base_url('roles'); ?>" class="kt-menu__link" target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Roles</span>
														</a>
													</li>
													<?php } ?>
													<?php if($access[$users]['can_view']==1){ ?>
													<li class="kt-menu__item" aria-haspopup="true">
														<a href="<?php echo base_url('users'); ?>" class="kt-menu__link" target="_blank">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
															<span class="kt-menu__link-text">Users</span>
														</a>
													</li>
													<?php } ?>
												</ul>
											</div>
										</li>
										<?php 
									} 
								?>
								<?php 
								 	$settings=array_search('settings', array_column($access, 'param'));
								 	if($access[$settings]['can_view']==1){
									?>
									<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
										<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
											<span class="kt-menu__link-icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										        <rect x="0" y="0" width="24" height="24"/>
										        <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>
											    </g>
												</svg>
											</span>
											<span class="kt-menu__link-text">Settings</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
										</a>
										<div class="kt-menu__submenu">
											<span class="kt-menu__arrow"></span>
											<ul class="kt-menu__subnav">
												<li class="kt-menu__item kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Settings</span></span></li>
												<li class="kt-menu__item" aria-haspopup="true">
													<a href="<?php echo base_url('settings'); ?>" class="kt-menu__link" target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Basic Detail</span>
													</a>
												</li>
												<li class="kt-menu__item" aria-haspopup="true">
													<a href="<?php echo base_url('settings/email_templates'); ?>" class="kt-menu__link" target="_blank">
														<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
														<span class="kt-menu__link-text">Email Templates</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
								<?php } ?>
								<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
										<span class="kt-menu__link-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect x="0" y="0" width="24" height="24"/>
									        <path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="#000000"/>
									        <path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="#000000" opacity="0.3"/>
										    </g>
											</svg>
										</span>
										<span class="kt-menu__link-text">Support</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
									</a>
									<div class="kt-menu__submenu">
										<span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Support</span></span></li>
											<li class="kt-menu__item" aria-haspopup="true">
												<a href="<?php echo base_url('support/add_ticket'); ?>" class="kt-menu__link" target="_blank">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">Add Ticket</span>
												</a>
											</li>
											<li class="kt-menu__item" aria-haspopup="true">
												<a href="<?php echo base_url('support/my_tickets'); ?>" class="kt-menu__link" target="_blank">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">My Tickets</span>
												</a>
											</li>
											<?php if($access[$settings]['can_view']==1){ ?>
											<li class="kt-menu__item" aria-haspopup="true">
												<a href="<?php echo base_url('support/all_tickets'); ?>" class="kt-menu__link" target="_blank">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">All Tickets</span>
												</a>
											</li>
											<li class="kt-menu__item" aria-haspopup="true">
												<a href="<?php echo base_url('support/assign_tickets'); ?>" class="kt-menu__link" target="_blank">
													<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
													<span class="kt-menu__link-text">Assign Tickets</span>
												</a>
											</li>
											<?php } ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
						<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
						<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
							<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile kt-header-menu--layout-default ">
								<ul class="kt-menu__nav">
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="http://www.aimtron.com/" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AC.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Corporation" />
										</a>
									</li>
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="http://www.aimtron.in" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AE.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Electronics Pvt. Ltd." />
										</a>
									</li>
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="http://www.aimtronsystems.com" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AS.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Systems" />
										</a>
									</li>	
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="https://www.aimdzn.com" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AD.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Design Studio" />
										</a>
									</li>	
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="https://www.aimtronfoundation.org/" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AF.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Foundation" />
										</a>
									</li>
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="https://www.american-pinball.com/" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/AP.png'); ?>" width="50" data-toggle="kt-tooltip" title="American Pinball" />
										</a>
									</li>	
									<li class="kt-menu__item kt-menu__item--rel">
										<a href="https://www.youtube.com/channel/UC7Y297T8LOnQRpK3ZIkqZNA" class="kt-menu__link" target="_blank">
											<img src="<?php echo base_url('assets/img/companies/youtube.png'); ?>" width="50" data-toggle="kt-tooltip" title="Aimtron Media" />
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="kt-header__topbar">
							<div class="kt-header__topbar-item kt-header__topbar-item--user">
								<?php 
									$words = explode(" ", $this->session->userdata('name'));
									$initials = null;
									foreach ($words as $w) {
									 	$initials .= $w[0];
									}
								?>
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
									<div class="kt-header__topbar-user" data-toggle="kt-tooltip" title="<?php echo $this->session->userdata('name'); ?>">
										<span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
										<span class="kt-header__topbar-username kt-hidden-mobile"><?php echo $this->session->userdata('name'); ?></span>
										<?php if($this->session->userdata('profile_image')!=''){ ?>
											<img class="kt-hidden" alt="<?php echo $this->session->userdata('name'); ?>" src="<?php echo base_url(); ?>documents/users/profile_images/<?php echo $this->session->userdata('profile_image'); ?>" />
										<?php }else{ ?>
											<img class="kt-hidden" alt="<?php echo $this->session->userdata('name'); ?>" src="<?php echo base_url(); ?>documents/users/profile_images/default.jpg" />
										<?php } ?>
										<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?php echo strtoupper($initials); ?></span>
									</div>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
									<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(<?php echo base_url(); ?>assets/media/misc/bg-1.jpg)">
										<div class="kt-user-card__avatar">
											<?php if($this->session->userdata('profile_image')!=''){ ?>
												<img alt="<?php echo $this->session->userdata('name'); ?>" src="<?php echo base_url(); ?>documents/users/profile_images/<?php echo $this->session->userdata('profile_image'); ?>" />
											<?php }else{ ?>
												<img alt="<?php echo $this->session->userdata('name'); ?>" src="<?php echo base_url(); ?>documents/users/profile_images/default.jpg" />
											<?php } ?>
											<span class="kt-hidden kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?php echo strtoupper($initials); ?></span>
										</div>
										<div class="kt-user-card__name">
											<?php echo $this->session->userdata('name'); ?>
										</div>
										<div class="kt-user-card__badge">
											<!-- <span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span> -->
										</div>
									</div>
									<div class="kt-notification">
										<a href="<?php echo base_url('profile'); ?>" class="kt-notification__item">
											<div class="kt-notification__item-icon">
												<i class="flaticon2-calendar-3 kt-font-success"></i>
											</div>
											<div class="kt-notification__item-details">
												<div class="kt-notification__item-title kt-font-bold">
													My Profile
												</div>
												<div class="kt-notification__item-time">
													Account settings and more
												</div>
											</div>
										</a>
										<div class="kt-notification__custom kt-space-between">
											<a href="<?php echo base_url('login/logout') ?>" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<script src="<?php echo base_url('assets/js/demo1/scripts.bundle.js'); ?>" type="text/javascript"></script>
						<?php echo $content; ?>
					</div>
					<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
						<div class="kt-container  kt-container--fluid ">
							<div class="kt-footer__copyright">
								<?php echo date('Y'); ?>&nbsp;&copy;&nbsp;<a href="http://aimtron.com" target="_blank" class="kt-link"><?php echo get_site_setting('copyright_text'); ?></a>
							</div>
							<div class="kt-footer__menu">
								<a href="<?php echo base_url().'documents/site_settings/'.get_site_setting('privacy_policy'); ?>" target="_blank" class="kt-footer__menu-link kt-link">Privacy</a>
								<a href="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_terms'); ?>" target="_blank" class="kt-footer__menu-link kt-link">Terms</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<script src="<?php echo base_url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js'); ?>" type="text/javascript"></script>
		<?php echo $cssincludes; ?>
		<script type="text/javascript">
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
			$.fn.dataTable.moment('MM/DD/YYYY');
			$(document).ready(function(){
				var cur_link="<?php echo base_url().uri_string(); ?>";
      			$("a").each(function(){
			        if($(this).attr('href')==cur_link){
			          $(this).parents("li").addClass("kt-menu__item--active");
			          $(this).closest('li').closest('li.kt-menu__item--submenu').addClass('kt-menu__item--open kt-menu__item--here');
			          $(this).closest('li').parents('li.kt-menu__item--submenu').addClass('kt-menu__item--open kt-menu__item--here');
			          $(this).closest('li').addClass('kt-menu__item--active');
			        }
			    });
	     		toastr.options = {
				  	"closeButton": false,
				  	"debug": false,
				  	"newestOnTop": false,
				  	"progressBar": true,
				  	"positionClass": "toast-top-right",
				  	"preventDuplicates": false,
				 	"onclick": null,
				  	"showDuration": "100",
				  	"hideDuration": "1000",
				  	"timeOut": "5000",
				  	"extendedTimeOut": "1000",
				  	"showEasing": "swing",
				  	"hideEasing": "linear",
				  	"showMethod": "slideDown",
				  	"hideMethod": "slideUp"
				};
				<?php if($this->session->flashdata('error')!=''){ ?>
					toastr.error("<?php echo $this->session->flashdata('error'); ?>");
				<?php $this->session->unset_userdata('error'); } ?>
				<?php if($this->session->flashdata('success')!=''){ ?>
					toastr.success("<?php echo $this->session->flashdata('success'); ?>");
				<?php $this->session->unset_userdata('success'); } ?>
				<?php if($this->session->flashdata('welcome')!=''){ ?>
					toastr.info("<?php echo $this->session->flashdata('welcome'); ?>");
				<?php $this->session->unset_userdata('welcome'); } ?>
			});
		</script>
	</body>
</html>