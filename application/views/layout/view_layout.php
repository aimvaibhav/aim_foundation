<html>
	<head>
		<title><?php if(isset($page_title)) : ?><?php echo $page_title ?> <?php endif; ?></title>
		<?php if(get_site_setting('company_favicon')==''){ ?>
			<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/media/logos/favicon.png" />
		<?php }else{ ?>
			<link rel="shortcut icon" href="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_favicon'); ?>" />
		<?php } ?>
		<link href="<?php echo base_url(); ?>assets/vendors/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			.table-font-size th,.table-font-size td{
			    font-size: 12px;
			  	font-weight: normal;
			}
			.redgly{
				color: #b40404;
				padding-left:2px;
			}
			.geeengly{
				color: #3c763d;
				padding-left: 10px;
			}
			pre{
			  	white-space: pre-wrap;       /* css-3 */
			  	white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
			  	white-space: -pre-wrap;      /* Opera 4-6 */
			  	white-space: -o-pre-wrap;    /* Opera 7 */
			  	word-wrap: break-word; 
			  	border: none;
			  	background: inherit;
			  	font-family: inherit;
			  	padding: 0px;
			  	font-size: inherit;
			  	margin:0px;
			}
			.content{
				margin:auto;
			    width: 1400px;
			    padding:1em;
			    margin-top:20px;
			    margin-bottom:20px;
			    page-break-after: always;
			    border:1px solid #333;
			    box-shadow: 0 0 5px rgba(33, 33, 33, 1);
			}
			.bom_content{
				margin:0 auto;
			    width: 1140px;
			    page-break-after: always;
			}
			.mytb{
				width: 100%;
			}
			td,th{
				padding: 5px;
			}
			.econumhead{
				color: #000;
				background: #fff;
				text-align: center;
				padding: 5px;
			}
			.echead{
				color: #000;
				background: #ccc !important;
			}
			.ec{
				background: #ddd !important;
				text-align: center;
				padding: 8px;
				border-radius: 5px;
				width: 350px;
			}
			.ec h3{
				margin:0px;
				margin-bottom:10px;
			}
			.tbbd td{
				border-right:1px solid #333;
			}
			.tbbd td:last-child{
				border:none;
			}
			.actb th,.actb td{
				border:1px solid #333;
			}
			.padding-0-15{
			  padding: 0px 15px !important;
			}
			.cus-table th, .cus-table td{
			  border: 1px solid #333 !important;
			}
		</style>
		<script src="<?php echo base_url('assets/vendors/global/plugins.bundle.js'); ?>" type="text/javascript"></script>
	</head>
	<body>
		<?php echo $content; ?>
	</body>
</html>