<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php if(isset($page_title)) : ?><?php echo $page_title ?> <?php endif; ?></title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<link href="<?php echo base_url(); ?>assets/css/demo1/pages/general/login/login-1.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/vendors/global/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/media/logos/favicon.png" rel="shortcut icon" />

		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<script src="<?php echo base_url(); ?>assets/vendors/global/vendors.bundle.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/js/demo1/scripts.bundle.js" type="text/javascript"></script>
	</head>
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
					<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(<?php echo base_url(); ?>assets/media/bg/bg-4.jpg);">
						<div class="kt-grid__item">
							<a href="<?php echo base_url(); ?>" class="kt-login__logo">
								<?php if(get_site_setting('company_logo')==''){ ?>
									<img src="<?php echo base_url(); ?>assets/media/logos/aimtron.png" width="50%">
								<?php }else{ ?>
									<img src="<?php echo base_url().'documents/site_settings/'.get_site_setting('company_logo');; ?>" width="50%">
								<?php } ?>
							</a>
						</div>
						<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
							<div class="kt-grid__item kt-grid__item--middle">
								<h3 class="kt-login__title"><?php echo get_site_setting('welcome_line'); ?></h3>
								<h4 class="kt-login__subtitle"><?php echo get_site_setting('tagline'); ?></h4>
							</div>
						</div>
						<div class="kt-grid__item">
							<div class="kt-login__info">
								<div class="kt-login__copyright">
									&copy <?php echo date('Y'); ?> <?php echo get_site_setting('copyright_text'); ?>
								</div>
								<div class="kt-login__menu">
									<a href="<?php echo base_url().'documents/site_settings/'.get_site_setting('privacy_policy'); ?>" class="kt-link">Privacy</a>
									<a href="<?php echo base_url().'documents/site_settings/'.get_site_setting('comany_terms'); ?>" class="kt-link">Terms</a>
								</div>
							</div>
						</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
						<?php echo $content; ?>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
	toastr.options = {
	  "closeButton": false,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": true,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "100",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "slideDown",
	  "hideMethod": "slideUp"
	};
	<?php if($this->session->flashdata('error')!=''){ ?>
		toastr.error("<?php echo $this->session->flashdata('error'); ?>");
	<?php } ?>
	<?php if($this->session->flashdata('success')!=''){ ?>
		toastr.success("<?php echo $this->session->flashdata('success'); ?>");
	<?php } ?>
});
</script>