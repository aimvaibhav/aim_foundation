<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Sticky Notes</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
        <span class="kt-subheader__breadcrumbs-separator"></span>
        <a href="javascript:;" class="kt-subheader__breadcrumbs-link">
          Sticky Notes 
        </a>
			</div>
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
  <button class="btn btn-outline-brand btn-elevate btn-icon sticky-button" type="button" onclick="addnote();">
    <span class="la la-plus"></span>
  </button>
  <?php if($sticky_notes!=null){ ?>
  <ul class="notes">
    <?php foreach ($sticky_notes as $note) { ?>
      <li>
    	  <div 
          ondblclick="updatesticky('edit','<?php echo $note->sticky_id; ?>');"
          <?php 
            if($note->sperson!=null){ 
              echo "class='sharedsticky'"; 
              echo 'data-toggle="kt-tooltip"  data-html="true" data-placement="bottom" title="<strong>Shared With : </strong>'.$note->sperson.'<br><strong>Shared By : </strong>'.$note->ad_name.'"';
            } 
          ?> 
        >
          <small class="toptext">
            <?php if($note->updated_time!=''){ echo date('m/d/Y H:i:s',$note->updated_time);  }else{ echo date('m/d/Y H:i:s',$note->added_time); } ?>
          </small>
          <?php 
            if($note->sticky_heading != null){
              echo '<h4>'.$note->sticky_heading.'</h4>';
            }else{
              echo '<br />';
            } 
          ?>
          <p>
            <pre><?php $countw=strlen($note->sticky_desc); if($countw <= 100){ echo $note->sticky_desc; } else { $string = substr($note->sticky_desc, 0, 85); echo $string.'... <a class="viewlink" href="#" onclick="updatesticky("edit",'.$note->sticky_id.');">Click here to view</a>'; } ?></pre>
          </p>
          <?php if($this->session->userdata('id')==$note->added_by){ ?>
            <span class="delbtn" onclick="updatesticky('delete','<?php echo $note->sticky_id; ?>')" data-toggle="kt-tooltip" data-placement="top" data-html="true" title="Delete">
              <i class="fa fa-trash-alt"></i>
            </span>
          <?php } ?>
          <span class="share-btn" data-toggle="kt-tooltip" data-placement="top" data-html="true" title="Share" onclick="sharemodel('<?php echo $note->sticky_id; ?>','<?php echo $note->shared_person; ?>');">
            <i class="fa fa-share"></i>
          </span>
        </div>
      </li>
    <?php } ?>  
  </ul>
  <?php } else { ?>
    <div class="no-content text-center">
      <h1>Let's Start with Sticky</h1>
      <h5>Now Share with your teammate. :)</h5>
      <img src="<?php base_url(); ?>assets/img/sticky.png" width="150">
    </div>
  <?php } ?> 
</div>
<div class="modal fade" id="addnotemodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					<span id="modal_title">Add</span> Note
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" id="addnoteform" action="<?php echo base_url(); ?>sticky/addnote">
          <input type="hidden" name="stickyid" id="stickyid" />
          <div class="form-group">
            <label class="control-label">Heading</label>
            <input type="text" class="form-control" name="heading" id="heading"  />
          </div>
          <div class="form-group">
            <label class="control-label">Description<sup>*</sup></label>
            <textarea name="desc" id="desc" class="form-control" rows="5"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">     
        <button type="button" id="editbtn" class="btn btn-primary" onclick="$('#addnoteform').submit();">Save</button>   
        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
      </div> 
    </div>
  </div>
</div>
<div class="modal inmodal fade" id="delete_note" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Note</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url('sticky/delete_note'); ?>" id="delete_note_form">
          <input type="hidden" name="delete_note_id" id="delete_note_id">
          <div class="alert alert-warning">
            <strong>Warning :</strong>This action is affect in many other places and all data will be deleted after this action.
          </div>
          <h4 class="text-center">Are you sure you still want to delete this Note ?</h4>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger" onclick="$('#delete_note_form').submit();">Yes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="sharemodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Share Note</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" id="sharenoteform" action="<?php echo base_url(); ?>sticky/sharenote">
          <input type="hidden" name="sharestickyid" id="sharestickyid" />
          <div class="form-group">
            <div class="col-md-12">
              <select class="form-control" name="person[]" id="person" multiple="multiple">
                <?php foreach ($users as $user) { ?>  
                  <option value="<?php echo $user->id; ?>"><?php echo $user->name; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">     
        <button type="button" id="editbtn" class="btn btn-primary" onclick="document.getElementById('sharenoteform').submit();">Share</button>   
        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
      </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $("#addnoteform").validate ({
    rules: {
      desc:{
        required : true,
      }
    }, 
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      element.closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.error').remove();
    }
  });
  $("#person").select2({
    placeholder: "Select Person"
  });
});
function addnote(){
  $('#stickyid').val("");
  $('#desc').val("");
  $('#heading').val("");
  $('#modal_title').html('Add')
  $('#addnotemodel').modal('show');
}
function updatesticky(action,id){
  $.ajax({
    type:'POST',   
    url: "<?php echo base_url('sticky/getstickydetail'); ?>",
    data: {"stickyid":id},
    success: function(data){
      var json=jQuery.parseJSON(data);
      if(action=='edit'){
        $('#stickyid').val(json.notes.sticky_id);
        $('#heading').val(json.notes.sticky_heading);
        $("#desc").val(json.notes.sticky_desc);
        $('#modal_title').html('Edit');
        $('#addnotemodel').modal('show');
      }else if(action=='delete'){
        $('#delete_note_id').val(json.notes.sticky_id);
        $('#delete_note').modal('show');
      }  
    }
  });
}
function sharemodel(id,shared_person){
  var shared_person = shared_person.split(',');
  $('#person').val(shared_person);
  $('#person').trigger("change");
  $('#sharestickyid').val(id);
  $('#sharemodel').modal('show');
}
</script>