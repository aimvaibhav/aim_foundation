<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Holiday List</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					HR 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Holiday List 
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<?php 
	      $access=$this->access->can_access(); 
	      $holidays_access=array_search('holidays', array_column($access, 'param')); 
	      if($access[$holidays_access]['can_create']==1){
	    ?>
			<button type="button" class="btn btn-brand btn-icon-sm" onclick="add_holiday();">
				<i class="flaticon2-plus"></i> Add New
			</button>
			<?php } ?>
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<table class="table table-bordered table-striped" id="holiday_list">
				<thead>
					<th>Date</th>
					<th>Day</th>
					<th>Name</th>
					<th>Company</th>
					<th>Note</th>
					<?php if($access[$holidays_access]['can_edit']==1){ ?>
					<th>Action</th>
					<?php } ?>
				</thead>
				<tbody>
					<?php 
						foreach ($holidays as $holiday) { 
							$class="";
							if($holiday->date<date('Y-m-d')){
								$class="kt-font-danger kt-font-bolder";
							}else if($holiday->date<date('Y-m-d',strtotime('+20 Days'))){
								$class="kt-font-success kt-font-bolder";
							}
					?>
					<tr>
						<td class="<?php echo $class; ?>"><?php echo date('m/d/Y',strtotime($holiday->date)); ?></td>
						<td class="<?php echo $class; ?>"><?php echo date('l', strtotime($holiday->date)); ?></td>
						<td class="<?php echo $class; ?>"><?php echo $holiday->name; ?></td>
						<td class="<?php echo $class; ?>"><?php echo $holiday->company; ?></td>
						<td class="<?php echo $class; ?>"><?php echo $holiday->note; ?></td>
						<?php if($access[$holidays_access]['can_edit']==1){ ?>
							<td class="<?php echo $class; ?>">
								<button class="btn btn-sm btn-clean btn-icon btn-icon-md edit-action" title="Edit details" data-id="<?php echo $holiday->id; ?>">
									<i class="la la-edit"></i>
								</button>
							</td>
						<?php } ?>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="holiday_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					<span id="modal_title">Add</span> Holiday
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="holiday_form" method="post" action="<?php echo base_url('hr/save_holiday'); ?>">
					<input type="hidden" name="holiday_id" id="holiday_id">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" id="name" class="form-control">
					</div>
					<div class="form-group">
						<label>Date</label>
						<input type="text" name="date" id="date" class="form-control">
					</div>
					<div class="form-group">
						<label>Company</label>
						<select name="company" id="company" class="form-control">
							<option value="Aimtron Corporation">Aimtron Corporation</option>
							<option value="Aimtron Electronics Pvt. Ltd.">Aimtron Electronics Pvt. Ltd.</option>
							<option value="Aimtron Systems">Aimtron Systems</option>
						</select>
					</div>
					<div class="form-group">
						<label>Notes</label>
						<textarea name="note" id="note" class="form-control"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#holiday_form').submit();">Save</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#holiday_form').validate({
			rules: {
				name: {
					required: true
				},
				date: {
					required: true
				}
			}
		});
		var table = $('#holiday_list').DataTable({
			dom: `<'row'<'col-sm-6 text-left'f>>
				<'row'<'col-sm-12'tr>>
				<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			pageLength: 500,
			lengthMenu: [ 10, 25, 50, 75, 100, 200, 300, 400, 500 ],
			fixedHeader: {
	      header: true,
	      footer: true
	    },
	    responsive: true,
			bAutoWidth: false
		});
		$('#kt_aside_toggler').on('click',function(){
			setTimeout(function(){
				table.fixedHeader.adjust();
			}, 1000);
		});
		var arrows = {
	    leftArrow: '<i class="la la-angle-left"></i>',
	    rightArrow: '<i class="la la-angle-right"></i>'
		};
		$('#date').datepicker({
	    todayHighlight: true,
	    orientation: "bottom left",
	    autoclose: true,
	    templates: arrows
		});
	 	$('#holiday_list').on('click','.edit-action',function(){
			$.ajax({
        type:'POST',   
        url: "<?php echo base_url('hr/get_holiday_remote'); ?>",
        data: {"holiday_id":$(this).attr("data-id")},
        success: function(data){
          var json = jQuery.parseJSON(data);
          $('#name').val(json.holiday.name);
          $('#date').val(json.holiday.date);
          $('#company').val(json.holiday.company);
          $('#note').val(json.holiday.note);
          $('#holiday_id').val(json.holiday.id);
          $('#modal_title').html('Edit <b>"'+json.holiday.name+'"</b>');
          $('#holiday_model').modal('show');
        }
      });
		});
	});
	function add_holiday(){
		$('#name').val("");
    $('#date').prop('checked',true);
    $('#holiday_id').val("");
    $('#note').val("");
    $('#company').val("Aimtron Corporation");
    $('#modal_title').html("Add ");
    $("#supervisor").select2("val", " ");
    $('#holiday_model').modal('show');
	}
</script>