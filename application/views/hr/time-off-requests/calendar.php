<link href="<?php echo base_url(); ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
<style type="text/css">
  td.change:after {
    content: attr(data-content);
    font-size: 30px;
  }
  td.change{
    height: 100%;
    vertical-align: middle;
    text-align: center;
  }
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
  <div class="kt-container  kt-container--fluid ">
  	<div class="kt-subheader__main">
  		<h3 class="kt-subheader__title">Timeoff Calendar</h3>
  		<span class="kt-subheader__separator kt-hidden"></span>
  		<div class="kt-subheader__breadcrumbs">
  			<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
  			<span class="kt-subheader__breadcrumbs-separator"></span>
  			<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
  				HR
  			</a>
  			<span class="kt-subheader__breadcrumbs-separator"></span>
  			<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
  				Timeoff Calendar
  			</a>
  		</div>
  	</div>
  	<div class="kt-subheader__toolbar">
  	</div>
  </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div id="kt_calendar"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#kt_calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listWeek'
    },
    editable: false,
    eventLimit: true,
    navLinks: true,
    displayEventTime: true,
    timeFormat: "h:mm a",
    displayEventEnd:true,
    events: [
    	<?php 
        foreach ($requests as $request) {
          $comment = str_replace(array("\r\n", "\n"), "<br>", str_replace("'", " ", $request->comment));
      		echo "{ 
            title: '".$request->user_name."',
            start:'".$request->from_date."',
            description:'".str_replace("'", " ", $request->reason)." - ".str_replace("'", " ", $comment)."',
            end:'".$request->thru_date."',
            className:'fc-event-light fc-event-solid-primary', 
          },";
      	} 
      ?>
    ],
    eventRender: function(event, element) {
      if (element.hasClass('fc-day-grid-event')) {
        element.data('content', event.description);
        element.data('placement', 'top');
         KTApp.initPopover(element);
      } else if (element.hasClass('fc-time-grid-event')) {
        element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>'); 
      } else if (element.find('.fc-list-item-title').lenght !== 0) {
        element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>'); 
      }
    },
    dayRender: function (date, cell) {
      <?php foreach ($holidays as $holiday) { ?>
        var d = moment(date).format("YYYY-MM-DD");
        if (d=='<?php echo $holiday->date; ?>'){
          cell.css("background-color","#feb8d0");
          cell.addClass('change').attr('data-content','<?php echo str_replace("'", " ", $holiday->name); ?>');
        }
      <?php } ?>
    }
  });
});
</script>