<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">My time off requests</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					HR
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					My Time off requests
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-md-12 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" placeholder="Search Time Off Requests" id="generalSearch">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-datatable"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="cancel_request_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Cancel Timeoff Request
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="cancel_request_form" method="post" action="<?php echo base_url('hr/cancel_timeoff_request'); ?>">
					<input type="hidden" name="cancel_ticket_id" id="cancel_ticket_id">
					<div class="alert alert-warning" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">Are you sure you want to cancel your timeoff request ?</div>
        	</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#cancel_request_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var datatable = $('.kt-datatable').KTDatatable({
		data: {
			type: 'local',
			source: <?php echo json_encode($requests); ?>,
			pageSize: 10,
		},
		layout: {
			scroll: false,
			footer: false,
		},
		sortable: true,
		pagination: true,
		search: {
			input: $('#generalSearch'),
		},
		columns: [
			{
				field: 'id',
				title: '#',
				width: 20,
				type: 'number',
				textAlign: 'center',
			}, {
				field: 'date',
				title: 'Date',
			}, {
				field: 'from_date',
				title: 'From Date',
			}, {
				field: 'thru_date',
				title: 'Thru Date',
			}, {
				field: 'reason',
				title: 'Reason',
			}, {
				field: 'status',
				title: 'Status',
				template: function(row) {
					var status = {
						0: {'title': 'New', 'class': 'kt-badge--brand'},
						1: {'title': 'Approved By Supervisor', 'class': 'kt-badge--success'},
						2: {'title': 'Decline By Supervisor', 'class': 'kt-badge--danger'},
						3: {'title': 'Approved By Hr', 'class': 'kt-badge--success'},
						4: {'title': 'Decline By Hr', 'class': 'kt-badge--danger'},
						5: {'title': 'Canceled', 'class': 'kt-badge--info'},
					};
					return '<span class="kt-badge ' + status[row.status].class + ' kt-badge--inline kt-badge--pill">' + status[row.status].title + '</span>';
				},
			},
			{
				field: '',
				title: 'Actions',
				sortable: false,
				width: 110,
				overflow: 'visible',
				autoHide: false,
				template: function(row) {
					if(row.status!=5){
						return '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md cancel-action" title="Cancel" data-id="'+row.id+'"><i class="la la-times"></i></a>';
					}else{
						return;
					}	
				},
			}
		],
	});
	$(document).ready(function(){
		$('.cancel-action').click(function(){
			$('#cancel_ticket_id').val($(this).attr("data-id"));
      $('#cancel_request_model').modal('show');
		});
	});
</script>