<?php 
  $access=$this->access->can_access(); 
  $time_off_requests_list=array_search('time_off_requests_list', array_column($access, 'param')); 
?>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Time off request</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					HR
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Time off request
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<form method="post" action="<?php echo base_url('hr/save_time_off_request') ?>" id="tr_form">
				<input type="hidden" name="timeoff_id" id="timeoff_id">
				<div class="form-group">
					<label>Date</label>
					<input readonly type="text" class="form-control" name="date" id="tr_date" value="<?php echo date('m/d/Y'); ?>">
				</div>
				<?php if($access[$time_off_requests_list]['can_delete']==1){ ?>
					<div class="form-group">
						<label>Name</label>
						<select class="form-control" name="name" id="tr_name" onchange="select_department(this.value);">
							<?php foreach ($users as $user) { ?>
								<option value="<?php echo $user->id; ?>" <?php if($this->session->userdata('id')==$user->id){ echo 'selected'; } ?>><?php echo $user->name; ?></option>
							<?php } ?>
						</select>
					</div>
				<?php }else{ ?>
					<div class="form-group">
						<label>Name</label>
						<input readonly type="text" class="form-control" name="name" id="tr_name" value="<?php echo $this->session->userdata('name'); ?>">
					</div>
				<?php } ?>
				<div class="form-group">
					<label>Department</label>
					<input readonly type="text" class="form-control" name="department" id="tr_department" value="<?php echo $this->session->userdata('department_name') ?>">
					<input readonly type="hidden" class="form-control" name="department_id" id="tr_department_id" value="<?php echo $this->session->userdata('department_id') ?>">
				</div>
				<div class="form-group">
					<label>From Date &amp; Time</label>
					<input type="text" class="form-control" name="from_date" id="tr_from_date" value="<?php echo date('F d Y h:i A'); ?>">
				</div>
				<div class="form-group">
					<label>Thru Date &amp; Time</label>
					<input readonly type="text" class="form-control" name="thru_date" id="tr_thru_date">
				</div>
				<div class="form-group">
					<label>Reason</label>
					<select class="form-control" name="reason" id="tr_reason">
						<option value="">Select Reason</option>
						<option value="Vacation">Vacation</option>
						<option value="Personal">Personal</option>
						<option value="Sick">Sick</option>
						<option value="Jury Duty">Jury Duty</option>
						<option value="Bereavement">Bereavement</option>
						<option value="Other">Other</option>
					</select>
				</div>
				<div class="form-group">
					<label>Comment</label>
					<textarea class="form-control" name="comment" id="tr_comment"></textarea>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#tr_form').validate({
		rules: {
			from_date: {
				required: true,
			},
			thru_date: {
				required: true,
			},
			reason:{
				required:true,
			},
			comment:{
				required:true,
			}
		}
	});
	$('#tr_from_date,#tr_thru_date').datetimepicker({
    format: "MM dd yyyy HH:ii P",
    showMeridian: true,
    todayHighlight: true,
    autoclose: true
  });
});
function select_department(user_id){
	$.ajax({
    type:'POST',   
    url: "<?php echo base_url('hr/get_user_department'); ?>",
    data: {"user_id":user_id},
    success: function(data){
      var json = jQuery.parseJSON(data);
      $('#tr_department').val(json.department.name);
      $('#tr_department_id').val(json.department.id);
    }
  });
}
</script>