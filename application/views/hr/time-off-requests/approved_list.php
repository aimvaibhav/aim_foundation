<?php 
  $access=$this->access->can_access(); 
  $time_off_requests_list=array_search('time_off_requests_list', array_column($access, 'param')); 
?>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Time off request Approved list</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					HR
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Time off request Approved list
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div class="row">
				<div class="col-md-12">
		          <form method="post" action="<?php echo base_url('hr/set_approved_list_date_filter'); ?>">
		            <div class="form-group row">
		              <div class="col-2 col-form-label text-right">From Date</div>
		              <div class="col-3">
		                <input type="text" name="approved_list_from_date_filter" id="approved_list_from_date_filter" class="form-control" value="<?php 
		                  if($this->session->userdata('approved_list_from_date')!=''){ 
		                    echo $this->session->userdata('approved_list_from_date'); 
		                  }else{
		                    echo date('m/d/Y',strtotime('-1 Year'));
		                  }
		                ?>">
		              </div>
		              <div class="col-2 col-form-label text-right">To Date</div>
		              <div class="col-3">
		                <input type="text" name="approved_list_to_date_filter" id="approved_list_to_date_filter" class="form-control" value="<?php 
		                  if($this->session->userdata('approved_list_to_date')!=''){ 
		                    echo $this->session->userdata('approved_list_to_date'); 
		                  }else{
		                    echo date('m/d/Y');
		                  }
		                ?>">
		              </div>
		              <div class="col-2">
		                <button class="btn btn-info btn-block" type="submit">Search</button>
		              </div>
		            </div>
		          </form>
		        </div>
			</div>
			<table class="table table-bordered table-striped" id="ajax_data">
				<thead>
					<th>#</th>
					<th>Date</th>
					<th>Name</th>
					<th>Department</th>
					<th>From Date</th>
					<th>Thru Date</th>
					<th>Reason</th>
					<th>Status</th>
					<th>Actions</th>
				</thead>
				<tbody>
					<?php foreach ($requests as $req) { ?>
						<tr>
							<td><?php echo $req->id; ?></td>
							<td><?php echo $req->date; ?></td>
							<td><?php echo $req->user_name; ?></td>
							<td><?php echo $req->department_name; ?></td>
							<td><?php echo $req->from_date; ?></td>
							<td><?php echo $req->thru_date; ?></td>
							<td><?php echo $req->reason; ?></td>
							<td>
								<?php if($req->status==0){ ?>
									<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">New</span>
								<?php }else if($req->status==1){ ?>
									<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Approved By Supervisor</span>
								<?php }else if($req->status==2){ ?>
									<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Decline By Supervisor</span>
								<?php }else if($req->status==3){ ?>
									<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Approved By Hr</span>
								<?php }else if($req->status==4){ ?>
									<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Decline By Hr</span>
								<?php }else if($req->status==5){ ?>
									<span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill">Canceled</span>
								<?php } ?>
							</td>
							<td>
								<?php if($access[$time_off_requests_list]['can_edit']==1){ ?>
									<button class="btn btn-sm btn-clean btn-icon btn-icon-md edit-action" title="Edit details" data-id="<?php echo $req->id; ?>">
										<i class="la la-edit"></i>
									</button>
								<?php } ?>
								<?php if($access[$time_off_requests_list]['can_delete']==1){ ?>
									<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md delete-action" title="Delete" data-id="<?php echo $req->id; ?>">
										<i class="la la-trash"></i>
									</a>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="timeoff_edit_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Edit Timeoff Request
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="timeoff_edit_form" method="post" action="<?php echo base_url('hr/save_time_off_request'); ?>">
					<input type="hidden" name="timeoff_id" id="timeoff_id">
					<div class="form-group">
						<label>Date</label>
						<input type="text" name="date" id="timeoff_date" class="form-control" disabled>
					</div>
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" id="timeoff_name" class="form-control" disabled>
					</div>
					<div class="form-group">
						<label>Department</label>
						<input type="text" name="department" id="timeoff_department" class="form-control" disabled>
					</div>
					<div class="form-group">
						<label>From Date & Time</label>
						<input type="text" name="from_date" id="timeoff_from_date" class="form-control">
					</div>
					<div class="form-group">
						<label>Thru Date & Time</label>
						<input type="text" name="thru_date" id="timeoff_thru_date" class="form-control">
					</div>
					<div class="form-group">
						<label>Reason</label>
						<select name="reason" id="timeoff_reason" class="form-control">
							<option value="">Select Reason</option>
							<option value="Vacation">Vacation</option>
							<option value="Personal">Personal</option>
							<option value="Sick">Sick</option>
							<option value="Jury Duty">Jury Duty</option>
							<option value="Bereavement">Bereavement</option>
							<option value="Other">Other</option>
						</select>
					</div>
					<div class="form-group">
						<label>Comment</label>
						<textarea class="form-control" name="comment" id="timeoff_comment"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#timeoff_edit_form').submit();">Save</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_timeoff_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Delete Timeoff Request
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="delete_timeoff_form" method="post" action="<?php echo base_url('hr/delete_timeoff_request'); ?>">
					<input type="hidden" name="delete_timeoff_id" id="delete_timeoff_id">
					<div class="alert alert-warning" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">This action is affect in many other places and all data will be deleted after this action.</div>
        	</div>
          <h4 class="text-center">Are you sure you still want to delete this timeoff request ?</h4>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#delete_timeoff_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	<?php if($access[$time_off_requests_list]['can_export']==1){ ?>
	    var export_button = `<'col-sm-6 text-right'B>`;
	<?php }else{ ?>
	    var export_button = '';
	<?php } ?>
	$('#ajax_data').DataTable({
		dom: `<'row'<'col-sm-6 text-left'f>`+export_button+`>
				<'row'<'col-sm-12'tr>>
				<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
		pageLength: 100,
		order: [[ 1, "desc" ]],
		buttons:["print","copyHtml5","excelHtml5","csvHtml5"],
	    fixedHeader: {
	      	header: true,
	      	footer: true
	    },
	    responsive: true
	});
	var arrows = {
	    leftArrow: '<i class="la la-angle-left"></i>',
	    rightArrow: '<i class="la la-angle-right"></i>'
	};
	$("#approved_list_from_date_filter,#approved_list_to_date_filter").datepicker({
	    todayHighlight: true,
	    orientation: "bottom left",
	    autoclose: true,
	    templates: arrows
	});
	$('#ajax_data').on('click','.edit-action',function(){
		$.ajax({
		    type:'POST',   
		    url: "<?php echo base_url('hr/get_timeoff_request_detail'); ?>",
		    data: {"timeoff_id":$(this).attr("data-id")},
		    success: function(data){
		      var json = jQuery.parseJSON(data);
		      $('#timeoff_id').val(json.request.id);
		      $('#timeoff_date').val(json.request.date);
		      $('#timeoff_name').val(json.request.user_name);
		      $('#timeoff_department').val(json.request.department_name);
		      $('#timeoff_from_date').val(json.request.from_date);
		      $('#timeoff_thru_date').val(json.request.thru_date);
		      $('#timeoff_reason').val(json.request.reason);
		      $('#timeoff_comment').val(json.request.comment);
		      $('#timeoff_edit_model').modal('show');
		    }
		});
	});
	$('#ajax_data').on('click','.delete-action',function(){
		$('#delete_timeoff_id').val($(this).attr("data-id"));
  		$('#delete_timeoff_model').modal('show');
	});
	$('#timeoff_edit_form').validate({
		rules: {
			from_date: {
				required: true,
			},
			thru_date: {
				required: true,
			},
			reason:{
				required:true,
			},
			comment:{
				required:true,
			}
		}
	});
	$('#timeoff_from_date,#timeoff_thru_date').datetimepicker({
	 	format: "MM dd yyyy HH:ii:ss P",
	    showMeridian: true,
	    todayHighlight: true,
	    autoclose: true
	});
});
</script>