<div class="kt-subheader   kt-grid__item" id="kt_subheader">
  <div class="kt-container  kt-container--fluid ">
  	<div class="kt-subheader__main">
  		<h3 class="kt-subheader__title">Timeoff Request Approval For <?php echo $time_off["user_name"]; ?></h3>
  		<span class="kt-subheader__separator kt-hidden"></span>
  		<div class="kt-subheader__breadcrumbs">
  			<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
  			<span class="kt-subheader__breadcrumbs-separator"></span>
  			<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
  				HR
  			</a>
  			<span class="kt-subheader__breadcrumbs-separator"></span>
  			<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
  				Timeoff Request Approval For <?php echo $time_off["user_name"]; ?>
  			</a>
  		</div>
  	</div>
  	<div class="kt-subheader__toolbar">
  	</div>
  </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<table class="table table-bordered table-striped">
        <thead class="bg-success">
          <th>Employee Name</th>
          <th>Department</th>
          <th>From Date</th>
          <th>Thru Date</th>
          <th>Reason</th>
          <th>Comment</th>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $time_off["user_name"]; ?></td>
            <td><?php echo $time_off["department_name"]; ?></td>
            <td><?php echo date('m/d/Y H:i:s',strtotime($time_off["from_date"])); ?></td>
            <td><?php echo date('m/d/Y H:i:s',strtotime($time_off["thru_date"])); ?></td>
            <td><?php echo $time_off["reason"]; ?></td>
            <td><?php echo $time_off["comment"]; ?></td>
          </tr>
        </tbody>
      </table>
      <form method="post" action="<?php echo base_url('hr/update_time_off_request_approval') ?>" id="">
        <input type="hidden" name="timeoff_id" id="timeoff_id" value="<?php echo $time_off['id']; ?>">
        <?php if($person=='supervisor'){ ?>
          <button class="btn btn-primary" type="submit" name="status" value="1">Approve</button>
          <button class="btn btn-danger" type="button" name="status" value="2" onclick="decline_timeoff(this.value,'<?php echo $time_off['id']; ?>');">Decline</button>
        <?php }else if($person=='hr'){ ?>
          <button class="btn btn-primary" type="submit" name="status" value="3">Approve</button>
          <button class="btn btn-danger" type="button" name="status" value="4" onclick="decline_timeoff(this.value,'<?php echo $time_off['id']; ?>');">Decline</button>
        <?php } ?>
      </form>
		</div>
	</div>
</div>
<div class="modal fade" id="decline_timeoff_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Decline Timeoff
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="decline_timeoff_form" method="post" action="<?php echo base_url('hr/decline_time_off_request_approval'); ?>">
					<input type="hidden" name="status" id="status">
          <input type="hidden" name="timeoff_id_modal" id="timeoff_id_modal">
					<div class="form-group">
            <label>Reason<sup>*</sup></label>
            <textarea name="decline_reason" id="decline_reason" class="form-control"></textarea>
        	</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#decline_timeoff_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#decline_timeoff_form").validate ({
        rules: {
            decline_reason:{
                required : true,
            }
        },
        highlight: function(element) {
          $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
          element.closest('.form-group').removeClass('has-error').addClass('has-success');
          $(element).closest('.error').remove();
        }
    });
});
function decline_timeoff(status,timeoff_id){
    $('#status').val(status);
    $('#timeoff_id_modal').val(timeoff_id);
    $('#decline_timeoff_modal').modal({backdrop: 'static'});
}
</script>