<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Dashboard</h3>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
</div>
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid" id="kt_content">

</div>
<script src="<?php echo base_url('assets/vendors/custom/charts/fusioncharts/js/fusioncharts.js'); ?>"></script>   
<script src="<?php echo base_url('assets/vendors/custom/charts/fusioncharts/js/fusioncharts.charts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/demo1/highcharts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/demo1/highcharts-3d.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#eco_table').DataTable();
	$('#deviation_table').DataTable();
	var arrows = {
	    leftArrow: '<i class="la la-angle-left"></i>',
	    rightArrow: '<i class="la la-angle-right"></i>'
	};
	$("#chart_eco_from_date,#chart_otd_from_date,#chart_fpty_from_date,#chart_quote_from_date").datepicker({
	    todayHighlight: true,
	    orientation: "bottom left",
	    autoclose: true,
	    templates: arrows
	});
});
Morris.Line({
	element: 'eco_chart',
	data: [
		<?php 
			foreach($permonthECO as $key) { 
				echo "{y:'".date('Y-m',strtotime($key->month))."', a:".$key->ecerr.", b:".$key->ec."},"; 
			} 
		?>
	],
	xkey: 'y',
	ykeys: ['a', 'b'],
	labels: ['Internal Error', 'Total ECOs'],
	lineColors: ['#ED8739','#1e88e5'],
	lineWidth: '3px',
	resize: true,
	redraw: true
});
FusionCharts.ready(function () {
  var quotesChart = new FusionCharts({
    type: 'mscombidy2d',
    renderAt: 'quotes_chart',
    width: '100%',
    height: '350',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "caption": "",
        "subCaption": "",
        "xAxisname": "Month",
        "pYAxisName": "Quotes",
        "sYAxisName": "We Won",
        "numberPrefix": "",
        "sNumberSuffix" : "",
        "sYAxisMaxValue" : "100",
        "pYAxisMaxValue" : "100",
        //Cosmetics
        "paletteColors" : "#2f4050,#9f8a69,#9f8a69",
        "baseFontColor" : "#333333",
        "baseFont" : "Helvetica Neue,Arial",
        "captionFontSize" : "14",
        "subcaptionFontSize" : "14",
        "subcaptionFontBold" : "0",
        "showBorder" : "0",
        "bgColor" : "#ffffff",
        "showShadow" : "0",
        "canvasBgColor" : "#ffffff",
        "canvasBorderAlpha" : "0",
        "divlineAlpha" : "100",
        "divlineColor" : "#999999",
        "divlineThickness" : "1",
        "divLineIsDashed" : "1",
        "divLineDashLen" : "1",
        "divLineGapLen" : "1",
        "usePlotGradientColor" : "0",
        "showplotborder" : "0",
        "showXAxisLine" : "1",
        "xAxisLineThickness" : "1",
        "xAxisLineColor" : "#999999",
        "showAlternateHGridColor" : "0",
        "showAlternateVGridColor" : "0",
        "legendBgAlpha" : "0",
        "legendBorderAlpha" : "0",
        "legendShadow" : "0",
        "legendItemFontSize" : "10",
        "legendItemFontColor" : "#666666"
      },
      "categories": [
      	{
          "category": [
          	<?php foreach ($graphval as $key) { echo "{\"label\" : "."\"".$key->month."\"},"; } ?>
          ]
      	}
      ],
      "dataset": [
        {
          "seriesName": "Quotes",
          "data": [<?php foreach ($graphval as $key) { echo "{\"value\" : "."\"".$key->quotes."\"},"; } ?>]
        }, 
        {
          "seriesName": "We Won",
          "renderAs": "area",
          "showValues": "0",
          "data": [<?php foreach ($graphval as $key) { echo "{\"value\" : "."\"".$key->wewin."\"},"; } ?> ]
        },
        {
            "seriesname": "Ratio",
            "parentyaxis": "S",
            "color": "FF0000",
            "renderas": "Line",
            "linethickness": "4",
            "data":[ 
              <?php 
                foreach ($graphval as $key) { 
                  echo "{\"value\" : "."\"".(($key->wewin*100)/$key->quotes)."\"},"; 
                } 
              ?>  
            ]
          }
      ]
    }
  });
  quotesChart.render();
});
FusionCharts.ready(function () {
  var rmaAndShipChart = new FusionCharts({
    type: 'mscolumn3dlinedy',
    renderAt: 'rma_and_ship_chart',
    width: '100%',
    height:'670',
    dataFormat: 'json',
    dataSource: {
      "chart": {
        "baseFontSize":"14",
        "baseFontColor":"#333",
        "toolTipColor": "#ffffff",
        "toolTipBorderThickness": "0",
        "toolTipBgColor": "#000000",
        "toolTipBgAlpha": "80",
        "toolTipBorderRadius": "2",
        "toolTipPadding": "5",
        "seriesnameintooltip": "0",
        "showborder": "0",
        "showvalues": "1",
        "placeValuesInside":"1",
        "formatNumber":'0',
        "formatNumberScale":'0',
        "showYAxisValues":"1",
        "formatNumber":'0',
        "formatNumberScale":'0',
        "pYAxisName":"RMA/Shipping",
        "sYAxisName":"Ratio",
        "SYAxisValuesStep":'2',
      },
      "categories": [
        {
          "category": [ <?php foreach ($alerts as $key) { echo '{ label: "'.$key['month'].'" },'; } ?> ] 
        }
      ],
      "dataset": [
       	{
          "seriesname": "Shipping Quantity",
          "showvalues": "1",
          "data": [
            <?php foreach ($alerts as $key) {echo '{ value: "'.$key['total_shipment'].'" },'; } ?>
          ]
    		},
        {
          "seriesname": "Return Quantity",
          "color": "FC8620",
          "showvalues": "1",
          "data": [
             <?php foreach ($alerts as $key) { echo '{ value: "'.$key['total_return'].'" },'; } ?>
          ]
        },
        {
          	"seriesname": "Ratio",
          	"parentyaxis": "S",
          	"color": "FF0000",
          	"renderas": "Line",
          	"linethickness": "4",
          	"data":[ 
          		<?php 
          			foreach ($alerts as $key) { 
          				if($key['total_shipment']!=0){
         				 	$flt = ($key['total_return']/$key['total_shipment'])*100;
          				} else {
            				$flt = 0;
          				}
          				echo '{ "value": "'.sprintf("%.2f",$flt).'"},';
        			}
        		?>  
        	]
        }
      ],
    }
  });
  rmaAndShipChart.render();
});
Highcharts.chart('first_pass_test_yield', {
  	colors: [
  		'#73a39c', 
  		'#ccabab', 
  		'#8d4654', 
  		'#7798BF', 
  		'#aaeeee',
  		'#ff0066', 
  		'#eeaaee', 
  		'#55BF3B', 
  		'#DF5353', 
  		'#7798BF', 
  		'#aaeeee'
  	],
    chart: {
      type: 'column',
      options3d: {
        enabled: false,
        alpha: 5,
        beta: 20,
        depth: 70
      }
    },
    plotOptions: {
      column: {
        depth: 25
      }
    },
    xAxis: {
      	categories: [
	      	<?php 
	      		foreach($gvalfpty as $key){
	      			if($key->y1>0){
	      				echo "'".$key->m1."',";
	      			}
	      		} 
	      	?>
     	],
	    labels: {
	        skew3d: true,
	        style: {
	          fontSize: '16px'
	        }
	    }
    },
    yAxis: {
    	plotLines:[{
		  	value: 95,
		  	width: 2,
		  	dashStyle: 'shortdash',
		    color: 'red',
		    label: {
	        	text: 'Goal'
     		}
		}],
      	title: {
        	text: "Yield"
      	}
    },
    series: [{
        name: 'First Pass Test Yield ( % )',
        data: [
        	<?php 
        		foreach($gvalfpty as $key){
        			if($key->y1>0){
        				echo $key->y1.",";
        			}
        		} 
        	?>
        ]
    }]
});
Highcharts.chart('on_time_delivery_chart', {
  	colors: [
  		'#FB6A6A', 
  		'#ccabab', 
  		'#8d4654', 
  		'#7798BF', 
  		'#aaeeee',
  		'#ff0066', 
  		'#eeaaee', 
  		'#55BF3B', 
  		'#DF5353', 
  		'#7798BF', 
  		'#aaeeee'
  	],
    chart: {
        type: 'column',
        options3d: {
            enabled: false,
            alpha: 5,
            beta: 20,
            depth: 70
        }
    },
    plotOptions: {
        column: {
            depth: 25
        }
    },
    xAxis: {
        categories: [
        <?php 
			foreach($gvalontimedel as $key) { 
				echo "'".date('M-Y',strtotime($key->month))."',"; 
			} 
		?>],
        labels: {
            skew3d: true,
            style: {
                fontSize: '16px'
            }
        }
    },
    yAxis: {
    	plotLines:[{
		  value: 95,
		  	width: 2,
		  	dashStyle: 'shortdash',
		    color: 'red',
		    label: {
                text: 'Goal'
            }
		}],
        title: {
            text: "%"
        }
    },
    series: [{
        name: 'Total Ship ( % )',
        data: [98.5,92.7,95.5,90,93.4,89.8,96,93.4,95.6,92.8,91.9,90.5,92.5]  
    }]
});

// <?php 
// 	foreach($gvalontimedel as $key){ 
// 		if($key->totalship>0){
// 			echo $key->totalship.",";
// 		} 
// 	}  
// ?>

function validate_asm(){
	var pr = $('#homeasmval').closest('form');
	$('.error', pr).remove();
	if($('#homeasmval').val() == '') {
		$('#homeasmval').closest('form').append('<span class="error">This field is required</span>');
		return false;
	}
	return true;
}
function validate_part(){
	var pr = $('#homepartval').closest('form');
	$('.error', pr).remove();
	if($('#homepartval').val() == '') {
		$('#homepartval').closest('form').append('<span class="error">This field is required</span>');
		return false;
	}
	return true;
}
function validate_job(){
	var pr = $('#job').closest('form');
	$('.error', pr).remove();
	if($('#job').val() == '') {
		$('#job').closest('form').append('<span class="error">This field is required</span>');
		return false;
	}
	return true;
}
function validate_where(){
	var pr = $('#homewhereval').closest('form');
	$('.error', pr).remove();
	if($('#homewhereval').val() == '') {
		$('#homewhereval').closest('form').append('<span class="error">This field is required</span>');
		return false;
	}
	return true;
}
</script>