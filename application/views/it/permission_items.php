<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Permission Items</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					IT 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="#" class="kt-subheader__breadcrumbs-link">
					Permission Items
				</a>
			</div>
		</div>
	</div>
</div>
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<table class="table table-bordered">
				<thead class="table-active">
					<th>Permissions</th>
					<th class="text-center">View</th>
					<th class="text-center">Create</th>
					<th class="text-center">Edit</th>
					<th class="text-center">Delete</th>
					<th class="text-center">Action</th>
				</thead>
				<tbody>
					<?php
					 foreach ($permission_items as $key => $permission) {
					 	if(isset($user_permissions[$key])){ 
              $temp = str_split ($user_permissions[$key]);
	          }else{ 
              $temp = array(); 
	          } 
	        ?>
					<tr>
						<td><?php echo $permission->name; ?></td>
						<td class="text-center">
							<button onclick="add_permission_access('<?php echo $permission->id; ?>','<?php echo $permission->name; ?>','<?php echo $permission->item_view; ?>','item_view');" class="btn btn-clear btn-icon-sm <?php if($permission->item_view == 0){ echo 'btn-outline-danger'; }else{ echo 'btn-outline-success'; } ?>" data-toggle="kt-tooltip" data-html="true"
							  <?php
									if (!empty(json_decode($permission->item,true))) {
										?>title="<?php
											foreach (json_decode($permission->item,true) as $json_data) {
												echo "<ul class='list-unstyled'>";
												if($json_data['access'] == 'View'){
													echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
												}
												echo "</ul>";
											}
										?>"<?php
									}
								?>
							  ><?php echo $permission->item_view == 0 ? 'Disable':'Enable'; ?>
							</button>
						</td>
						<td class="text-center">
							<button onclick="add_permission_access('<?php echo $permission->id; ?>','<?php echo $permission->name; ?>','<?php echo $permission->item_create; ?>','item_create');" class="btn btn-clear btn-icon-sm <?php if($permission->item_create == 0){ echo 'btn-outline-danger'; }else{ echo 'btn-outline-success'; } ?>" data-toggle="kt-tooltip" data-html="true"
								<?php 
									if (!empty(json_decode($permission->item,true))) {
										?>title="<?php
											foreach (json_decode($permission->item,true) as $json_data) {
												echo "<ul class='list-unstyled'>";
												if($json_data['access'] == 'Create'){
													echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
												}
												echo "</ul>";
											}
										?>"<?php
									}
								?>
								><?php echo $permission->item_create == 0 ? 'Disable':'Enable'; ?>
							</button>
						</td>
						<td class="text-center">
							<button onclick="add_permission_access('<?php echo $permission->id; ?>','<?php echo $permission->name; ?>','<?php echo $permission->item_edit; ?>','item_edit');" class="btn btn-clear btn-icon-sm <?php if($permission->item_edit == 0){ echo 'btn-outline-danger'; }else{ echo 'btn-outline-success'; } ?>" data-toggle="kt-tooltip" data-html="true"
								<?php 
									if (!empty(json_decode($permission->item,true))) {
										?>title="<?php
											foreach (json_decode($permission->item,true) as $json_data) {
												echo "<ul class='list-unstyled'>";
												if($json_data['access'] == 'Edit'){
													echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
												}
												echo "</ul>";
											}
										?>"<?php
									}
								?>
								><?php echo $permission->item_edit == 0 ? 'Disable':'Enable'; ?>
							</button>
						</td>
						<td class="text-center">
							<button onclick="add_permission_access('<?php echo $permission->id; ?>','<?php echo $permission->name; ?>','<?php echo $permission->item_delete; ?>','item_delete');" class="btn btn-clear btn-icon-sm <?php if($permission->item_delete == 0){ echo 'btn-outline-danger'; }else{ echo 'btn-outline-success'; } ?>" data-toggle="kt-tooltip" data-html="true"
								<?php 
									if (!empty(json_decode($permission->item,true))) {
										?>title="<?php
											foreach (json_decode($permission->item,true) as $json_data) {
												echo "<ul class='list-unstyled'>";
												if($json_data['access'] == 'Delete'){
													echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
												}
												echo "</ul>";
											}
										?>"<?php
									}
								?>
								><?php echo $permission->item_delete == 0 ? 'Disable':'Enable'; ?>
							</button>
						</td>
						<td class="text-center">
							<button onclick="add_permission_items('<?php echo $permission->id; ?>');" class="btn btn-clear btn-icon-sm btn-outline-brand btn-icon" title="Add Items">
								<i class="la la-plus"></i>
							</button>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="permission_items_modal" tabindex="-1" role="dialog" aria-labelledby="permissionItemModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="permissionItemModalLabel">
					<span id="permission_modal_title">Permission For </span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="permission_item_form" method="post" action="<?php echo base_url('it/save_permission_items'); ?>">
					<input type="hidden" name="permission_items_id" id="permission_items_id">
					<div>
						<table class="table table-striped">
							<thead class="thead-dark">
								<tr>
									<th>Access</th>
									<th>Items</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="permission_item_rows"></tbody>
						</table>
					</div>
					<div class="form-group">
						<label>Permission</label>
						<select name="permission" id="permission" class="form-control">
							<option value="View">View</option>
							<option value="Create">Create</option>
							<option value="Edit">Edit</option>
							<option value="Delete">Delete</option>
						</select>
					</div>
					<div class="form-group">
						<label>Items</label>
						<input type="text" name="permission_item" id="permission_item" class="form-control">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#permission_item_form').submit();">Save</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="permission_accesss_modal" tabindex="-1" role="dialog" aria-labelledby="permissionAccessModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="permissionAccessModalLabel">
					<span id="modal_access_title">Permission For </span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="permission_access_form" method="post" action="<?php echo base_url('it/save_permission_access'); ?>">
					<input type="hidden" name="permission_access_id" id="permission_access_id">
					<input type="hidden" name="permission_access" id="permission_access">
					<input type="hidden" name="permission_access_status" id="permission_access_status">
					<div class="alert alert-warning" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">This action is affect in many other places and all data will be deleted after this action.</div>
        	</div>
          <h5 class="text-center">Are you sure you still want to update this access ?</h5>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#permission_access_form').submit();">Save</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function add_permission_items($id){
		$.ajax({
	    type:'POST',
	    url: "<?php echo base_url('it/get_permission_items_remote'); ?>",
	    data: {"permission_remote_id":$id},
	    success: function(data){
	      var json = jQuery.parseJSON(data);
		    $('#permission_items_id').val(json.permission_item.id);
		    $('#permission_item_rows').html(json.html);
		    $('#permission_modal_title').html('Permission For <b>"'+json.permission_item.param+'"</b>');
		    $('#permission_items_modal').modal('show');
	    }
	  });
	}
	function add_permission_access($id,$param,$status,$access){
	    $('#permission_access_id').val($id);
	    $('#permission_access').val($access);
	    $('#permission_access_status').val($status);
	    $('#modal_access_title').html('Permission For <b>"'+$param+'"</b>');
	    $('#permission_accesss_modal').modal('show');
	}
</script>