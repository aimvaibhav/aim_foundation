<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Basic Detail</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Settings 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Basic Detail 
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<form method="post" id="settings_form" action="<?php echo base_url('settings/save'); ?>" enctype="multipart/form-data">
				<div class="form-group">
					<label>Company Name</label>
					<input type="text" name="company_name" id="company_name" class="form-control" value="<?php echo get_site_setting('company_name'); ?>">
				</div>
				<div class="form-group">
					<label>Date Format</label>
					<input type="text" name="date_time_format" id="date_time_format" class="form-control" value="<?php echo get_site_setting('date_time_format'); ?>">
				</div>
				<div class="form-group">
					<label>Welcome Line</label>
					<input type="text" name="welcome_line" id="welcome_line" class="form-control" value="<?php echo get_site_setting('welcome_line'); ?>">
				</div>
				<div class="form-group">
					<label>Tagline</label>
					<input type="text" name="tagline" id="tagline" class="form-control" value="<?php echo get_site_setting('tagline'); ?>">
				</div>
				<div class="form-group">
					<label>Copyright Text</label>
					<input type="text" name="copyright_text" id="copyright_text" class="form-control" value="<?php echo get_site_setting('copyright_text'); ?>">
				</div>
				<div class="form-group">
					<label>Privacy Policy</label>
					<div>
						<input type="file" name="privacy_policy" id="privacy_policy">
					</div>
					<?php if(get_site_setting('privacy_policy')!=''){ ?>
						<a href="<?php echo base_url('documents/site_settings/').get_site_setting('privacy_policy'); ?>" class="float-right font-size-20" target="_blank"><i class="fa fa-file"></i></a>
					<?php } ?>
				</div>
				<div class="form-group">
					<label>Terms and Conditions</label>
					<div>
						<input type="file" name="company_terms" id="company_terms">
					</div>
					<?php if(get_site_setting('company_terms')!=''){ ?>
						<a href="<?php echo base_url('documents/site_settings/').get_site_setting('company_terms'); ?>" class="float-right font-size-20" target="_blank"><i class="fa fa-file"></i></a>
					<?php } ?>
				</div>
				<div class="form-group">
					<label>Company Logo</label>
					<div>
						<input type="file" name="company_logo" id="cmpany_logo">
					</div>
					<?php if(get_site_setting('company_logo')!=''){ ?>
						<img src="<?php echo base_url('documents/site_settings/').get_site_setting('company_logo'); ?>" class="float-right img-thumbnail" width="50" height="50" />
					<?php } ?>
				</div>
				<div class="form-group">
					<label>Favicon</label>
					<div>
						<input type="file" name="company_favicon" id="company_favicon">
					</div>
					<?php if(get_site_setting('company_favicon')!=''){ ?>
						<img src="<?php echo base_url('documents/site_settings/').get_site_setting('company_favicon'); ?>" class="float-right img-thumbnail" width="50" height="50" />
					<?php } ?>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success">Save Changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
 	$("#settings_form").validate ({
    rules: {
      company_name:{
        required : true,
      },
      welcome_line:{
      	required:true,
      },
      tagline:{
      	required:true,
      },
      copyright_text:{
      	required:true,
      },
      date_time_formate:{
      	required:true,
      }
    }, 
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      element.closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.error').remove();
    }
	});
});
</script>