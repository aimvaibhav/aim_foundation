<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Email Templates</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Settings 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Email Templates
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<button type="button" class="btn btn-brand btn-icon-sm" onclick="add_email_templaate();">
				<i class="flaticon2-plus"></i> Add New
			</button>
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-md-12 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" placeholder="Search Email Template" id="generalSearch">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-datatable" id="ajax_data"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="et_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					<span id="modal_title">Add</span> Email Template
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="et_form" method="post" action="<?php echo base_url('settings/save_email_template'); ?>">
					<input type="hidden" name="et_id" id="et_id">
					<div class="form-group">
						<label class="kt-checkbox">
							<input type="checkbox" name="status" id="et_status" value="1" checked> Active
							<span></span>
						</label>
					</div>
					<div class="form-group">
						<label>Title</label>
						<input type="text" name="title" id="et_title" class="form-control">
					</div>
					<div class="alert alert-solid-brand alert-bold">
						<div class="alert-text text-uppercase line-height-30">
							{{USER_NAME}} &nbsp; {{USER_EMAIL}} &nbsp; {{SUPERVISOR_NAME}} &nbsp; {{FROM_DATE}} &nbsp; {{THRU_DATE}} &nbsp; {{REASON}} &nbsp; {{COMMENT}} &nbsp; {{DEPARTMENT}}  &nbsp; {{DECLINE_REASON}} &nbsp; {{APPROVAL_LINK}}
						</div>
					</div>
					<div class="form-group">
						<label>Subject</label>
						<input type="text" name="subject" id="et_subject" class="form-control">
					</div>
					<div class="form-group">
						<label>Message</label>
						<textarea name="message" id="et_message" class="form-control summernote"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#et_form').submit();">Save</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
	data: {
		type: 'local',
		source: <?php echo json_encode($templates); ?>,
		pageSize: 10,
	},
	layout: {
		scroll: false,
		footer: false,
	},
	sortable: true,
	pagination: true,
	search: {
		input: $('#generalSearch'),
	},
	columns: [
		{
			field: 'id',
			title: '#',
			width: 20,
			type: 'number',
			textAlign: 'center',
		}, {
			field: 'title',
			title: 'Title',
		}, {
			field: 'status',
			title: 'Status',
			template: function(row) {
				var status = {
					1: {'title': 'Active', 'class': 'kt-badge--brand'},
					0: {'title': 'Inactive', 'class': ' kt-badge--danger'},
				};
				return '<span class="kt-badge ' + status[row.status].class + ' kt-badge--inline kt-badge--pill">' + status[row.status].title + '</span>';
			},
		}, {
			field: 'Actions',
			title: 'Actions',
			sortable: false,
			width: 110,
			overflow: 'visible',
			autoHide: false,
			template: function(row) {
				var edit = '<button class="btn btn-sm btn-clean btn-icon btn-icon-md edit-action" title="Edit details" data-id="'+row.id+'">\
					<i class="la la-edit"></i>\
				</a>';
				var delete_action ='<button class="btn btn-sm btn-clean btn-icon btn-icon-md delete-action" title="Delete" data-id="'+row.id+'">\
					<i class="la la-trash"></i>\
				</button>';
				return edit+delete_action;
			},
		}
	],
});
$(document).ready(function(){
	 $('.summernote').summernote({
    height: 300
  });
 	$("#et_form").validate ({
    rules: {
      title:{
        required : true,
      },
      subject:{
      	required : true,
      },
      message:{
      	required : true,
      }
    }, 
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      element.closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.error').remove();
    }
	});

	$('.edit-action').click(function(){
		$.ajax({
      type:'POST',   
      url: "<?php echo base_url('settings/get_email_template_remote'); ?>",
      data: {"et_id":$(this).attr("data-id")},
      success: function(data){
        var json = jQuery.parseJSON(data);
        $('#et_title').val(json.template.title);
        if(json.template.status == 1){
          $('#et_status').prop('checked',true);
        }else{
          $('#et_status').prop('checked',false);
        }
        $('#et_id').val(json.template.id);
        $('#et_subject').val(json.template.subject);
        $('#et_message').summernote('code', json.template.message);
        $('#modal_title').html('Edit <b>"'+json.template.title+"\"</b> ");
        $('#et_model').modal('show');
      }
    });
	});
	$('.delete-action').click(function(){
		$('#delete_department_id').val($(this).attr("data-id"));
    $('#delete_department_model').modal('show');
	});
});
function add_email_templaate(){
	$('#et_message').summernote('code', "");
	$('#et_title').val("");
  $('#et_subject').val("");
  $('#et_message').val("");
  $('#modal_title').html("Add ");
  $('#et_model').modal('show');
}
</script>