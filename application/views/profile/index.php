<script src="<?php echo base_url('assets/vendors/custom/password/PassRequirements.js'); ?>"></script>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title"><?php echo $this->session->userdata('name'); ?></h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Profile 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					<?php echo $this->session->userdata('name'); ?> 
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
		<div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">
			<div class="kt-portlet kt-portlet--height-fluid-">
				<div class="kt-portlet__head kt-portlet__head--noborder">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget kt-widget--user-profile-2">
						<div class="kt-widget__head">
							<div class="kt-widget__media">
								<div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
                    <div class="kt-avatar__holder" style="background-image: url(<?php if($this->session->userdata('profile_image')!=''){ echo base_url('documents/users/profile_images/').$this->session->userdata('profile_image'); }else{ echo base_url('documents/users/profile_images/default.jpg'); } ?>)"></div>
                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                        <i class="fa fa-pen"></i>
                        <form method="post" id="profile-pic-upload" action="<?php echo base_url('profile/profile_pic_upload'); ?>" enctype="multipart/form-data">
                        	<input type="file" onchange="alert_magic();" name="profile" accept=".png, .jpg, .jpeg">
                        </form>
                    </label>
                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                        <i class="fa fa-times"></i>
                    </span>
                </div>
								<!-- <img class="kt-widget__img kt-hidden-" src="<?php echo base_url(); ?>assets/media/users/100_1.jpg" alt="image"> -->
								<?php 
									$words = explode(" ", $this->session->userdata('name'));
									$initials = null;
									foreach ($words as $w) {
									 	$initials .= $w[0];
									}
								?>
								<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
									<?php echo strtoupper($initials); ?>
								</div>
							</div>
							<div class="kt-widget__info">
								<a href="#" class="kt-widget__username">
									<?php echo $this->session->userdata('name'); ?>
								</a>
								<span class="kt-widget__desc">
									<?php echo $this->session->userdata('department_name'); ?>
								</span>
							</div>
						</div>
						<div class="kt-widget__body">
							<div class="kt-widget__section">
								<?php echo $this->session->userdata('bio'); ?>
							</div>
							<div class="kt-widget__item">
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Email:</span>
									<a href="mailto:<?php echo $this->session->userdata('email'); ?>" class="kt-widget__data"><?php echo $this->session->userdata('email'); ?></a>
								</div>
								<div class="kt-widget__contact">
									<span class="kt-widget__label">Phone:</span>
									<a href="#" class="kt-widget__data"><?php echo $this->session->userdata('phone'); ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Change Password
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
					</div>
				</div>
				<div class="kt-portlet__body">
					<form method="post" action="<?php echo base_url('profile/change_password'); ?>" id="change_password_form">
						<div class="form-group">
							<label>Old Password</label>
							<input type="password" name="old_password" id="old_password" class="form-control">
						</div>
						<div class="form-group">
							<label>New Password</label>
							<input type="password" name="password" id="password" class="form-control pr-password">
						</div>
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" name="confirm_password" id="confirm_password" class="form-control">
						</div>
					</form>
				</div>
				<div class="kt-portlet__foot">
					<button type="button" onclick="$('#change_password_form').submit();" class="btn btn-primary">Change</button>
				</div>
			</div>
		</div>
		<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Your Detail
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
					</div>
				</div>
				<div class="kt-portlet__body">
					<form method="post" action="<?php echo base_url('profile/save_detail'); ?>" id="profile_detail_form">
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group">
									<label>Name<sup class="kt-font-danger">*</sup></label>
									<input type="text" name="name" id="name" class="form-control" value="<?php echo $this->session->userdata('name'); ?>">
								</div>
								<div class="form-group">
									<label>Emergency Phone</label>
									<input type="text" name="emergency_phone" id="emergency_phone" class="form-control" value="<?php echo $this->session->userdata('emergency_phone'); ?>">
								</div>
								<div class="form-group">
									<label>Blood Group</label>
									<input type="text" name="blood_group" id="blood_group" class="form-control" value="<?php echo $this->session->userdata('blood_group'); ?>">
								</div>
								<div class="form-group">
									<label>Address Line 1</label>
									<input type="text" name="address_line_1" id="address_line_1" class="form-control" value="<?php echo $this->session->userdata('address_line_1'); ?>">
								</div>
								<div class="form-group">
									<label>City</label>
									<input type="text" name="city" id="city" class="form-control" value="<?php echo $this->session->userdata('city'); ?>">
								</div>
								<div class="form-group">
									<label>Country</label>
									<input type="text" name="country" id="country" class="form-control" value="<?php echo $this->session->userdata('country'); ?>">
								</div>
							</div>
							<div class="col-xl-6">
								<div class="form-group">
									<label>Phone<sup class="kt-font-danger">*</sup></label>
									<input type="text" name="phone" id="phone" class="form-control" value="<?php echo $this->session->userdata('phone'); ?>">
								</div>
								<div class="form-group">
									<label>Date of Birth</label>
									<input type="text" name="date_of_birth" id="date_of_birth" class="form-control" value="<?php if($this->session->userdata('date_of_birth')!=''){ echo date('m/d/Y',strtotime($this->session->userdata('date_of_birth'))); } ?>" readonly>
								</div>
								<div class="form-group">
									<label>Marital Status</label>
									<select name="marital_status" id="marital_status" class="form-control">
										<option value="">Select Martial Status</option>
										<option value="0" <?php if($this->session->userdata('marital_status')!=''){ if($this->session->userdata('marital_status')==0){ echo "selected"; } } ?>>Unmarried</option>
										<option value="1" <?php if($this->session->userdata('marital_status')!=''){ if($this->session->userdata('marital_status')==1){ echo "selected"; } } ?>>Married</option>
									</select>
								</div>
								<div class="form-group">
									<label>Address Line 2</label>
									<input type="text" name="address_line_2" id="address_line_2" class="form-control" value="<?php echo $this->session->userdata('address_line_2'); ?>">
								</div>
								<div class="form-group">
									<label>State</label>
									<input type="text" name="state" id="state" class="form-control" value="<?php echo $this->session->userdata('state'); ?>">
								</div>
								<div class="form-group">
									<label>Zip Code</label>
									<input type="text" name="zip_code" id="zip_code" class="form-control" value="<?php echo $this->session->userdata('zip_code'); ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Bio</label>
							<textarea name="bio" id="bio" class="form-control"><?php echo $this->session->userdata('bio'); ?></textarea>
						</div>
					</form>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-align-right">
						<button type="button" class="btn btn-brand" onclick="$('#profile_detail_form').submit();">Save Detail</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#date_of_birth').datepicker({
	    format: "m/dd/yyyy",
	    todayHighlight: true,
	    autoclose: true
	});
	$('#password').PassRequirements({
        rules: {
        	minlength: {
		      	text: "Be at least minLength characters long.",
		      	minLength: 8,
		    },
            containSpecialChars: {
                text: "Contain at least minLength special character(s).",
                minLength: 1,
                regex: new RegExp('([^!,%,&,@,#,$,^,*,?,_,~])', 'g')
            },
            containNumbers: {
                text: "Contain at least minLength number(s).",
                minLength: 2,
                regex: new RegExp('[^0-9]', 'g')
            },
            containLowercase: {
		      	text: "Contain at least minLength lower case character.",
		      	minLength: 1,
		      	regex: new RegExp('[^a-z]', 'g')
		    },
		    containUppercase: {
		      	text: "Contain at least minLength upper case character.",
		      	minLength: 1,
		      	regex: new RegExp('[^A-Z]', 'g')
		    },
		    containWord: {
		      	text: "Not contain AIMTRON Keyword.",
		      	minLength: 1,
		      	regex: new RegExp('^.*AIMTRON.*', 'i')
		    },
		    containName: {
		      	text: "Not contain your name.",
		      	minLength: 1,
		      	regex: new RegExp('^.*<?php echo strtoupper($this->session->userdata('name')); ?>.*', 'i')
		    }
        },
        popoverPlacement: 'top',
        defaults: false,
        trigger: 'click'
    });
	$.validator.addMethod("pwcheck", function(value) {
	   	return /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/.test(value)  && value.toUpperCase().search("AIMTRON") == -1 && value.toUpperCase().search("<?php echo strtoupper($this->session->userdata('name')); ?>") == -1
	});
	$("#change_password_form").validate ({
    rules: {
    	old_password:{
    		required:true,
    	},
      password:{
        required : true,
        pwcheck: true,
      },
      confirm_password:{
        required : true,
        equalTo:"#password",
      },
    }, 
    messages: {
        password: {
            pwcheck: "This pattern does not match with requested format"
        }
    },
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      element.closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.error').remove();
    }
	});
	$("#profile_detail_form").validate ({
	    rules: {
	    	name:{
	    		required:true,
	    	},
	      	phone:{
	        	required : true,
	      	}
	    }, 
	    highlight: function(element) {
	      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	    },
	    success: function(element) {
	      element.closest('.form-group').removeClass('has-error').addClass('has-success');
	      $(element).closest('.error').remove();
	    }
	});
	new KTAvatar('kt_user_avatar_3');
});
function alert_magic(){
	$('#profile-pic-upload').submit();
}
</script>