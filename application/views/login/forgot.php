<div class="kt-login__body">
	<div class="kt-login__form">
		<div class="kt-login__title">
			<h3>Forgot Password</h3>
		</div>
		<form id="fp_form" class="kt-form" method="post" action="<?php echo base_url('login/send_forgot_password_link'); ?>" novalidate="novalidate">
			<div class="form-group">
            	<div class="kt-radio-inline">
					<label class="kt-radio">
						<input name="user_type" id="user_type_your_self" value="1" checked="" type="radio"> Yourself
						<span></span>
					</label>
					<label class="kt-radio">
						<input name="user_type" id="user_type_team" value="2" type="radio"> Team
						<span></span>
					</label>
				</div>
            </div>
			<div class="form-group">
				<input class="form-control" type="text" placeholder="Enter username to recover password" name="forgot_password_username" autocomplete="off">
			</div>
			<div class="form-group">
				<input class="form-control" type="text" placeholder="Enter email to recover password" name="forgot_password_email" autocomplete="off">
			</div>
			<div class="kt-login__actions">
				<a href="<?php echo base_url('login'); ?>" class="kt-link kt-login__link-forgot">
					Back to Login
				</a>
				<button type="submit" name="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Send</button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#fp_form').validate({
			rules: {
				forgot_password_email: {
					required: true,
					email:true,
				}
			},
			highlight: function(element) {
	      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	    },
	    success: function(element) {
	      element.closest('.form-group').removeClass('has-error').addClass('has-success');
	      $(element).closest('.error').remove();
	    }
		});
	});
</script>