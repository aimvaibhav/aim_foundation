<div class="kt-login__body">
	<div class="kt-login__form">
		<div class="kt-login__title">
			<h3>Sign In</h3>
		</div>
		<form id="login_form" class="kt-form" method="post" action="<?php echo base_url('login'); ?>" novalidate="novalidate">
			<div class="form-group">
				<input class="form-control" type="text" placeholder="Username" name="username" autocomplete="off">
			</div>
			<div class="form-group">
				<input class="form-control" type="password" placeholder="Password" name="password">
			</div>
			<div class="kt-login__actions">
				<a href="<?php echo base_url('forgot-password'); ?>" class="kt-link kt-login__link-forgot">
					Forgot Password ?
				</a>
				<button type="submit" name="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Sign In</button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#login_form').validate({
			rules: {
				username: {
					required: true
				},
				password: {
					required: true
				}
			}
		});
	});
</script>