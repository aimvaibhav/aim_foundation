<script src="<?php echo base_url('assets/vendors/custom/password/PassRequirements.js'); ?>"></script>
<div class="kt-login__body">
	<div class="kt-login__form">
		<div class="kt-login__title">
			<h3>Reset Password</h3>
		</div>
		<form id="rp_form" class="kt-form" method="post" action="<?php echo base_url('login/expire_password_update'); ?>" novalidate="novalidate">
			<div class="form-group">
				<input class="form-control" type="password" placeholder="Old Password" name="old_password" id="old_password" autocomplete="off">
			</div>
			<div class="form-group">
				<input class="form-control" type="password" placeholder="New Password" name="password" id="password" autocomplete="off">
			</div>
			<div class="form-group">
				<input class="form-control" type="password" placeholder="Confirm Password" name="confirm_password" id="confirm_password" autocomplete="off">
			</div>
			<div class="kt-login__actions">
				<a href="<?php echo base_url('login'); ?>" class="kt-link kt-login__link-forgot">
					Back to Login
				</a>
				<button type="submit" name="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Save</button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#password').PassRequirements({
        rules: {
        	minlength: {
		      	text: "Be at least minLength characters long.",
		      	minLength: 8,
		    },
            containSpecialChars: {
                text: "Contain at least minLength special character(s).",
                minLength: 1,
                regex: new RegExp('([^!,%,&,@,#,$,^,*,?,_,~])', 'g')
            },
            containNumbers: {
                text: "Contain at least minLength number(s).",
                minLength: 2,
                regex: new RegExp('[^0-9]', 'g')
            },
            containLowercase: {
		      	text: "Contain at least minLength lower case character.",
		      	minLength: 1,
		      	regex: new RegExp('[^a-z]', 'g')
		    },
		    containUppercase: {
		      	text: "Contain at least minLength upper case character.",
		      	minLength: 1,
		      	regex: new RegExp('[^A-Z]', 'g')
		    },
		    containWord: {
		      	text: "Not contain AIMTRON Keyword.",
		      	minLength: 1,
		      	regex: new RegExp('^.*AIMTRON.*', 'i')
		    },
		    containName: {
		      	text: "Not contain your name.",
		      	minLength: 1,
		      	regex: new RegExp('^.*<?php echo strtoupper($this->session->userdata('expire_username')); ?>.*', 'i')
		    }
        },
        popoverPlacement: 'top',
        defaults: false,
        trigger: 'click'
    });
	$.validator.addMethod("pwcheck", function(value) {
	   	return /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/.test(value)  && value.toUpperCase().search("AIMTRON") == -1 && value.toUpperCase().search("<?php echo strtoupper($this->session->userdata('expire_username')); ?>") == -1
	});
	$('#rp_form').validate({
		rules: {
			old_password:{
				required:true,
			},
			password: {
				required: true,
				pwcheck: true,
			},
			confirm_password: {
				required: true,
				equalTo:"#password",
			},
		},
		messages: {
	        password: {
	            pwcheck: "This pattern does not match with requested format"
	        }
	    },
		highlight: function(element) {
	      	$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	    },
   		success: function(element) {
	      	element.closest('.form-group').removeClass('has-error').addClass('has-success');
	      	$(element).closest('.error').remove();
	    }
	});
});
</script>