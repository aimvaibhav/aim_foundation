<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">My Tickets</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Support 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					My Tickets 
				</a>
			</div>
		</div>
	</div>
</div>
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<table class="table table-bordered table-striped" id="ticket_table">
				<thead>
					<th>Ticket#</th>
					<th>Date</th>
					<th>Issue</th>
					<th>Priority</th>
					<th>Status</th>
					<th>Assign</th>
					<th>Action</th>
				</thead>
				<tbody>
					<?php foreach ($tickets as $ticket) { ?>
					<tr>
						<td>
							<a href="<?php echo base_url('support/ticket_detail/').$ticket->id; ?>" target="_blank">
								<?php 
									$company_shortcode = $this->config->item('company_shorcode');
									echo $company_shortcode.'-'.str_pad($ticket->id, 4, '0', STR_PAD_LEFT); 
								?>
							</a>
						</td>
						<td>
							<?php echo date('m/d/Y h:i:s',strtotime($ticket->date)); ?>
						</td>
						<td>
							<?php echo $ticket->issue; ?>
						</td>
						<td>
							<?php 
								if($ticket->priority=='1'){ 
									echo '<span class="badge badge-inline badge-success">Low</span>'; 
								}else if($ticket->priority=='2'){
									echo '<span class="badge badge-inline badge-warning">Medium</span>'; 
								}else if($ticket->priority=='3'){
									echo '<span class="badge badge-inline badge-danger">High</span>'; 
								}
							?>
						</td>
						<td>
							<?php
								if($ticket->status==0){
									echo 'New';
								}else if($ticket->status==1){
									echo 'Assign';
								}else if($ticket->status==2){
									echo 'Working';
								}else if($ticket->status==3){ 
									echo 'Resolved';
								}else if($ticket->status==4){
									echo 'Reopen';
								}else if($ticket->status==5){
									echo 'Closed';
								}
							?>
						</td>
						<td><?php echo $ticket->assign_person; ?></td>
						<td>
							<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="edit_ticket('<?php echo $ticket->id; ?>');"><i class="la la-edit"></i></button>
							<?php if($ticket->status==3){ ?>
								<?php if($ticket->status!=5){ ?>
									<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="reopen_ticket('<?php echo $ticket->id; ?>');"><i class="la la-refresh"></i></button>
								<?php } ?>
								<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="close_ticket('<?php echo $ticket->id; ?>');"><i class="la la-check"></i></button>
							<?php } ?>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="edit_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					<span id="modal_title">Edit</span> Ticket
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="edit_form" method="post" action="<?php echo base_url('support/save_ticket'); ?>">
					<input type="hidden" name="ticket_id" id="ticket_id">
					<div class="form-group">
						<label class="control-label">Ticket Type</label>
		            	<div class="kt-radio-inline">
							<label class="kt-radio">
								<input name="ticket_type" id="ticket_type_hardware" value="1" checked="" type="radio"> Hardware/Network
								<span></span>
							</label>
							<label class="kt-radio">
								<input name="ticket_type" id="ticket_type_software" value="2" type="radio"> Software
								<span></span>
							</label>
						</div>
		            </div>
		            <div id="hardware_part">
			            <div class="form-group">
			            	<div class="kt-radio-inline">
								<label class="kt-radio">
									<input name="hardware_type" id="hardware_type_issue" value="1" checked="" type="radio"> Issue
									<span></span>
								</label>
								<label class="kt-radio">
									<input name="hardware_type" id="hardware_type_request" value="2" type="radio"> New Request
									<span></span>
								</label>
							</div>
			            </div>
			            <div class="form-group">
							<label class="control-label">Item</label>
							<select name="hardware_item" id="hardware_item" class="form-control">
								<option value="">Select Item</option>
								<option value="Internet">Internet</option>
								<option value="Laptop">Laptop</option>
								<option value="Computer">Computer</option>
								<option value="Moniter">Moniter</option>
								<option value="CPU">CPU</option>
								<option value="Keyboard">Keyboard</option>
								<option value="Mouse">Mouse</option>
								<option value="Mobile">Mobile</option>
								<option value="Other">Other</option>
							</select>
						</div>
						<div class="form-group" id="hardware_other_div" style="display: none;">
							<label class="control-label">Other Item</label>
							<input type="text" name="hardware_other_item" id="hardware_other_item" class="form-control">
						</div>
					</div>
					<div id="software_part" style="display: none;">
						<div class="form-group">
			            	<div class="kt-radio-inline">
								<label class="kt-radio">
									<input name="software_type" id="software_type_issue" value="1" checked="" type="radio"> Issue
									<span></span>
								</label>
								<label class="kt-radio">
									<input name="software_type" id="software_type_request" value="2" type="radio"> New Request
									<span></span>
								</label>
							</div>
			            </div>
			            <div class="form-group">
							<label class="control-label">Item</label>
							<select name="software_item" id="software_item" class="form-control">
								<option value="">Select Item</option>
								<option value="Aimnet">Aimnet</option>
								<option value="Epicor">Epicor</option>
								<option value="Hr Portal">Hr Portal</option>
								<option value="Office 365">Office 365</option>
								<option value="Goto Metting">Goto Metting</option>
								<option value="Skype">Skype</option>
								<option value="Team">Team</option>
								<option value="Other">Other</option>
							</select>
						</div>
						<div class="form-group" id="software_other_div" style="display: none;">
							<label class="control-label">Other Item</label>
							<input type="text" name="software_other_item" id="software_other_item" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label>Issue</label>
						<input type="text" name="issue" id="issue" class="form-control">
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control summernote" name="description" id="description"></textarea>
					</div>
					<div class="form-group">
						<label>Priority</label>
						<select class="form-control" name="priority" id="priority">
							<option value="1">Low</option>
							<option value="2">Medium</option>
							<option value="3">High</option>
						</select>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#edit_form').submit();">Save</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="reopen_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Reopen Ticket
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="reopen_form" method="post" action="<?php echo base_url('support/reopen_ticket'); ?>">
					<input type="hidden" name="reopen_ticket_id" id="reopen_ticket_id">
					<h4 class="kt-font-primary">Are you sure you want to reopen this ticket ??</h4>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#reopen_form').submit();">Reopen</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="close_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Close Ticket
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="close_form" method="post" action="<?php echo base_url('support/close_ticket'); ?>">
					<input type="hidden" name="close_ticket_id" id="close_ticket_id">
					<h4 class="kt-font-primary">Are you sure you want to close this ticket ??</h4>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#close_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#ticket_table').DataTable({
		dom: `<'row'<'col-sm-12 text-left'f>>
			<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
		pageLength: 500,
		lengthMenu: [ 10, 25, 50, 75, 100, 200, 300, 400, 500 ],
		order: [[ 0, "desc" ]],
	    responsive: true,
		bAutoWidth: false
	});
	$("#edit_form").validate ({
    rules: {
      issue:{
        required : true,
      },
      description:{
      	required:true,
      },
      priority:{
      	required:true,
      }
    }, 
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      element.closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.error').remove();
    }
	});
	$('.summernote').summernote({
    height: 300
  });
	$('input[name=ticket_type]').on('change',function(){
		if($(this).val()==1){
			$('#software_part').hide();
			$('#hardware_part').show();
		}else{
			$('#software_part').show();
			$('#hardware_part').hide();
		}
	});
	$('#software_item').on('change',function(){
		if($(this).val()=='Other'){
			$('#software_other_div').show();
		}else{
			$('#software_other_div').hide();
		}
	});
	$('#hardware_item').on('change',function(){
		if($(this).val()=='Other'){
			$('#hardware_other_div').show();
		}else{
			$('#hardware_other_div').hide();
		}
	});
});
function edit_ticket(ticket_id){
	$.ajax({
    type:'POST',   
    url: "<?php echo base_url('support/get_ticket_remote'); ?>",
    data: {"ticket_id":ticket_id},
    success: function(data){
      var json = jQuery.parseJSON(data);
      $('#issue').val(json.ticket.issue);
      $('#description').summernote('code', json.ticket.description);
      $('#ticket_id').val(json.ticket.id);
      if(json.ticket.ticket_type==1){
    	$('#ticket_type_hardware').attr('checked',true);
  	  }else{
  	  	$('#ticket_type_software').attr('checked',true);
  	  }
  	  if(json.ticket.hardware_type==1){
    	$('#hardware_type_issue').attr('checked',true);
  	  }else{
  	  	$('#hardware_type_request').attr('checked',true);
  	  }
  	  if(json.ticket.software_type==1){
    	$('#software_type_issue').attr('checked',true);
  	  }else{
  	  	$('#software_type_request').attr('checked',true);
  	  }
      $('#hardware_item').val(json.ticket.hardware_item);
      $('#hardware_other_item').val(json.ticket.hardware_other_item);
      $('#software_item').val(json.ticket.software_item);
      $('#software_other_item').val(json.ticket.software_other_item);
      if(json.ticket.ticket_type==1){
		$('#software_part').hide();
		$('#hardware_part').show();
	  }else{
		$('#software_part').show();
		$('#hardware_part').hide();
	  }
	  if(json.ticket.software_item=='Other'){
	  	$('#software_other_div').show();
	  }else{
	  	$('#software_other_div').hide();
	  }
	  if(json.ticket.hardware_item=='Other'){
	  	$('#hardware_other_div').show();
	  }else{
	  	$('#hardware_other_div').hide();
	  }
      $('#priority').val(json.ticket.priority);
      $('#edit_model').modal('show');
    }
  });
}
function reopen_ticket(ticket_id){
	$('#reopen_ticket_id').val(ticket_id);
	$('#reopen_model').modal('show');
}
function close_ticket(ticket_id){
	$('#close_ticket_id').val(ticket_id);
	$('#close_model').modal('show');
}
</script>