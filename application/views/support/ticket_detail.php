<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Ticket Detail</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Support 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Ticket Detail
				</a>
			</div>
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="row">
      	<div class="col-md-3">
      		<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Ticket Detail
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="margin-bottom-10">
						<h4>Date</h4>
						<div><?php echo date('m/d/Y h:i:s',strtotime($mytickets['date'])); ?></div>
					</div>
					<div class="margin-bottom-10">
						<h4>Priority</h4>
						<div>
							<?php 
								if($mytickets['priority']=='1'){ 
									echo '<span class="badge badge-inline badge-success">Low</span>'; 
								}else if($mytickets['priority']=='2'){
									echo '<span class="badge badge-inline badge-warning">Medium</span>'; 
								}else if($mytickets['priority']=='3'){
									echo '<span class="badge badge-inline badge-danger">High</span>'; 
								}
							?>
						</div>
					</div>
					<div class="margin-bottom-10">
						<h4>Issue</h4>
						<div><?php echo $mytickets['issue']; ?></div>
					</div>
					<div class="margin-bottom-10">
						<h4>Description</h4>
						<div><?php echo $mytickets['description']; ?></div>
					</div>
					<div class="margin-bottom-10">
						<h4>Status</h4>
						<div>
							<?php
								if($mytickets['status']==0){ 
									echo '<span class="badge badge-inline badge-info">New</span>';
								}else if($mytickets['status']==1){ 
									echo '<span class="badge badge-inline badge-success">Assign</span>';
								}else if($mytickets['status']==2){ 
									echo '<span class="badge badge-inline badge-warning">Working</span>';
								}else if($mytickets['status']==3){ 
									echo '<span class="badge badge-inline badge-success">Resolved</span>';
								}else if($mytickets['status']==4){ 
									echo '<span class="badge badge-inline badge-warning">Reopen</span>';
								}else if($mytickets['status']==5){ 
									echo '<span class="badge badge-inline badge-danger">Closed</span>';
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
      		<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Files
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<form class="kt-dropzone dropzone no-border" action="<?php echo base_url('support/upload_ticket_document'); ?>" method="POST" id="doczone">
						<input type="hidden" name="ticket_id" id="ticket_id" value="<?php echo $mytickets['id']; ?>">
			            <div class="kt-dropzone__msg dz-message needsclick">
			              <h4 class="kt-dropzone__msg-title">
			                <i class="flaticon-download-1 font-size-100"></i><br/>
			                Drop files here or click to upload.<br />
			              </h4>
			              <div class="fallback">
			                <input name="files" id="files" type="file" multiple accept="application/pdf">
			              </div>
			            </div>
			        </form>
			        <br /><br />
			        <?php if(!empty($ticket_documents)){ ?>
			        <div class="row">
						<div class="col-md-12">
		          			<?php foreach ($ticket_documents as $document){ ?>
								<div class="doc-block">
									<a href="<?php echo base_url('documents/support/ticket_document/').$document->document_name; ?>" target="_blank">
			            				<img src="<?php echo base_url('documents/support/ticket_document/').$document->document_name; ?>" class="img-thumbnail img-size-150">
			          				</a>
			          				<div class="text-center row">
			          					<h6 class="text-center my-2 col-md-8"><?php echo $document->original_name; ?></h6>
										<button type="button" class="col-md-4 btn btn-sm btn-clean btn-icon btn-icon-md" onclick="delete_document('<?php echo $document->id; ?>');"><i class="la la-trash"></i></button>
			          				</div>
		          				</div>
							<?php } ?>
						</div>
					</div>
					<?php }else{ ?>
						<div class="alert alert-warning" role="alert">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text">No files found for this ticket.</div>
                        </div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-md-3">
      		<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Comment
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<form class="kt-form margin-bottom-20" id="comment_form" method="post" action="<?php echo base_url('support/save_comment'); ?>">
						<input type="hidden" name="ticket_id" id="ticket_id" value="<?php echo $mytickets['id']; ?>">
						<div class="form-group">
							<textarea class="form-control" name="comment" id="comment"></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Save</button>
					</form>
					<?php if(!empty($ticket_comments)){ ?>
						<div class="row">
							<div class="col-md-12">
								<?php foreach ($ticket_comments as $comment){ ?>
									<div class="kt-widget__contact">
										<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="delete_comment('<?php echo $comment->id; ?>');"><i class="la la-trash"></i></button>
										<span class="kt-widget__label"><?php echo date('M-d Y h:i',$comment->added_time); ?>  :</span>
										<span class="kt-widget__data"><?php echo $comment->comment; ?></span>
									</div>
									<hr class="mt-0">
								<?php } ?>
							</div>
						</div>
					<?php }else{ ?>
						<div class="alert alert-warning" role="alert">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text">No comments added for this ticket.</div>
                        </div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="edit_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					<span id="modal_title">Edit</span> Comment
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="edit_form" method="post" action="<?php echo base_url('support/edit_comment'); ?>">
					<input type="hidden" name="comment_id" id="comment_id">
					<input type="hidden" name="edit_ticket_id" id="edit_ticket_id">
					<div class="form-group">
						<label class="control-label">Comment</label>
						<input type="text" name="comments" id="comments" class="form-control">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#edit_form').submit();">Save</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_comment_model" tabindex="-1" role="dialog" aria-labelledby="DeleteticketcommentModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="Course">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="DeleteticketcommentModalLabel">
					<span class="text-center" id="delete_comment_title">Delete</span> - Comment
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="delete_comment_form" method="post" action="<?php echo base_url('support/delete_comment'); ?>">
					<input type="hidden" name="delete_comment_id" id="delete_comment_id">
					<input type="hidden" name="myticket_id" id="myticket_id" value="<?php echo $mytickets['id']; ?>">
          			<h6 class="text-center">Are you sure you still want to delete this Comment ?</h6>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#delete_comment_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_document_model" tabindex="-1" role="dialog" aria-labelledby="DeleteticketdocumentModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="Course">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="DeleteticketdocumentModalLabel">
					<span class="text-center" id="delete_document_title">Delete</span> - Document
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="delete_document_form" method="post" action="<?php echo base_url('support/delete_document'); ?>">
					<input type="hidden" name="delete_document_id" id="delete_document_id">
					<input type="hidden" name="ticket_document_id" id="ticket_document_id" value="<?php echo $mytickets['id']; ?>">
          			<h6 class="text-center">Are you sure you still want to delete this Document ?</h6>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#delete_document_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function edit_ticket(ticket_comment_id){
		$.ajax({
	    type:'POST',   
	    url: "<?php echo base_url('support/get_ticket_comment_remote'); ?>",
	    data: {"ticket_comment_id":ticket_comment_id},
	    success: function(data){
	      var json = jQuery.parseJSON(data);
	      $('#comment_id').val(json.ticket_comment.id);
	      $('#edit_ticket_id').val(json.ticket_comment.ticket_id);
	      $('#comments').val(json.ticket_comment.comment);
	      $('#edit_model').modal('show');
	    }
	  });
	}
	function delete_comment(ticket_id){
		$('#delete_comment_id').val(ticket_id);
		$('#delete_comment_model').modal('show');
	}
	function delete_document(document_id){
		$('#delete_document_id').val(document_id);
		$('#delete_document_model').modal('show');
	}
	$(document).ready(function(){
		$("#comment_form").validate ({
	    rules: {
	      comment:{
	      	required:true,
	      }
	    }, 
	    highlight: function(element) {
	      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	    },
	    success: function(element) {
	      element.closest('.form-group').removeClass('has-error').addClass('has-success');
	      $(element).closest('.error').remove();
	    }
		});
		$(document).ready(function(){
		  $('#doczone').dropzone({
		    paramName: "files", // The name that will be used to transfer the file
		    maxFilesize: 100, // MB
		    addRemoveLinks: true,
		    uploadMultiple : true,
		    parallelUploads: 20,
		    success: function(){
		      window.location.reload();
		    }
		  });
		});
	});
</script>