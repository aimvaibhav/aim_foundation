<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Assign Tickets</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Support 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Assign Tickets 
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">

		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<table class="table table-bordered" id="ticket_table">
				<thead>
					<th>Ticket#</th>
					<th>Date</th>
					<th>Issue</th>
					<th>Priority</th>
					<th>Status</th>
					<th>Added Person</th>
					<th>Action</th>
				</thead>
				<tbody>
					<?php foreach ($tickets as $ticket) { ?>
						<tr>
							<td>
								<a href="<?php echo base_url('support/ticket_detail/').$ticket->id; ?>" target="_blank">
									<?php
										$company_shortcode = $this->config->item('company_shorcode');
										echo $company_shortcode.'-'.str_pad($ticket->id, 4, '0', STR_PAD_LEFT); 
									?>
								</a>
							</td>
							<td>
								<?php echo date('m/d/Y h:i:s',strtotime($ticket->date)); ?>
							</td>
							<td>
								<?php echo $ticket->issue; ?>
							</td>
							<td>
								<?php 
									if($ticket->priority=='1'){ 
										echo '<span class="badge badge-inline badge-success">Low</span>'; 
									}else if($ticket->priority=='2'){
										echo '<span class="badge badge-inline badge-warning">Medium</span>'; 
									}else if($ticket->priority=='3'){
										echo '<span class="badge badge-inline badge-danger">High</span>'; 
									}
								?>
							</td>
							<td>
								<?php
									if($ticket->status==0){
										echo 'New';
									}else if($ticket->status==1){
										echo 'Assign';
									}else if($ticket->status==2){
										echo 'Working';
									}else if($ticket->status==3){
										echo 'Resolved';
									}else if($ticket->status==4){
										echo 'Reopen';
									}else if($ticket->status==5){
										echo 'Closed';
									}
								?>
							</td>
							<td>
								<?php echo $ticket->added_person; ?>
							</td>
							<td>
								<?php if($ticket->status==1 || $ticket->status==4){ ?>
									<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="start_ticket('<?php echo $ticket->id; ?>');">
										<i class="la la-play"></i>
									</button>
								<?php } ?>
								<?php if($ticket->status==2){ ?>
									<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="resolve_ticket('<?php echo $ticket->id; ?>');">
										<i class="la la-check"></i>
									</button>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="working_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Start Working
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="start_form" method="post" action="<?php echo base_url('support/start_ticket'); ?>">
					<input type="hidden" name="ticket_id" id="ticket_id">
					<h4 class="kt-font-primary">Are you sure you want to start working on this ticket ??</h4>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#start_form').submit();">Start</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="resolve_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Resolve Ticket
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="resolve_form" method="post" action="<?php echo base_url('support/resolve_ticket'); ?>">
					<input type="hidden" name="resolve_ticket_id" id="resolve_ticket_id">
					<h4 class="kt-font-primary">Are you sure you want to resolve this ticket ??</h4>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#resolve_form').submit();">Resolve</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#ticket_table').DataTable({
		dom: `<'row'<'col-sm-12 text-left'f>>
			<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
		pageLength: 500,
		lengthMenu: [ 10, 25, 50, 75, 100, 200, 300, 400, 500 ],
		order: [[ 0, "desc" ]],
	    responsive: true,
		bAutoWidth: false
	});
});
function start_ticket(ticket_id){
	$('#ticket_id').val(ticket_id);
	$('#working_model').modal('show');
}
function resolve_ticket(ticket_id){
	$('#resolve_ticket_id').val(ticket_id);
	$('#resolve_model').modal('show');
}
</script>