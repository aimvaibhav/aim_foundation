<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Add Ticket</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Support 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Add Ticket 
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">

		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div class="alert alert-info" role="alert">
				<div class="alert-text">
					<p><h4>Ticket Type</h4></p>
					<p><b>Hardware/Network : </b>Select hardware/network ticket type if you have issue in your pc, laptop, internet or any other hardware related activities.</p>
					<p><b>Software : </b>Select software ticket type if you have issue in aimnet, epicor, office365 or any other software related activities.</p>
				</div>
			</div>
			<form method="post" action="<?php echo base_url('support/save_ticket'); ?>" id="ticket_form">
				<div class="form-group">
					<label class="control-label">Date</label>
					<input type="text" name="date" id="date" readonly value="<?php echo date('m/d/Y H:i:s'); ?>" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Ticket Type</label>
	            	<div class="kt-radio-inline">
						<label class="kt-radio">
							<input name="ticket_type" id="ticket_type_hardware" value="1" checked="" type="radio"> Hardware/Network
							<span></span>
						</label>
						<label class="kt-radio">
							<input name="ticket_type" id="ticket_type_software" value="2" type="radio"> Software
							<span></span>
						</label>
					</div>
	            </div>
	            <div id="hardware_part">
		            <div class="form-group">
		            	<div class="kt-radio-inline">
							<label class="kt-radio">
								<input name="hardware_type" id="hardware_type_issue" value="1" checked="" type="radio"> Issue
								<span></span>
							</label>
							<label class="kt-radio">
								<input name="hardware_type" id="hardware_type_request" value="2" type="radio"> New Request
								<span></span>
							</label>
						</div>
		            </div>
		            <div class="form-group">
						<label class="control-label">Item</label>
						<select name="hardware_item" id="hardware_item" class="form-control">
							<option value="">Select Item</option>
							<option value="Internet">Internet</option>
							<option value="Laptop">Laptop</option>
							<option value="Computer">Computer</option>
							<option value="Moniter">Moniter</option>
							<option value="CPU">CPU</option>
							<option value="Keyboard">Keyboard</option>
							<option value="Mouse">Mouse</option>
							<option value="Mobile">Mobile</option>
							<option value="Other">Other</option>
						</select>
					</div>
					<div class="form-group" id="hardware_other_div" style="display: none;">
						<label class="control-label">Other Item</label>
						<input type="text" name="hardware_other_item" id="hardware_other_item" class="form-control">
					</div>
				</div>
				<div id="software_part" style="display: none;">
					<div class="form-group">
		            	<div class="kt-radio-inline">
							<label class="kt-radio">
								<input name="software_type" id="software_type_issue" value="1" checked="" type="radio"> Issue
								<span></span>
							</label>
							<label class="kt-radio">
								<input name="software_type" id="software_type_request" value="2" type="radio"> New Request
								<span></span>
							</label>
						</div>
		            </div>
		            <div class="form-group">
						<label class="control-label">Item</label>
						<select name="software_item" id="software_item" class="form-control">
							<option value="">Select Item</option>
							<option value="Aimnet">Aimnet</option>
							<option value="Epicor">Epicor</option>
							<option value="Hr Portal">Hr Portal</option>
							<option value="Office 365">Office 365</option>
							<option value="Goto Metting">Goto Metting</option>
							<option value="Skype">Skype</option>
							<option value="Team">Team</option>
							<option value="Other">Other</option>
						</select>
					</div>
					<div class="form-group" id="software_other_div" style="display: none;">
						<label class="control-label">Other Item</label>
						<input type="text" name="software_other_item" id="software_other_item" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label">Request</label>
					<input type="text" name="issue" id="issue" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Description</label>
					<textarea name="description" id="description" class="form-control summernote"></textarea>
				</div>
				<div class="form-group">
					<label class="control-label">Priority</label>
					<select class="form-control" name="priority" id="priority">
						<option value="1">Low</option>
						<option value="2">Medium</option>
						<option value="3">High</option>
					</select>
				</div>
				<div class="form-group">
					<button type="button" id="ticket_submit_button" onclick="ticket_submit();" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
 	$("#ticket_form").validate ({
	    rules: {
	      issue:{
	        required : true,
	      },
	      description:{
	      	required:true,
	      },
	      priority:{
	      	required:true,
	      }
	    }, 
	    highlight: function(element) {
	      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	    },
	    success: function(element) {
	      element.closest('.form-group').removeClass('has-error').addClass('has-success');
	      $(element).closest('.error').remove();
	    }
		});
		$('.summernote').summernote({
	    height: 300
	});
	$('input[name=ticket_type]').on('change',function(){
		if($(this).val()==1){
			$('#software_part').hide();
			$('#hardware_part').show();
		}else{
			$('#software_part').show();
			$('#hardware_part').hide();
		}
	});
	$('#software_item').on('change',function(){
		if($(this).val()=='Other'){
			$('#software_other_div').show();
		}else{
			$('#software_other_div').hide();
		}
	});
	$('#hardware_item').on('change',function(){
		if($(this).val()=='Other'){
			$('#hardware_other_div').show();
		}else{
			$('#hardware_other_div').hide();
		}
	});
});
function ticket_submit(){
	$('#ticket_submit_button').attr('disabled',true);
	$('#ticket_form').submit();
}
</script>