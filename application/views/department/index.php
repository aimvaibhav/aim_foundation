<div class="kt-subheader kt-grid__item" id="kt_subheader">
	<div class="kt-container kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Departments</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					IT 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Departments 
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<?php 
	      $access=$this->access->can_access(); 
	      $departments_access=array_search('departments', array_column($access, 'param')); 
	      if($access[$departments_access]['can_create']==1){
	    ?>
			<button type="button" class="btn btn-brand btn-icon-sm" onclick="add_department();">
				<i class="flaticon2-plus"></i> Add New
			</button>
			<?php } ?>
		</div>
	</div>
</div>
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-md-12 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" placeholder="Search Department" id="generalSearch">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-datatable" id="ajax_data"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="department_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					<span id="modal_title">Add</span> Department
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="department_form" method="post" action="<?php echo base_url('departments/save_department'); ?>">
					<input type="hidden" name="department_id" id="department_id">
					<div class="form-group">
						<label class="kt-checkbox">
							<input type="checkbox" name="status" id="department_status" value="1"> Active
							<span></span>
						</label>
					</div>
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" id="department_name" class="form-control">
					</div>
					<div class="form-group">
						<label>Supervisor</label>
						<div class="row">
							<div class="col-md-12">
								<select name="supervisor" id="supervisor" class="form-control kt-select2">
									<option value="">Select Supervisor</option>
									<?php foreach ($users as $user) { ?>
										<option value="<?php echo $user->id; ?>"><?php echo $user->name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#department_form').submit();">Save</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_department_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Delete Department
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="delete_department_form" method="post" action="<?php echo base_url('departments/delete_department'); ?>">
					<input type="hidden" name="delete_department_id" id="delete_department_id">
					<div class="alert alert-warning" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">This action is affect in many other places and all data will be deleted after this action.</div>
        	</div>
          <h4 class="text-center">Are you sure you still want to delete this department ?</h4>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#delete_department_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var datatable = $('.kt-datatable').KTDatatable({
		data: {
			type: 'local',
			source: <?php echo json_encode($departments); ?>,
			pageSize: 10,
		},
		layout: {
			scroll: false,
			footer: false,
		},
		sortable: true,
		pagination: true,
		search: {
			input: $('#generalSearch'),
		},
		columns: [
			{
				field: 'id',
				title: '#',
				width: 20,
				type: 'number',
				textAlign: 'center',
			}, {
				field: 'name',
				title: 'Name',
			}, {
				field: 'supervisor_name',
				title: 'Supervisor',
			}, {
				field: 'status',
				title: 'Status',
				template: function(row) {
					var status = {
						1: {'title': 'Active', 'class': 'kt-badge--brand'},
						0: {'title': 'Inactive', 'class': ' kt-badge--danger'},
					};
					return '<span class="kt-badge ' + status[row.status].class + ' kt-badge--inline kt-badge--pill">' + status[row.status].title + '</span>';
				},
			}, {
				field: '',
				title: 'Actions',
				sortable: false,
				width: 110,
				overflow: 'visible',
				autoHide: false,
				template: function(row) {
					<?php if($access[$departments_access]['can_edit']==1){ ?>
					var edit = '<button class="btn btn-sm btn-clean btn-icon btn-icon-md edit-action" title="Edit details" data-id="'+row.id+'">\
						<i class="la la-edit"></i>\
					</button>';
					<?php }else{ ?>
						var edit = '';
					<?php } ?>
					<?php if($access[$departments_access]['can_delete']==1){ ?>
					var delete_action='<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md delete-action" title="Delete" data-id="'+row.id+'">\
						<i class="la la-trash"></i>\
					</a';
					<?php }else{ ?>
						var delete_action = '';
					<?php } ?>
					return edit+delete_action;
				},
			}
		],
	});
	$(document).ready(function(){
		$('#department_form').validate({
			rules: {
				name: {
					required: true
				},
				supervisor: {
					required: true
				}
			}
		});
	 	$('#supervisor').select2({
	      	placeholder: "Select Supervisor"
	  	});
	 	$('#ajax_data').on('click','.edit-action',function(){
			$.ajax({
        type:'POST',   
        url: "<?php echo base_url('departments/get_department_remote'); ?>",
        data: {"department_id":$(this).attr("data-id")},
        success: function(data){
          var json = jQuery.parseJSON(data);
          $('#department_name').val(json.department.name);
          if(json.department.status == 1){
            $('#department_status').prop('checked',true);
          }else{
            $('#department_status').prop('checked',false);
          }
          $("#supervisor").select2("val", ""+json.department.supervisor+"");
          $('#department_id').val(json.department.id);
          $('#modal_title').html('Edit <b>"'+json.department.name+'"</b>');
          $('#department_model').modal('show');
        }
      });
		});
		$('#ajax_data').on('click','.delete-action',function(){
			$('#delete_department_id').val($(this).attr("data-id"));
      $('#delete_department_model').modal('show');
		});
	});
	function add_department(){
		$('#department_name').val("");
    $('#department_status').prop('checked',true);
    $('#department_id').val("");
    $('#modal_title').html("Add ");
    $("#supervisor").select2("val", " ");
    $('#department_model').modal('show');
	}
</script>