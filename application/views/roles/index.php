<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Roles</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					IT 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="javascript:;" class="kt-subheader__breadcrumbs-link">
					Roles 
				</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		 	<?php 
	      $access=$this->access->can_access(); 
	      $roles_access=array_search('roles', array_column($access, 'param')); 
	      if($access[$roles_access]['can_create']==1){
	    ?>
			<a href="<?php echo base_url('roles/add_role'); ?>" class="btn btn-brand btn-icon-sm">
				<i class="flaticon2-plus"></i> Add New
			</a>
			<?php  } ?>
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-md-12 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" placeholder="Search Role" id="generalSearch">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-datatable" id="ajax_data"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_role_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Delete Role
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form class="kt-form" id="delete_role_form" method="post" action="<?php echo base_url('roles/delete_role'); ?>">
					<input type="hidden" name="delete_role_id" id="delete_role_id">
					<div class="alert alert-warning" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">This action is affect in many other places and all data will be deleted after this action.</div>
        	</div>
          <h4 class="text-center">Are you sure you still want to delete this role ?</h4>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary" onclick="$('#delete_role_form').submit();">Yes</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var datatable = $('.kt-datatable').KTDatatable({
		data: {
			type: 'local',
			source: <?php echo json_encode($roles); ?>,
			pageSize: 10,
		},
		layout: {
			scroll: false,
			footer: false,
		},
		sortable: true,
		pagination: true,
		search: {
			input: $('#generalSearch'),
		},
		columns: [
			{
				field: 'id',
				title: '#',
				width: 20,
				type: 'number',
				textAlign: 'center',
			}, {
				field: 'name',
				title: 'Name',
			}, {
				field: 'status',
				title: 'Status',
				template: function(row) {
					var status = {
						1: {'title': 'Active', 'class': 'kt-badge--brand'},
						0: {'title': 'Inactive', 'class': ' kt-badge--danger'},
					};
					return '<span class="kt-badge ' + status[row.status].class + ' kt-badge--inline kt-badge--pill">' + status[row.status].title + '</span>';
				},
			}, {
				field: 'Actions',
				title: 'Actions',
				sortable: false,
				width: 110,
				overflow: 'visible',
				autoHide: false,
				template: function(row) {
					<?php if($access[$roles_access]['can_edit']==1){ ?>
					var edit = '<a href="<?php echo base_url(); ?>roles/edit_role/'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">\
						<i class="la la-edit"></i>\
					</a>';
					<?php }else{ ?>
						var edit = '';
					<?php } ?>
					<?php if($access[$roles_access]['can_delete']==1){ ?>
					var delete_action ='<button class="btn btn-sm btn-clean btn-icon btn-icon-md delete-action" title="Delete" data-id="'+row.id+'">\
						<i class="la la-trash"></i>\
					</button>';
					<?php }else{ ?>
						var delete_action = '';
					<?php } ?>
					return edit+delete_action;
				},
			}
		],
	});
	$(document).ready(function(){
		$('.delete-action').click(function(){
			$('#delete_role_id').val($(this).attr("data-id"));
      $('#delete_role_model').modal('show');
		});
	});
</script>