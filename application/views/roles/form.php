<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Roles</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?php echo base_url(); ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="<?php echo base_url('roles'); ?>" class="kt-subheader__breadcrumbs-link">
					Roles 
				</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="#" class="kt-subheader__breadcrumbs-link">
					<?php echo empty($role) ? "Add" : "Edit"; ?> Role 
				</a>
			</div>
		</div>
	</div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__body">
			<form method="post" action="<?php echo base_url('roles/save_role') ?>" id="role_form">
				<input type="hidden" name="role_id" id="role_id" value="<?php if(isset($role['id'])){ echo $role['id']; } ?>">
				<div class="form-group">
					<label class="kt-checkbox">
						<input type="checkbox" name="status" id="role_status" value="1" <?php if(empty($role)){ echo 'checked'; }else{  if($role['status'] == 1){ echo 'checked'; } } ?>> Active
						<span></span>
					</label>
				</div>
				<?php if(!empty($role)){ ?>
          <div class="alert alert-solid-warning alert-bold" role="alert">
            <div class="alert-text">
            	<div>Changing role permissions won't affected current users permissions that are using this role.</div>
            	<br />
	            <label class="kt-checkbox">
	              <input type="checkbox" id="user_check" name="user_check" value="1"> Update all users permissions that are using this role
	              <span></span>
	            </label>
	          </div>
          </div>
        <?php } ?>
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" id="role_name" class="form-control" value="<?php if(isset($role['name'])){ echo $role['name']; } ?>">
				</div>
				<table class="table table-bordered">
					<thead class="table-active">
						<th>Permissions</th>
						<th class="text-center">
							View
							<label class="kt-checkbox permission-checkbox">
								<input type="checkbox" name="can_view" id="can_view" value="1" onchange="check_checkbox('can_view');">
								<span></span>
							</label>
						</th>
						<th class="text-center">
							Create
							<label class="kt-checkbox permission-checkbox">
								<input type="checkbox" name="can_create" id="can_create" value="1" onchange="check_checkbox('can_create');">
								<span></span>
							</label>
						</th>
						<th class="text-center">
							Edit
							<label class="kt-checkbox permission-checkbox">
								<input type="checkbox" name="can_edit" id="can_edit" value="1" onchange="check_checkbox('can_edit');">
								<span></span>
							</label>
						</th>
						<th class="text-center">
							Delete
							<label class="kt-checkbox permission-checkbox">
								<input type="checkbox" name="can_delete" id="can_delete" value="1" onchange="check_checkbox('can_delete');">
								<span></span>
							</label>
						</th>
						<th class="text-center">
							Export
							<label class="kt-checkbox permission-checkbox">
								<input type="checkbox" name="can_export" id="can_export" value="1" onchange="check_checkbox('can_export');">
								<span></span>
							</label>
						</th>
					</thead>
					<tbody>
						<?php
							if(isset($role['permissions'])){ 
                $role_permissions = explode(",", $role['permissions']); 
            	} else{ 
                $role_permissions = array(); 
              }
						 foreach ($permissions as $key => $permission) {
						 	if(isset($role_permissions[$key])){ 
	              $temp = str_split ($role_permissions[$key]);
		          } else{ 
	              $temp = array(); 
		          } 
		        ?>
						<tr>
							<td><?php echo $permission->name; ?></td>
							<td class="text-center">
								<label class="kt-checkbox" data-toggle="kt-tooltip" data-html="true"
									<?php 
										if (!empty(json_decode($permission->item,true))) {
											?>title="<?php
												foreach (json_decode($permission->item,true) as $json_data) {
													echo "<ul class='list-unstyled'>";
														if($json_data['access'] == 'View'){
															echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
														}
													echo "</ul>";
												}
											?>"<?php
										}
									?>
								>
									<input type="checkbox" name="can_view[<?php echo $permission->id; ?>]" <?php if ($permission->item_view != 1 ) { echo "disabled";	} ?> value="1" <?php if(isset($temp[0])){ if($temp[0]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_view">
									<span></span>
								</label>
							</td>
							<td class="text-center">
								<label class="kt-checkbox" data-toggle="kt-tooltip" data-html="true"
									<?php 
										if (!empty(json_decode($permission->item,true))) {
											?>title="<?php
												foreach (json_decode($permission->item,true) as $json_data) {
													echo "<ul class='list-unstyled'>";
														if($json_data['access'] == 'Create'){
															echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
														}
													echo "</ul>";
												}
											?>"<?php
										}
									?>
								>
									<input type="checkbox" name="can_create[<?php echo $permission->id; ?>]" <?php if ($permission->item_create != 1 ) { echo "disabled";	} ?> value="1" <?php if(isset($temp[1])){ if($temp[1]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_create">
									<span></span>
								</label>
							</td>
							<td class="text-center">
								<label class="kt-checkbox" data-toggle="kt-tooltip" data-html="true"
									<?php 
										if (!empty(json_decode($permission->item,true))) {
											?>title="<?php
												foreach (json_decode($permission->item,true) as $json_data) {
													echo "<ul class='list-unstyled'>";
														if($json_data['access'] == 'Edit'){
															echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
														}
													echo "</ul>";
												}
											?>"<?php
										}
									?>
								>
									<input type="checkbox" name="can_edit[<?php echo $permission->id; ?>]" <?php if ($permission->item_edit != 1 ) { echo "disabled";	} ?> value="1" <?php if(isset($temp[2])){ if($temp[2]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_edit">
									<span></span>
								</label>
							</td>
							<td class="text-center">
								<label class="kt-checkbox" data-toggle="kt-tooltip" data-html="true"
									<?php 
										if (!empty(json_decode($permission->item,true))) {
											?>title="<?php
												foreach (json_decode($permission->item,true) as $json_data) {
													echo "<ul class='list-unstyled'>";
														if($json_data['access'] == 'Delete'){
															echo "<li class='mt-list-item'><h6>".$json_data['item']."</h6</ul>";
														}
													echo "</ul>";
												}
											?>"<?php
										}
									?>
								>
									<input type="checkbox" name="can_delete[<?php echo $permission->id; ?>]" <?php if ($permission->item_delete != 1 ) { echo "disabled";	} ?> value="1" <?php if(isset($temp[3])){ if($temp[3]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_delete">
									<span></span>
								</label>
							</td>
							<td class="text-center">
								<label class="kt-checkbox">
									<input type="checkbox" name="can_export[<?php echo $permission->id; ?>]" value="1" <?php if(isset($temp[4])){ if($temp[4]==1){ echo "checked"; } } ?> id="<?php echo $permission->param ?>_can_export">
									<span></span>
								</label>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">
						Save Role
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
 	$("#role_form").validate ({
    rules: {
      name:{
        required : true,
      }
    }, 
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      element.closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.error').remove();
    }
	});
});
function check_checkbox(action){
  $("form#role_form :input[id*='_"+action+"']").each(function(){
    if($('#'+action+'').prop('checked')==true){
    	$(this).prop('checked',true);
    }else{
      $(this).prop('checked',false);
    }
  });
}
</script>