<?php
	defined('BASEPATH') or exit('No direct script access allowed');
	function get_site_setting($field){
	  $CI = & get_instance();
	  $res = $CI->db->select('value')->where('name',$field)->get('site_settings')->row_array();
	  if(!empty($res)){
	  	return $res['value'];
	  }else{
	  	return;
	  }
	}

	function file_upload($element_name,$location){
		$CI = & get_instance();
		$config = array('upload_path'=>"./documents/temp",'allowed_types'=>"*");
		$CI->load->library('upload', $config);
		$uploaded_file_name='';
		if ($CI->upload->do_upload($element_name)){
			$upload_data = $CI->upload->data();
			$file_name = $upload_data['file_name'];	

			$filenamearr = explode(".", $file_name);
			$filename = $filenamearr[0];
			$filetype = $filenamearr[1];

			$CI->load->helper('string');
			$random = random_string('alnum',5);
			$random1 = random_string('alnum',5);

			$uploaded_file_name = $random.'-'.$random1.".".$filetype;

			$src="./documents/temp/".$file_name;
		
			$dest='./documents/'.$location.'/'.$uploaded_file_name;
			copy($src , $dest); 
			unlink($src);
		}
		return $uploaded_file_name;
	}

	function file_upload_with_original_name($element_name,$location){
		$CI = & get_instance();
		$config = array('upload_path'=>"./documents/temp",'allowed_types'=>"*");
		$CI->load->library('upload', $config);
		$uploaded_file_name='';
		if ($CI->upload->do_upload($element_name)){
			$upload_data = $CI->upload->data();
			$file_name = $upload_data['file_name'];	

			$src="./documents/temp/".$file_name;

			if (!is_dir('./documents/'.$location)) {
				mkdir('./documents/'.$location, 0777, true);
			}
		
			$dest='./documents/'.$location.'/'.$file_name;
			copy($src , $dest); 
			unlink($src);
		}
	}

	function get_email_template_by_title($title=""){
		$CI = & get_instance();
		return $CI->db->select('subject,message')->where('status',1)->where('title',$title)->get('email_templates')->row_array();
	}

	function count_todays_events(){
		$CI = & get_instance();
		$count = $CI->db->select('*')->where('event_date',date("Y-m-d"))->order_by('event_date','ASC')->get('eventmaster')->result();
		return count($count);
	}

	function count_sticky(){
		$CI = & get_instance();
		$id = $CI->session->userdata('id');
		$count = $CI->db->select('*')->where('sticky_notes.added_by',$id)->where('sticky_status',1)->or_where("FIND_IN_SET(".$id.",shared_person) !=", 0)->get('sticky_notes')->result();
		return count($count);
	}


	function convert_time_to_60($time){
		$clock_in_time = explode('.', $time); 
		$temp_cit='';
		if($clock_in_time[0]==''){
			$temp_cit = '0';
		}else{
			$temp_cit = $clock_in_time[0];
		}
		if($clock_in_time[1]==''){
			$temp_cit .= ':00';
		}else{
			$t1 = substr(floor(($clock_in_time[1]*60)/100),0,2);
			$temp_cit .= ':'.str_pad($t1, 2, '0', STR_PAD_LEFT);
		}
		return $temp_cit;
	}

	function month_name($month_number){
		return date('F', mktime(0, 0, 0, $month_number, 10));
	}


	// get get last date of given month (of year)
	function month_end_date($year, $month_number){
		return date("t", strtotime("$year-$month_number"));
	}

	// return two digit month or day, e.g. 04 - April
	function zero_pad($number){
		if($number < 10)
			return "0$number";
		
		return "$number";
	}

?>