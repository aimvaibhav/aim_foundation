"use strict";
var KTDatatableRemoteAjaxDemo = function() {
	var demo = function() {
		var datatable = $('.kt-datatable').KTDatatable({
			data: {
				type: 'remote',
				source: {
					read: {
						url: $('#base_url').val()+'users/get_user_data',
						headers: {'x-my-custokt-header': 'some value', 'x-test-header': 'the value'},
						map: function(raw) {
							var dataSet = raw;
							if (typeof raw.data !== 'undefined') {
								dataSet = raw.data;
							}
							return dataSet;
						},
					},
				},
				pageSize: 5,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},
			layout: {
				scroll: false,
				footer: false,
			},
			sortable: true,
			pagination: true,
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'id',
					title: '#',
					sortable: 'asc',
					width: 30,
					type: 'number',
					selector: false,
					textAlign: 'center',
				}, {
					field: 'username',
					title: 'Username',
				}, {
					field: 'name',
					title: 'Name'
				}, {
					field: 'email',
					title: 'Email'
				}, {
					field: 'phone',
					title: 'Phone',
				}
			],
		});
    $('#kt_form_status').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'Status');
    });
    $('#kt_form_type').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'Type');
    });
    $('#kt_form_status,#kt_form_type').selectpicker();
	};
	return {
		init: function() {
			demo();
		},
	};
}();
jQuery(document).ready(function() {
	KTDatatableRemoteAjaxDemo.init();
});